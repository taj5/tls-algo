//
// Created by Shangxichao on 2018/3/20.
//
#ifndef MT_IO_LOG_IMTLOG_H
#define MT_IO_LOG_IMTLOG_H
#include "Common/MTUtilGlobal.h"
#include <stdint.h>
#include <string>
#include <vector>
NS_BGN_MT_UTIL_LOGGING
enum class MTLogLevel {
	Off = 0,
	Fatal,
	Error,
	Warn,
	Info,
	Debug,
	Trace,
	All
};

enum class MTLogFormat {
	Normal = 0,
	File = 1,
	Sqlite = 2,
	CSV = 3
};

class MT_UTIL_API IMTLog {
public:


public:
	IMTLog() {}
	virtual ~IMTLog() {}
private:
	IMTLog(const IMTLog&);
	IMTLog& operator=(const IMTLog&);
public:
	virtual bool Open(const std::string& moduleName) = 0;
	virtual void SetLogPath(const std::string&) = 0;
	virtual void Log(MTLogLevel level,const char* title...) = 0;
	virtual void Log(MTLogLevel level, const std::string& title) = 0;
	virtual void Log(std::vector<std::string>& record) = 0;
	virtual void TableHead(std::vector<std::string>& record) = 0;
	virtual bool IsNeedRotate() = 0;
	virtual void Flush() = 0;
	virtual void Close() = 0;
	virtual void SetMaxLogLevel(MTLogLevel logLevel) = 0;
	virtual MTLogLevel GetMaxLogLevel() = 0;

	virtual void SetLogExpired(int minutes) = 0;
	virtual int GetLogExpired() const = 0;
	virtual void ClearExpiredLog() = 0;
    virtual void LogBinary(MTLogLevel level, const std::string& data) = 0;
};

class MTLogItem {
public:
	MTLogItem();
	//MTLogItem(const MTLogItem&);
	//MTLogItem& operator=(const MTLogItem&);
	~MTLogItem();
private:
	//std::string m_ProcessName;
	std::string m_ModuleName;
	MTLogLevel  m_LogLevel;
	int64_t    m_Timestamp;
	std::string m_Content;

public:
	std::string GetModuleName()const;
	void SetModuleName(const std::string& moduleName);

	MTLogLevel GetLogLevel() const;
	void SetLogLevel(const MTLogLevel& level);

	int64_t GetTimestamp() const;
	void SetTimestamp(int64_t ts);

	std::string GetContent() const;
	void SetContent(const std::string& content);


};



NS_END_MT_UTIL_LOGGING

#endif // MT_IO_LOG_IMTLOG_H
