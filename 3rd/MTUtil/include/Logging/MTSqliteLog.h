//
// Created by Qianguangpan on 2019/6/13.
//
#ifndef MT_IO_LOG_MTSQLLITELOG_H
#define MT_IO_LOG_MTSQLLITELOG_H
#include <string>
#include <vector>
#include <mutex>
#include <map>
#include <chrono>

#include "Common/MTUtilGlobal.h"
#include "Logging/IMTLog.h"
#include "SQLiteCpp/SQLiteCpp.h"

/*
    提供一种SQLLite日志的方式
    分配策略:
        1.按小时分表;
        2.按天分分库;
*/

NS_BGN_MT_UTIL_LOGGING
struct SqlLine
{
	std::string level;
	std::string ts;
	std::string msg;
#define MSG_STRING 1
#define MSG_BINARY 2
    int msgType;
};

class MT_UTIL_API MTSqliteLog :public IMTLog {
public:
	MTSqliteLog();
	~MTSqliteLog();
	MTSqliteLog(const MTSqliteLog&) = delete;
	MTSqliteLog&operator=(const MTSqliteLog&) = delete;

public:
	bool Open(const std::string& moduleName);
	void SetLogPath(const std::string&);
	void Log(MTLogLevel level, const char* title,...);
	void Log(MTLogLevel level, const std::string& title);
	void Log(std::vector<std::string>& record){};
	void TableHead(std::vector<std::string>& head){};
	bool IsNeedRotate();/*  这里应该是检查是否需要分库  */
	void Flush();
	void Close();
	void SetMaxLogLevel(MTLogLevel logLevel);
	MTLogLevel GetMaxLogLevel();
	void SetLogExpired(int minutes);
	int GetLogExpired() const;
	void ClearExpiredLog();
    void LogBinary(MTLogLevel level, const std::string& data);
private:
	void LogContent(MTLogLevel level, const std::string& content, int contentType);
    bool IfTableNeedSplit();
	bool IsDataBaseOutOfDate(const std::string& fileName);
    bool IsMyDataBase(const std::string& fileName);
	bool IsTableOutOfDate(const std::string& tableName);
    bool IsTimeExpired(const std::string& timeString);
	void SetTableName();

    SQLite::Database *m_Database; /*  运行时可能会换库，保存当前的数据库指针  */
    std::mutex m_DatabaseLocker;

    std::string m_DatabaseName; /*  按天分库    */
	std::string m_TableName;	/*	按小时分表	*/
	std::string m_Path; /*  库保存的路径 */

    typedef std::chrono::duration <int, std::ratio<60 * 60 * 24>> day_type;
    typedef std::chrono::duration <int, std::ratio<60 * 60>> hour_type;

    std::chrono::time_point<std::chrono::system_clock, day_type> m_DayLatest;
    std::chrono::time_point<std::chrono::system_clock, hour_type> m_HourLatest;
	int32_t		m_ABNo;
	std::vector<SqlLine>  m_LogCacheA;
	std::vector<SqlLine>  m_LogCacheB;
	std::mutex        m_CachedLocker;

	MTLogLevel m_MaxLogLevel;
	std::string m_ModuleName;
	int32_t 	m_LogExpired; /*    过期时间(单位分钟)  */
	const std::string m_LevelMap[8] = {"Off","Fatal","Error","Warn","Info","Debug","Trace","All"};
};
NS_END_MT_UTIL_LOGGING

#endif // MT_IO_LOG_MTSQLLITELOG_H
