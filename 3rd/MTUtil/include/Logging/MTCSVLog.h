//
// Created by Shangxichao on 2018/3/20.
//
#ifndef MT_IO_LOG_MTCSVLOG_H
#define MT_IO_LOG_MTCSVLOG_H
#include <string>
#include <vector>
#include <mutex>
#include "Common/MTUtilGlobal.h"
#include "Logging/IMTLog.h"

NS_BGN_MT_UTIL_LOGGING
#define LogCSVLength 31457280 //30M excel锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷[row < 1040k, col < 16k]
class MT_UTIL_API MTCSVLog :public IMTLog {
public:
	MTCSVLog();
	~MTCSVLog();
	MTCSVLog(const MTCSVLog&) = delete;
	MTCSVLog&operator=(const MTCSVLog&) = delete;
public:
	bool Open(const std::string& moduleName);
	void SetLogPath(const std::string&);
	void Log(MTLogLevel level, const char* title, ...);
	void Log(MTLogLevel level, const std::string& title);
	void Log(std::vector<std::string>& record);
	void TableHead(std::vector<std::string>& head);
	bool IsNeedRotate();
	void Flush();
	void Close();
	void SetMaxLogLevel(MTLogLevel logLevel);
	MTLogLevel GetMaxLogLevel();

	void SetLogExpired(int minutes);
	int GetLogExpired() const;
	void ClearExpiredLog();
    void LogBinary(MTLogLevel level, const std::string& data){};
private:
	std::string m_FileName;
	std::string m_Path;
	FILE		*m_File;
	uint64_t	m_CurrentFileSize;
	int32_t		m_ABNo;
	std::vector<std::vector<std::string>>  m_LogCacheA;
	std::vector<std::vector<std::string>>  m_LogCacheB;
	std::mutex                m_CachedLocker;
	std::string m_ModuleName;
	int32_t m_LogExpired;
};
NS_END_MT_UTIL_LOGGING

#endif // MT_IO_LOG_MTCSVLOG_H
