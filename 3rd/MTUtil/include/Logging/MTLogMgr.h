//
// Created by Shangxichao on 2018/3/20.
//

#ifndef MT_IO_LOG_MTLOGMGR_H
#define MT_IO_LOG_MTLOGMGR_H
#include <map>
#include <thread>
#include <mutex>
#include "Common/MTUtilGlobal.h"
#include "Logging/IMTLog.h"
NS_BGN_MT_UTIL_LOGGING
//class IMTLog;
class MT_UTIL_API MTLogMgr {
public:
	static MTLogMgr& GetInstance();
	~MTLogMgr();
private:
	MTLogMgr();
	MTLogMgr(const MTLogMgr&);
	MTLogMgr& operator=(const MTLogMgr&);
public:
	bool Start();
	void Stop();
	IMTLog *GetLogger(const std::string& moduleName, const std::string& path = ".", MTLogFormat format = MTLogFormat::Normal);

	void SetMaxLogLevel(MTLogLevel logLevel);
	void SetMaxLogLevel(const std::string& logLevel);

	void Log(const std::string& moduleName,MTLogLevel level, const char* title...);
	void Log(const std::string& moduleName,MTLogLevel level, const std::string& title);

	void Log(IMTLog *logger, MTLogLevel level, const char* title,...);
	void Log(IMTLog *logger, MTLogLevel level, const std::string& title);
	
	bool GetLoggingFlag();
	int32_t GetLogInterval();
	void SetLogInterval(int32_t msecs);

	void SetLogExpired(int minutes);
	int GetLogExpired(IMTLog* logger) const;

protected:
	typedef std::map<std::string, IMTLog*> MTLogMap;
	static void doLogging(MTLogMgr* logMgr);

	void setLoggingFlag(bool flag);
private:
	MTLogMap m_LogMap;
	uint8_t  m_LoggingFlag;
	int32_t  m_LogInterval;//msecs
	std::mutex m_Locker;
	std::thread *m_TLogging;
	MTLogLevel m_LogLevel;
};
NS_END_MT_UTIL_LOGGING
#endif // MT_IO_LOG_MTLOGMGR_H