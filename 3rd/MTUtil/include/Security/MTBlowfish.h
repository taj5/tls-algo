
/******************************************************************************
*       __        __      ____________   
*      /  \      /  \    |            |  
*     /    \    /    \    ----    ----   
*    /  /\  \  /  /\  \       |  |       
*   /  /  \  \/  /  \  \      |  |       
*  /__/    \____/    \__\     |__|       
******************************************************************************
*
*  Copyright (c) 2019 by Tianjin Meiteng Technology Co., Ltd. Tianjin, China. All
*  rights reserved. Property of Tianjin Meiteng Technology Co., Ltd. Restricted
*  rights to use, duplicate or disclose this code are granted through contract.
*
*  Created by LENOVO on 2019/1/7.
******************************************************************************/

#ifndef MTUTIL_MTBLOWFISH_H
#define MTUTIL_MTBLOWFISH_H

#include <string>
#include <Common/MTUtilGlobal.h>
#include "IMTSecurity.h"
NS_BGN_MT_UTIL_SECURITY
class MT_UTIL_API MTBlowfish:public IMTSecurity{

public:
    MTBlowfish();
    MTBlowfish(const std::string &key);
    virtual ~MTBlowfish();

public:
    std::string Encrypt(const std::string &data)  override;

    std::string Decrypt(const std::string &data)  override;

    void SetKey(const std::string &key) override;

private:
    void SetKey(const char *key, size_t byte_length);
    void EncryptBlock(uint32_t *left, uint32_t *right) const;
    void DecryptBlock(uint32_t *left, uint32_t *right) const;
    uint32_t Feistel(uint32_t value) const;

private:
    uint32_t pary_[18];
    uint32_t sbox_[4][256];
};
NS_END_MT_UTIL_SECURITY
#endif //MTUTIL_MTBLOWFISH_H
