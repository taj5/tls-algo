
/******************************************************************************
*       __        __      ____________   
*      /  \      /  \    |            |  
*     /    \    /    \    ----    ----   
*    /  /\  \  /  /\  \       |  |       
*   /  /  \  \/  /  \  \      |  |       
*  /__/    \____/    \__\     |__|       
******************************************************************************
*
*  Copyright (c) 2019 by Tianjin Meiteng Technology Co., Ltd. Tianjin, China. All
*  rights reserved. Property of Tianjin Meiteng Technology Co., Ltd. Restricted
*  rights to use, duplicate or disclose this code are granted through contract.
*
*  Created by sxc on 2019/1/7.
******************************************************************************/

#ifndef MTUTIL_IMTSECURITY_H
#define MTUTIL_IMTSECURITY_H

#include <string>
class IMTSecurity{
public:
    IMTSecurity(){}
    virtual ~IMTSecurity(){}

private:
    IMTSecurity(const IMTSecurity&) = delete;
    IMTSecurity& operator=(const IMTSecurity&) = delete;
public:
    virtual std::string Encrypt(const std::string& data)  = 0;
    virtual std::string Decrypt(const std::string& data)  = 0;
    virtual void SetKey(const std::string& key) = 0;
};

#endif //MTUTIL_IMTSECURITY_H
