
/******************************************************************************
*       __        __      ____________   
*      /  \      /  \    |            |  
*     /    \    /    \    ----    ----   
*    /  /\  \  /  /\  \       |  |       
*   /  /  \  \/  /  \  \      |  |       
*  /__/    \____/    \__\     |__|       
******************************************************************************
*
*  Copyright (c) 2019 by Tianjin Meitneg Technology Co., Ltd. Tianjin, China. All
*  rights reserved. Property of Tianjin Meitneg Technology Co., Ltd. Restricted
*  rights to use, duplicate or disclose this code are granted through contract.
*
*  Created by sxc on 2019/1/8.
******************************************************************************/

#ifndef MTUTIL_MTSECURITYMGR_H
#define MTUTIL_MTSECURITYMGR_H
#include <common/MTUtilGlobal.h>
#include "IMTSecurity.h"
#include "MTBlowfishEx.h"
#include "MTBlowfish.h"

NS_BGN_MT_UTIL_SECURITY

class MT_UTIL_API MTSecurityMgr{
private:
    MTSecurityMgr(){}
    MTSecurityMgr(const MTSecurityMgr&) = delete;
    MTSecurityMgr&operator=(const MTSecurityMgr&) = delete;

public:
    virtual ~MTSecurityMgr(){}
public:
    static MTSecurityMgr* GetInstance(){
        static  MTSecurityMgr instance;
        return &instance;
    }

public:
    IMTSecurity *GetSecurity(const std::string& funcName)
    {
        std::string upperFName = funcName;
        if (0 == strcmp(upperFName.c_str(), "BLOWFISH"))
        {
            static MTBlowfish mtBlowfish;
            return &mtBlowfish;
        }
        else if (0 == strcmp(upperFName.c_str(), "BLOWFISHEX"))
        {
            static CBlowFish mtBlowfishEx;
            return &mtBlowfishEx;
        }
        return nullptr;
    }
};

NS_END_MT_UTIL_SECURITY

#endif //MTUTIL_MTSECURITYMGR_H
