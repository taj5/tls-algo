#pragma once
#include <condition_variable>
#include <mutex>
#include <Common/MTUtilGlobal.h>
 
 class MT_UTIL_API  MTAtomicSemaphore
{
public:
	MTAtomicSemaphore(unsigned long cur = 0, unsigned long COUNT = 5);
	MTAtomicSemaphore(const MTAtomicSemaphore&) = delete;
	MTAtomicSemaphore& operator=(const MTAtomicSemaphore&) = delete;
	void addWaitAtomicStart();
	void addWaitAtomicEnd();
	void minusWaitAtomicStart();
	void minusWaitAtomicEnd();
	int minusLessThanWaitAtomicStart(int size =100);
	void WaitAtomicEnd();
	int addLessThanWaitAtomicStart(int size =100);
	bool isEmpty();
	bool isFull();
private:
	std::mutex m_atomicMutex;
	std::mutex m_mutex;
	std::condition_variable m_minusPtr;
	std::condition_variable m_addPr;
	unsigned long m_count;
	unsigned long m_cur;
};