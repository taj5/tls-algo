#ifndef CONSUME_EXCEPTION_H
#define CONSUME_EXCEPTION_H
#include<exception>    
#include<iostream>    
#include <string>

class ConsumeCannotContinue:public std::exception
{
public:
	ConsumeCannotContinue(const std::string& content)
		:std::exception(content.c_str())
	{

	}
};
#endif

