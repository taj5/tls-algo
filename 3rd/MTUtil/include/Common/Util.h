#ifndef MTIO_UTIL_H
#define MTIO_UTIL_H
#include <stdint.h>
#include <string>
#include "Common/MTUtilGlobal.h"
//#include "boost/pool/pool.hpp"
//#include "boost/pool/object_pool.hpp"

/* Evaluates to the same boolean value as 'p', and hints to the compiler that
* we expect this value to be false. */
#if defined(__GNUC__) && __GNUC__ >= 3         /* gcc 3.0 or later */
#define UTIL_UNLIKELY(p) __builtin_expect(!!(p),0)
#else
#define UTIL_UNLIKELY(p) (p)
#endif

MT_UTIL_API void util_mttime_init();
MT_UTIL_API float util_mttime_diff_ms(uint64_t ts);
MT_UTIL_API uint64_t util_mttime_tick_count();

struct timezone;
MT_UTIL_API int util_gettimeofday(struct timeval *tv, struct timezone *tz);
MT_UTIL_API long util_tv_to_msec(const struct timeval *tv);

MT_UTIL_API int64_t util_gettimestamp();
// "yyyy-MM-dd HH:mm:ss"
MT_UTIL_API void util_gettimestampString(int64_t timestamp, char *date_string);
// "yyyyMMddHHmmss"
MT_UTIL_API void util_gettimestampString2(int64_t timestamp, char *date_string);
// "yyyy-MM-dd HH:mm:ss.zzz"
MT_UTIL_API void util_gettimestampWithMSString(int64_t timestamp, char *date_string);

MT_UTIL_API uint16_t util_flipbytes(uint16_t value);
MT_UTIL_API uint32_t util_flipbytes(uint32_t value);
MT_UTIL_API uint64_t util_flipbytes(uint64_t value);

MT_UTIL_API void util_get_vertion(std::string &version);

MT_UTIL_API void sprintf_std_string(std::string & destination, const char * format, ...);

#ifdef _WIN32
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#define TM_YEAR_BASE 1900
/*
 * We do not implement alternate representations. However, we always
 * check whether a given modifier is allowed for a certain conversion.
 */
#define ALT_E     0x01
#define ALT_O     0x02
#define LEGAL_ALT(x)    { if (alt_format & ~(x)) return (0); }
char * strptime(const char *buf, const char *fmt, struct tm *tm);
static  int conv_num(const char **, int *, int, int);
static const char *day[7] = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
static const char *abday[7] = { "Sun","Mon","Tue","Wed","Thu","Fri","Sat" };
static const char *mon[12] = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
static const char *abmon[12] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
static const char *am_pm[2] = { "AM", "PM" };
#endif

#endif //ASHDETECTOR_UTIL_H
