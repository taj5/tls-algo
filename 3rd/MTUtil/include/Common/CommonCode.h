#ifndef MT_COMMNON_COMMONCODE_H
#define MT_COMMNON_COMMONCODE_H

#define CFG_INI_NAME		        "tds.ini"
#define CFG_INI_SECTION_REDIS	    "TDSSettings"
#define CFG_INI_REDISCFGCONSTR      "CfgConstr"

#define CFG_PARAMS_CONSTR	  "tix:settings:common:parammsgconnstring"
#define CFG_TDSPARAMS_CONSTR    "tix:settings:common:tdsparammsgconnstring"
#define CFG_CACHECONSTR       "tix:settings:common:cacheconnstring"
#define CFG_DES_KEY           "tix:settings:common:key"
#define CFG_CACHE_TIMEOUT	  "tix:settings:common:cachetimeout"
#define CFG_KCHANNELCONSTR    "tix:settings:common:kmsgconnstring"
#define CFG_ICHANNELCONNSTR   "tix:settings:common:imsgconnstring"   
#define CFG_TCHANNELCONNSTR   "tix:settings:common:tmsgconnstring"
#define CFG_LOGLEVEL          "tix:settings:common:loglevel"

enum class ConnectionStatus{
    UNKNOWN      = 0x00000000,
    DISCONNECTED = 0x00000001,
    CONNECTED    = 0x00000002
};

#endif
