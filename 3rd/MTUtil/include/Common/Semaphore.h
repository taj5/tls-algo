#ifndef SEMAPHORE_H
#define SEMAPHORE_H
#include <mutex>
#include <atomic>
#include "Util.h"
#include "Logging/IMTLog.h"
using MT::Util::Logging::IMTLog;

class Semaphore {
public:
	explicit Semaphore(int count = 0) :
		count_(count)
		, logger_(nullptr)
	{
	//	InitLogger();
	}

	void Signal()
	{
		std::unique_lock<std::mutex> lock(mutex_);
		if (logger_)
			logger_->Log(MTLogLevel::Debug, "Signal_0 count %d", count_);

		if (++count_ <=0)
			cv_.notify_one();

		if (logger_)
			logger_->Log(MTLogLevel::Debug, "Signal_1 count %d", count_);
	}

	void Wait()
	{
		std::unique_lock<std::mutex> lock(mutex_);
		if (logger_)
			logger_->Log(MTLogLevel::Debug, "Wait_0 count %d", count_);

		if (--count_ < 0)
		{
			cv_.wait(lock);
		}

		if (logger_)
			logger_->Log(MTLogLevel::Debug, "Wait_1 count %d", count_);
	}

	void InitLogger()
	{
		logger_ = MTLogMgr::GetInstance().GetLogger("Semaphore_", "D://");
	}

private:
	std::mutex mutex_;
	std::condition_variable cv_;
	int count_;
	IMTLog *logger_;
};
#endif