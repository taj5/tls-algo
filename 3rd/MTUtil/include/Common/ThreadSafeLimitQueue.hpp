#ifndef THREAD_SAFE_LIMIT_QUEUE_H
#define THREAD_SAFE_LIMIT_QUEUE_H
#include <queue>
#include <mutex>
#include <functional>
#include "Logging/IMTLog.h"
using MT::Util::Logging::IMTLog;
template<class T>
class ThreadSafeLimitQueue
{
public:
	ThreadSafeLimitQueue();
	virtual ~ThreadSafeLimitQueue();
	void SetCapacity(int capacity);
	void SetLogger(IMTLog* logger);
	void SetOverFlowHandler(std::function<void(T&)>overFlowHandler);
	void push(const T&data);
	void pop();
	T& front();
	bool empty();
	void swap(std::queue<T>& destination);
private:
	int m_Capacity;
	std::mutex m_Mutex;
	std::queue<T> m_Queue;
	IMTLog *m_Logger;
	std::function<void(T&)>m_OverFlowHandler;
};

template<class T>
ThreadSafeLimitQueue<T>::ThreadSafeLimitQueue()
	:m_Capacity(1000)
	,m_Logger(nullptr)
	,m_OverFlowHandler(nullptr)
{
}

template<class T>
ThreadSafeLimitQueue<T>::~ThreadSafeLimitQueue()
{
}

template<class T>
void ThreadSafeLimitQueue<T>::SetCapacity(int capacity)
{
	m_Capacity = capacity;
}

template<class T>
void ThreadSafeLimitQueue<T>::SetLogger(IMTLog* logger)
{
	m_Logger = logger;
}

template<class T>
void ThreadSafeLimitQueue<T>::SetOverFlowHandler(std::function<void(T &)>overFlowHandler)
{
	m_OverFlowHandler = overFlowHandler;
}

template<class T>
void ThreadSafeLimitQueue<T>::push(const T&data)
{
	std::unique_lock<std::mutex>lock(m_Mutex);
	if (m_Queue.size() >= m_Capacity)
	{
		T& head = m_Queue.front();
		m_Queue.pop();

		if (nullptr != m_Logger)
		{
			m_Logger->Log(MTLogLevel::Error, "the queue is full, change the queue capacity or check current situation.");
		}
		if (nullptr != m_OverFlowHandler)
		{
			m_OverFlowHandler(head);
		}
	}
	m_Queue.push(data);
}

template<class T>
void ThreadSafeLimitQueue<T>::pop()
{
	std::unique_lock<std::mutex>lock(m_Mutex);
	m_Queue.pop();
}

template<class T>
T& ThreadSafeLimitQueue<T>::front()
{
	std::unique_lock<std::mutex>lock(m_Mutex);
	return m_Queue.front();
}

template<class T>
bool ThreadSafeLimitQueue<T>::empty()
{
	std::unique_lock<std::mutex>lock(m_Mutex);
	return m_Queue.empty();
}

template<class T>
void ThreadSafeLimitQueue<T>::swap(std::queue<T>& destination)
{
	std::unique_lock<std::mutex>lock(m_Mutex);
	std::swap(m_Queue, destination);
}
#endif
