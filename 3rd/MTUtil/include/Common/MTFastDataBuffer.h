#ifndef MT_IO_COMMON_MTFASTDATABUFFER_H
#define MT_IO_COMMON_MTFASTDATABUFFER_H
#include <stdint.h>
#include "MTUtilGlobal.h"

NS_BGN_MT_UTIL
class MTFastDataBuffer{
    public:
        typedef unsigned char MTByte;
        MTFastDataBuffer(int32_t size=1024);
        MTFastDataBuffer(const MTByte *data,int dataSize);
        MTFastDataBuffer(const MTFastDataBuffer&);
        MTFastDataBuffer& operator=(const MTFastDataBuffer&);
        ~MTFastDataBuffer();

    public:
        void Append(const MTByte *data,int32_t dataSize);
        const MTByte* GetData(int32_t offset) const;
        int32_t GetDataSize() const;
        int32_t GetRemainDataSize(int32_t offset) const;     
        void Reset();
    protected:
        void extend(int32_t sizeExtend);
    protected:
        int32_t m_DefaultSize;
        int32_t m_ExtendSize;
        int32_t m_TotalSize;
        int32_t m_CurrentSize;
        MTByte* m_Data;    
};
NS_END_MT_UTIL

#endif