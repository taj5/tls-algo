#ifndef MT_IO_GLOBAL_H
#define MT_IO_GLOBAL_H

//#############################BEGIN#####################################
#ifndef NS_BGN_MT
    #define NS_BGN_MT namespace MT{
#endif

#define NS_BGN_MT_UTIL NS_BGN_MT namespace Util{
#define NS_BGN_MT_UTIL_NET NS_BGN_MT_UTIL namespace Net{
#define NS_BGN_MT_UTIL_LOGGING NS_BGN_MT_UTIL namespace Logging{
#define NS_BGN_MT_UTIL_CONFIGURATIONS NS_BGN_MT_UTIL namespace Configurations{
#define NS_BGN_MT_UTIL_IO NS_BGN_MT_UTIL namespace IO{
#define NS_BGN_MT_UTIL_SECURITY NS_BGN_MT_UTIL namespace Security{

#define NS_BGN_MT_UTIL_NET_TCP NS_BGN_MT_UTIL_NET namespace Tcp{
#define NS_BGN_MT_UTIL_NET_UDP NS_BGN_MT_UTIL_NET namespace Udp{
#define NS_BGN_MT_UTIL_NET_MQ NS_BGN_MT_UTIL_NET namespace Mq{



//#############################END#####################################
#ifndef NS_END_MT 
#define NS_END_MT } 
#endif
#define NS_END_MT_UTIL } NS_END_MT
#define NS_END_MT_UTIL_NET } NS_END_MT_UTIL
#define NS_END_MT_UTIL_LOGGING } NS_END_MT_UTIL
#define NS_END_MT_UTIL_CONFIGURATIONS } NS_END_MT_UTIL
#define NS_END_MT_UTIL_IO } NS_END_MT_UTIL
#define NS_END_MT_UTIL_SECURITY } NS_END_MT_UTIL

#define NS_END_MT_UTIL_NET_TCP } NS_END_MT_UTIL_NET
#define NS_END_MT_UTIL_NET_UDP } NS_END_MT_UTIL_NET
#define NS_END_MT_UTIL_NET_MQ } NS_END_MT_UTIL_NET


/**
 * A type wide enough to hold the output of "socket()" or "accept()".  On
 * Windows, this is an intptr_t; elsewhere, it is an int. */
#ifdef _WIN32
//#include <windows.h>
#ifdef _WIN32
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <winsock2.h>
#include <windows.h>
#undef WIN32_LEAN_AND_MEAN
#endif
typedef intptr_t IOHandle;
#else
typedef int IOHandle;
#endif


#ifdef _WIN32

    #if defined(MT_DLLEXPORT)
        #define MT_UTIL_API __declspec(dllexport)
    #elif defined(MT_DLLIMPORT)
        #define MT_UTIL_API __declspec(dllimport)
    #else
        #define MT_UTIL_API
    #endif
#else
    #define MT_UTIL_API
#endif


#endif
