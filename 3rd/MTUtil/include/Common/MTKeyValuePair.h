/***************************************************************************
 * CopyRight (C) ShangXichao     
 * Project:   
 * FileName: MTKeyValuePair.h                                                            
 * Created on: 2011-6-10
 *     Author: 商希超
 * Description:
 * FunctionList:
 * History:
 *   1.Date        : 2011-6-10
 *     Author      : 商希超
 *	   Modification: 
 *
 ***************************************************************************/

#ifndef MTKEYVALUEPAIR_H_
#define MTKEYVALUEPAIR_H_
#include <string>
#include "Common/MTUtilGlobal.h"   



#include <ostream>
using namespace std;

NS_BGN_MT_UTIL_CONFIGURATIONS
template<typename TKey,typename TValue>
class MTKeyValuePair
{
	public:
	MTKeyValuePair(const TKey& tKey,const TValue& tValue):
		_key(tKey),_value(tValue)
	{
		
	}

	MTKeyValuePair(const MTKeyValuePair& pair):
		_key(pair._key),_value(pair._value)
	{

	}

	MTKeyValuePair& operator=(const MTKeyValuePair& pair)
	{
		if(&pair != this)
		{
			_key = pair._key;
			_value = pair._value;
		}
		return *this;
	}

	bool operator<(const MTKeyValuePair& pair) const
	{
		if(&pair == this)
			return false;
		return _key < pair._key;
	}

	bool operator==(const MTKeyValuePair& pair) const
	{
		if(&pair == this)
			return true;
		return _key == pair._key && _value == pair._value;
	}

	friend ostream& operator<<(ostream& o,const MTKeyValuePair& pair)
	{
		pair.print(o);
		return o;
	}

protected:
	virtual void print(ostream& o) const
	{
		o << _key << " = " << _value ;
	}



	public:
		const TKey& getKey() const
		{
			return _key;
		}
		const TValue& getValue() const
		{
			return _value;
		}

		void setValue(const TValue& tValue)
		{
			_value = tValue;
		}

	private:
		TKey _key;
		TValue _value;
};
NS_END_MT_UTIL_CONFIGURATIONS
#endif /* MTKEYVALUEPAIR_H_ */
