/***************************************************************************
 * CopyRight (C)
 * Author: Qianguangpan
 * Description: Design a consumer with queue which lock free
 *
 ***************************************************************************/
#ifndef LOCK_FREE_QUEUE_CONSUMER_H
#define LOCK_FREE_QUEUE_CONSUMER_H
#include <atomic>
#include <thread>
#include <queue>
#include "ThreadSafeLimitQueue.hpp"
#include <functional>
#include "Logging/IMTLog.h"
#include "ConsumeException.h"
#include "Semaphore.h"
using MT::Util::Logging::IMTLog;

template <class T>
class LockFreeQueueConsumer
{
public:
	LockFreeQueueConsumer();
	virtual ~LockFreeQueueConsumer();
	//	绑定一个回调，用以处理收到的数据
	void BindDataDealFunc(std::function<void(T&)>dataDealFunc);
	// 绑定一个批量处理数据的回调
	void BindBulkDataDealFunc(std::function<void(std::queue<T>&)>bulkDataDealFunc);
	//	启动消费线程
	void StartConsuming();
	//	数据到来时调用此函数
	void OnDataArrived(const T& data);
	// 设置队列的容量
	void SetQueueSize(int queueSize);
	// 设置一个logger
	void SetLogger(IMTLog *logger);
	// 设置队列满时候的处理者
	void SetOverFlowHandler(std::function<void(T&)>overFlowHandler);
private:
	static void ConsumeData(void *param);
	void ConsumerDataImp();
	//	关闭消费线程
	void StopConsuming();

	//	标识当前正在写入的队列的编号
	std::atomic<int> m_WriteQueueIndex;
	//	两个队列，生产者和消费者分别使用其中一个，避免竞争
	ThreadSafeLimitQueue<T> m_Queues[2];
	//	消费线程句柄
	std::thread *m_ThrConsume;
	//	标识当前线程是否退出
	std::atomic<bool>m_Exit;
	//	数据处理回调
	std::function<void(T&)>m_DataDealFunc;
	//  提供一个批量处理数据的回调 
	std::function<void(std::queue<T>&)>m_DataBulkDealFunc;

	IMTLog *m_Logger;

	Semaphore m_Semaphore;
};

template <class T>
LockFreeQueueConsumer<T>::LockFreeQueueConsumer()
	:m_ThrConsume(nullptr)
	, m_WriteQueueIndex(0)
	, m_Exit(false)
	, m_DataDealFunc(nullptr)
	, m_Logger(nullptr)
	, m_DataBulkDealFunc(nullptr)
{
	for (auto& que : m_Queues)
		que.SetCapacity(2500);
}


template <class T>
LockFreeQueueConsumer<T>::~LockFreeQueueConsumer()
{
	StopConsuming();
}

template <class T>
void LockFreeQueueConsumer<T>::BindDataDealFunc(std::function<void(T&)>dataDealFunc)
{
	m_DataDealFunc = dataDealFunc;
}

template <class T>
void LockFreeQueueConsumer<T>::BindBulkDataDealFunc(std::function<void(std::queue<T>&)>bulkDataDealFunc)
{
	m_DataBulkDealFunc = bulkDataDealFunc;
}

template<class T>
void LockFreeQueueConsumer<T>::StartConsuming()
{
	m_ThrConsume = new std::thread(&ConsumeData, this);
}

template<class T>
void LockFreeQueueConsumer<T>::StopConsuming()
{
	if (!m_Exit)
	{
		m_Exit = true;
		if (nullptr != m_ThrConsume)
		{
			if (m_ThrConsume->joinable())
				m_ThrConsume->join();
			delete m_ThrConsume;
			m_ThrConsume = nullptr;
		}
	}
}

template <class T>
void LockFreeQueueConsumer<T>::ConsumeData(void *param)
{
	LockFreeQueueConsumer<T> *consumer = (LockFreeQueueConsumer<T>*)param;
	consumer->ConsumerDataImp();
}

template <class T>
void LockFreeQueueConsumer<T>::OnDataArrived(const T& data)
{
	m_Queues[m_WriteQueueIndex].push(data);
	m_Semaphore.Signal();
}

template <class T>
void LockFreeQueueConsumer<T>::ConsumerDataImp()
{
	while (!m_Exit)
	{
		int  readQueueIndex = m_WriteQueueIndex;
		//如果当前这个队列是空的
		if (m_Queues[readQueueIndex].empty())
		{
			//1.所有的数据都消费完：释放时间片，让生产者继续生产；
			if (m_Queues[readQueueIndex ^ 1].empty())
			{
				if (m_Exit)
					break;

				m_Semaphore.Wait();
				continue;
			}
			//2.数据在另外的队列里：直接取另外队列的数据消费即可，生产者继续往当前的队列中生产；
			else
			{
				readQueueIndex ^= 1;
			}
		}
		//当前这个队列有数据，此时消费者占用这个队列，让生产者往另外的队列里生产
		else
		{
			atomic_fetch_xor(&m_WriteQueueIndex, 1);
		}

		//拷贝当前队列数据
		std::queue<T>datas;
		m_Queues[readQueueIndex].swap(datas);

		if (nullptr != m_DataBulkDealFunc)
		{
			try{
				m_DataBulkDealFunc(datas);
			}	
			catch (ConsumeCannotContinue& e) {
				/*can't continue so discard these datas*/
				if (nullptr != m_Logger)
				{
					m_Logger->Log(MTLogLevel::Error, "some exception occored the work can't continue so discard the current queue datas which size is %d", datas.size());
				}
			}
		}
		else if (nullptr != m_DataDealFunc)
		{
		//将当前占用的队列的数据消费完
			while (!datas.empty())
			{
				T& data = datas.front();
				try {
					if (m_DataDealFunc)
						m_DataDealFunc(data);
				}
				catch (ConsumeCannotContinue& e) {
					/*can't continue so discard these datas*/
					if (nullptr != m_Logger)
					{
						m_Logger->Log(MTLogLevel::Error, "some exception occored the work can't continue so discard the current queue datas which size is %d", datas.size());
					}
					break;
				}

				datas.pop();

				if (m_Exit)
					break;
			}
			if (m_Exit)
				break;
		}
	}
}
template<class T>
void LockFreeQueueConsumer<T>::SetQueueSize(int queueSize)
{
	for (auto& que : m_Queues)
		que.SetCapacity(queueSize/2);
}

template<class T>
void LockFreeQueueConsumer<T>::SetLogger(IMTLog *logger)
{
	m_Logger = logger;
	for (auto& que : m_Queues)
		que.SetLogger(m_Logger);
}

template<class T>
void LockFreeQueueConsumer<T>::SetOverFlowHandler(std::function<void(T &)>overFlowHandler)
{
	for (auto& que : m_Queues)
		que.SetOverFlowHandler(overFlowHandler);
}
#endif