#ifndef MT_UTIL_MTTIMEMEASURE_H
#define MT_UTIL_MTTIMEMEASURE_H
#include <Common/MTUtilGlobal.h>
class MT_UTIL_API MTTimeMeasure
{
public:
	MTTimeMeasure();
	~MTTimeMeasure();
	
	static void init();
	void setTriggerTime(int seconds = 5, int minute = 0, int hour = 0);
	bool doCounts();
	float getOperatingEfficiency();
	void getOperatingEfficiency(int *count, double *usedSecond);
	void measureStart();
	float measureEndMilliSecond();
#ifdef _WIN32
	LARGE_INTEGER 
#else
	 uint64_t
#endif
		getCurrentTimeStamp();
private:
	static 
#ifdef _WIN32
		LARGE_INTEGER
#else
		uint64_t
#endif
		s_systemFrequence;

	unsigned int m_currentCounts, m_lastCounts;

#ifdef _WIN32
	LARGE_INTEGER
#else
uint64_t
#endif
		m_curTime, m_lastTime;

	double m_timeSpan;

	unsigned int m_countSpan;

	unsigned int m_triggerTimeSecond;

#ifdef _WIN32
	LARGE_INTEGER
#else
	uint64_t
#endif
		time_start, time_end;
 
};

#endif
