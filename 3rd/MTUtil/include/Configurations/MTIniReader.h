/***************************************************************************
 * CopyRight (C) ShangXichao     
 * Project:   
 * FileName:                                                       
 * Created on: 2011-6-10
 *     Author: 商希超
 * Description:
 * FunctionList:
 * History:
 *   1.Date        : 2011-6-10
 *     Author      : 商希超
 *	   Modification: 
 *
 ***************************************************************************/

#ifndef MTINIMANAGE_H_
#define MTINIMANAGE_H_
#include <string>
#include <map>
#include "Common/MTUtilGlobal.h"
#if defined(__cplusplus)
	
	#include "Common/MTKeyValuePair.h"
	#include <list>
	using namespace std;
#endif //defined(__cplusplus)



#define INIFILE_MAX_LINELEN 25600 /* max number of chars in a line */
#define COMMENT_CHAR '#'        /* signifies a comment */


typedef struct
{
  char tag[INIFILE_MAX_LINELEN];
  char rest[INIFILE_MAX_LINELEN];
} INIFILE_ENTRY;

#if defined(__cplusplus)
         extern  "C" {
#endif
const char* IniManage_afterequal(const char *string);

  /**
   * @brief returns ptr to first non-white in string, or NULL if it's all white
   **/
  char* IniManage_skipwhite(char *string);

  /**
   * @brief Positions file at line after section tag.
   * Returns 0 if section found;-1 if not.
   **/
  int IniManage_findSection(void *fp, const char *section);


  void xToUpper(char* pszString );
  void IniManage_getTagValue2(FILE *fp, const char* section, const char* tag,char* refValue);
  void IniManage_getTagValue(const char* fName, const char* section, const char* tag,char* refValue);

#if defined(__cplusplus)
         }
#endif

#if defined(__cplusplus)
NS_BGN_MT_UTIL_CONFIGURATIONS

class MTIniReader
{
 public:
//		MTIniReader(const char* path);
//		MTIniReader(const char* path,const char* sectionName);
		MTIniReader();
		MTIniReader(const std::string& path);
		MTIniReader(const std::string& path,const std::string& sectionName);
		virtual ~MTIniReader();

/**
 * No Copy Constructure && No operator=
 **/
private:
		MTIniReader(const MTIniReader& reader);
		MTIniReader& operator=(const MTIniReader& reader);
public:
		bool Open();
		bool Open(const std::string& path);
		//bool Open(const char *path);
		bool Close();
		bool IsValid() const;

		public:
		//typedef list<MTKeyValuePair<std::string,std::string> > SectionDictionary;
			typedef std::map<std::string, std::string> SectionDictionary;

		std::string GetTagValue(const std::string& section,const std::string& tag) const;
		std::string GetTagValue(const std::string& tag) const;


		//  SectionDictionary GetSectionContent(const char* section);
		SectionDictionary GetSectionContent(const std::string& section);
		SectionDictionary GetSectionContent();
		bool HasSection(const std::string& sectionName);

 private:
	  FILE* _fp;
	  std::string m_Path;
	  std::string _sectionName;

 public:
		const std::string& getSectionName() const;
		void setSectionName(const std::string& secName);


 private:
  /** @brief if the next non-whitespace character in string is '=', returns
   *  pointer to next non-whitespace after that, or NULL.
   **/
  static const char *afterequal(const char *string);

  /**
   * @brief returns ptr to first non-white in string, or NULL if it's all white
   **/
  static char* skipwhite(char *string);

  /**
   * @brief Positions file at line after section tag.
   * Returns 0 if section found;-1 if not.
   **/
  static int findSection(void *fp, const char *section);



  static std::string getTagValue(void * fp, /* already opened file ptr */
  const char * tag, /* string to find */
  const char * section); /* section it's in */

  static std::string getTagValue2(void *fp,
		  const char *tag,
		  const char *section);


  /**
   * @brief given 'section' and array of strings, fills strings with what was
   * found in the section, one line per string. Comments and blank lines
   * are omitted. 'array' is assumed to be allocated, of 'max' entries
   * of size INIFILE_MAX_LINELEN.
   * @param fp already opened file pointer
   * @param section Name what you want
   * Returns the <Key,Value> Pairs what in the section
   **/
  static SectionDictionary getSectionValue(void * fp, /* already opened file ptr */
  const char* section);
};
NS_END_MT_UTIL_CONFIGURATIONS

#endif //defined(__cplusplus)
#endif /* NS_END_MT_UTIL_CONFIGURATIONS */
