#ifndef MT_IO_CONFIGURATIONS_MTCONFIGMGR_H
#define MT_IO_CONFIGURATIONS_MTCONFIGMGR_H
#include <map>
#if defined(_WIN32)
#include <memory>
#else
#include <tr1/memory>
#endif

#include "Common/MTUtilGlobal.h"
#include "Configurations/IMTConfig.h"

NS_BGN_MT_UTIL_CONFIGURATIONS
enum class MTConfigSourceType {
	UnKnow = 0,
	IniFile  ,
	XmlFile  ,
	JsonFile ,
	LuaFile  ,


	OraclDatabase,
	MysqlDatabase,
	MSSQLDatabase,


	HBase,
	Hive,
	MongoDB,
	Redis,

};
class MT_UTIL_API MTConfigMgr {
public:
	~MTConfigMgr();
private:
	MTConfigMgr();
	MTConfigMgr(const MTConfigMgr&);
	MTConfigMgr& operator=(const MTConfigMgr&);
public:
	typedef std::map<MTConfigSourceType, std::shared_ptr<IMTConfig>> MTConfigSourceMap;
	static MTConfigMgr* GetInstance();
	std::shared_ptr<IMTConfig> GetConfig(MTConfigSourceType sourceType);
private:
	MTConfigSourceMap m_SourceInstanceMap;
};
NS_END_MT_UTIL_CONFIGURATIONS

#endif // !MT_IO_CONFIGURATIONS_MT0CONFIGMGR_H
