#ifndef MT_IO_CONFIGURATIONS_MTNETCONFIGSERVER_H
#define MT_IO_CONFIGURATIONS_MTNETCONFIGSERVER_H
#include <string>
#include <mutex>
#include <map>
#include "Common/MTUtilGlobal.h"
#include "Logging/IMTLog.h"
#include "Proto/Params.pb.h"

NS_BGN_MT_UTIL_CONFIGURATIONS

using MT::Util::Logging::IMTLog;
enum class SettingsType {
  Normal = 1,
  Triple = 2,
  ObjectAttr = 3,
  ImageParam = 4
};

class MT_UTIL_API MTNetConfigServer {
public:
	MTNetConfigServer(IMTLog* logger);
	virtual ~MTNetConfigServer();
private:
	MTNetConfigServer(const MTNetConfigServer&);
	MTNetConfigServer& operator=(const MTNetConfigServer&);
public:
	bool Open(const std::string& host,int port);
    void Refresh(unsigned char* data, int dataSize);
    const Params::ParamsInfo      &GetParamsInfo() const;
    const Params::TripleProducts  &GetTripleProducts() const;
    const Params::ObjectAttribute &GetObjectAttribute() const;
    const Params::ImageParams     &GetImageParams() const;
    const Params::CalibData       &GetCalibData() const;

    Params::ImageParams     &GetImageParamsRef();
    Params::CalibData       &GetCalibDataRef() ;
	IMTLog *GetLogger();
private:
    mutable std::mutex      m_Locker;
    Params::MTData          m_MTData;
    Params::ParamsInfo      m_ParamsInfo;
    Params::TripleProducts  m_TripleProducts;
    Params::ObjectAttribute m_ObjectAttribute;
    Params::ImageParams     m_ImageParams;
    Params::CalibData       m_CalibData;

	IMTLog* m_Logger;

};
NS_END_MT_UTIL_CONFIGURATIONS
#endif // !MT_IO_CONFIGURATIONS_MTNETCONFIGCLIENT_H
