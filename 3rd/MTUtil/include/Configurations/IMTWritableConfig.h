#ifndef MT_IO_CONFIGUREATIONS_IMTWRITABLECONFIG_H
#define MT_IO_CONFIGUREATIONS_IMTWRITABLECONFIG_H
#include <string>
#include <map>
#include "Common/MTIOGlobal.h"
NS_BGN_MT_IO_CONFIGUREATIONS
class IMTWritableConfig {
public:
	IMTWritableConfig() {}
	virtual ~IMTWritableConfig() {}
	typedef std::map<std::string, std::string> SectionDictionary;
private:
	IMTWritableConfig(const IMTWritableConfig&);
	IMTWritableConfig& operator=(const IMTWritableConfig&);
public:
	virtual bool Open(const std::string& connString) = 0;
    
    template< typename T >
    virtual void Write(const std::string& sectionName,const std::string& key,const T& value) = 0;
	
    template<typename T>
    virtual T GetValue(const std::string& sectionName,const std::string& key) const = 0;

    virtual void Commit(const std::string& sectionName) = 0;
};
NS_END_MT_IO_CONFIGUREATIONS
#endif // !MT_IO_CONFIGUREATIONS_IMTWRITABLECONFIG_H