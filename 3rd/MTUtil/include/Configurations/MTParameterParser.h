#ifndef  MT_UTIL_CONFIG_MTPARAMETERPARSER_H
#define MT_UTIL_CONFIG_MTPARAMETERPARSER_H
#include "Common/MTUtilGlobal.h"
#include<stdint.h>
#include <string>
#include <list>
#include <map>
#include "json.hpp"

NS_BGN_MT_UTIL_CONFIGURATIONS

#define KEY_CODE  "code"
#define KEY_MESSAGE  "message"
#define KEY_VERSION  "version"
#define KEY_NAME  "name"
#define KEY_K  "k"
#define KEY_VALID  "valid"
#define KEY_PARAM  "param"
#define KEY_ID  "id"
#define KEY_PID  "pid"
#define KEY_NAME  "name"
#define KEY_NODES  "nodes"
#define KEY_IS_ARRAY	"isArray"
#define KEY_IS_ARRAY	"isArray"
#define KEY_IS_ARRAY_VALUE	"0"
#define KEY_IS_VALID_ID  "isValidId"
#define KEY_UNIT "unit"
#define KEY_REQUIRED "required"
#define KEY_VALUE "value"
using json = nlohmann::json;

class MT_UTIL_API LeafNode
{
public:
	std::string name;// "ThLocalHost",
	std::string isValidId;// : "1",
	std::string unit;// " : "",
	std::string required;// " : "1",
	std::string value;// " : "192.168.1.1"
public:
	bool InitFromJson(nlohmann::basic_json<>  &j);
};

class MT_UTIL_API ParameterParser
{
public:
	int code;
	std::string version;
	std::string name;
	std::string k;
	std::string valid;
	std::string md5;
public:
	typedef std::list<LeafNode*> SettingsList;
	typedef std::map<std::string, SettingsList*> SectionMap;
	typedef std::map<std::string, SectionMap*> ProgramMap;
	bool LoadFromString(std::string& str);
	bool LoadFromFile(std::string& filename);
	void Reset();
	void PrintValue(const std::string& proName);
	bool GetParamValue(std::string& program, SectionMap **map);
private:
	bool InitFromJson();
private:
	ProgramMap  m_ProgramMap;
	json m_Parser;
};

NS_END_MT_UTIL_CONFIGURATIONS

#endif // ! MT_UTIL_CONFIG_MTPARAMETERPARSER_H