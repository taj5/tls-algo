#ifndef MT_IO_CONFIGURATIONS_MTNETCONFIGCLIENT_H
#define MT_IO_CONFIGURATIONS_MTNETCONFIGCLIENT_H
#include <string>
#include <map>
#include <mutex>
#include "Proto/Params.pb.h"
#include "Common/MTUtilGlobal.h"
#include "Net/ISocketEvtSubscription.h"
#include "Logging/IMTLog.h"
//#include "Logging/MTLogMgr.h"

NS_BGN_MT_UTIL_CONFIGURATIONS
typedef struct HostItem{
    std::string Host;
    int Port;
}MTHostItem;

enum class SettingsType {
  Normal = 1,
  Triple = 2,
  ObjectAttr = 4,
  ImageParam = 8
};
using MT::Util::Logging::IMTLog;
class MT_UTIL_API MTNetConfigClient {
public:
	MTNetConfigClient(IMTLog* logger);
	virtual ~MTNetConfigClient();
private:
	MTNetConfigClient(const MTNetConfigClient&);
	MTNetConfigClient& operator=(const MTNetConfigClient&);
public:
	//bool Open(const std::vector<MTHostItem>& hostList);
    void AddHost(const std::string& host,int port);
    void Commit(SettingsType settingsType);
    Params::ParamsInfo      &GetParamsInfo();
    Params::TripleProducts  &GetTripleProducts();
    Params::ObjectAttribute &GetObjectAttribute();
    Params::ImageParams     &GetImageParams();
    Params::CalibData       &GetCalibData();



private:
    std::vector<MTHostItem> m_HostList;
    Params::MTData          m_MTData;
    Params::ParamsInfo      m_ParamsInfo;
    Params::TripleProducts  m_TripleProducts;
    Params::ObjectAttribute m_ObjectAttribute;
    Params::ImageParams     m_ImageParams;
    Params::CalibData       m_CalibData;
    MT::Util::Logging::IMTLog *m_Logger;
	std::map<IOHandle, MTHostItem>  m_CommitFDs;
	std::mutex				m_FDLocker;


protected:
	void addCommitFD(IOHandle fd,const MT::Util::Configurations::MTHostItem& hostItem);
	void removeCommitFD(IOHandle fd);
	std::map<IOHandle,MTHostItem> getReverseCommitFD();
	void clearCommitFDs();

	class RecvObj : public MT::Util::Net::ISocketEvtSubscription
	{
	public:
		RecvObj(MTNetConfigClient* cfgClient,IMTLog *logger);
	private:
		MTNetConfigClient * m_Client;
		IMTLog			  * m_Logger;
		void OnDataReceived(IOHandle fd, unsigned char* data, int dataSize, void* userdata);
	};
};
NS_END_MT_UTIL_CONFIGURATIONS
#endif // !MT_IO_CONFIGURATIONS_MTNETCONFIGCLIENT_H