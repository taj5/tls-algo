#ifndef MT_UTIL_CONFIG_MTREDISCONFIG_H
#define MT_UTIL_CONFIG_MTREDISCONFIG_H
#include "Common/MTUtilGlobal.h"
#include "Configurations/IMTConfig.h"
#include "Net/MTRedisClient.h"
#include "Parser/MTParser.h"
NS_BGN_MT_UTIL_CONFIGURATIONS
using MT::Util::Net::MTRedisClient;
using MT::Util::Net::ConnectionObject;
class MT_UTIL_API MTRedisConfig :public IMTConfig {
public:
	MTRedisConfig();
	~MTRedisConfig();

public:
	virtual bool Open(const std::string& connString);
	virtual bool Load(const std::string& sectionName);
	/**
	返回false有两种情况：
	1、server连接失败
	2、数据库中没有值
	**/
	virtual bool HasKey(const std::string&) const;
	virtual std::string GetValue(const std::string&) const;
	virtual std::string GetValue(const std::string& sectionName, const std::string& key) const;
	virtual SectionDictionary GetSectionSettings(const std::string& sectionName) const;
private:
	MTRedisClient m_RedisCli;
	ConnectionObject m_connObject;
};
NS_END_MT_UTIL_CONFIGURATIONS
#endif // !MT_UTIL_CONFIG_MTREDISCONFIG_H
