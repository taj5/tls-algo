#ifndef MT_IO_CONFIGURATIONS_IMTCONFIG_H
#define MT_IO_CONFIGURATIONS_IMTCONFIG_H
#include <string>
#include <map>
#include "Common/MTUtilGlobal.h"
NS_BGN_MT_UTIL_CONFIGURATIONS
class IMTConfig {
public:
	IMTConfig() {}
	virtual ~IMTConfig() {}
	typedef std::map<std::string, std::string> SectionDictionary;
private:
	IMTConfig(const IMTConfig&);
	IMTConfig& operator=(const IMTConfig&);
public:
	virtual bool Open(const std::string& connString) = 0;
	virtual bool Load(const std::string& sectionName) = 0;
	virtual bool HasKey(const std::string&) const = 0;
	virtual std::string GetValue(const std::string&) const = 0;
	virtual std::string GetValue(const std::string& sectionName, const std::string& key)const = 0;
	virtual SectionDictionary GetSectionSettings(const std::string& sectionName) const = 0;
};
NS_END_MT_UTIL_CONFIGURATIONS
#endif // !MT_IO_CONFIGURATIONS_IMTCONFIG_H