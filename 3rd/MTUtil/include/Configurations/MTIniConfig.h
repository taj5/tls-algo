//
// Created by Shangxichao on 2018/3/20.
//
#ifndef MT_IO_CONFIGURATIONS_MTINICONFIG_H
#define MT_IO_CONFIGURATIONS_MTINICONFIG_H
#include "Common/MTUtilGlobal.h"
#include "Configurations/IMTConfig.h"
#include "Configurations/MTIniReader.h"
NS_BGN_MT_UTIL_CONFIGURATIONS
class MT_UTIL_API MTIniConfig :public IMTConfig {
public:
	MTIniConfig();
	~MTIniConfig();
public:
	// ͨ�� IMTConfig �̳�
	bool Open(const std::string& connString) override;
	bool Load(const std::string &) override;
	bool HasKey(const std::string &) const override;
	std::string GetValue(const std::string &)const override;
	std::string GetValue(const std::string &, const std::string &)const override;
	SectionDictionary GetSectionSettings(const std::string& sectionName) const override;
private:
	MTIniReader m_IniReader;
	bool m_Opened;
	typedef std::map<std::string, SectionDictionary> SectionConfigMap;
	SectionConfigMap m_SectionConfigMap;
	std::string m_CurrentSection;
};
NS_END_MT_UTIL_CONFIGURATIONS

#endif // !MT_IO_CONFIGURATIONS_MTINICONFIG_H
