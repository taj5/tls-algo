#ifndef MT_IO_PARSER_MTPARSER_H
#define MT_IO_PARSER_MTPARSER_H
#include <string>
#include "Common/MTUtilGlobal.h"

NS_BGN_MT_UTIL_NET
enum class ProtoType{
    TCP,
    UDP,
    MQ,
	CACHE
};

enum class OperationType {
	Concurrency,
	Queue,
	Sync
};

class UserInfoClass
{
	std::string userName;
	std::string userPwd;
	std::string ip;
	int port;
};

class MT_UTIL_API ConnectionObject {
public:
	ConnectionObject():
	m_Type(ProtoType::MQ)
	,m_Name("whoiam")
	,m_Password("")
	,m_Host("127.0.0.1")
	,m_Port(6379)
	,m_ConnectionTimeout(1000)
	,m_ReadTimeout(100)
	,m_WriteTimeout(100)
	,m_OpType(OperationType::Sync)
	,m_ScheduleParams(0)
	,m_Channel("report-lcipper"){};
	~ConnectionObject(){};
public:
	bool Parse(const std::string &connstring);
public:
    ProtoType m_Type;
	std::string m_Name;
	std::string m_Password;
    std::string m_Host;
    int m_Port;
    int m_ConnectionTimeout;
    int m_ReadTimeout;
    int m_WriteTimeout;
	OperationType m_OpType;
	void *m_ScheduleParams;
	std::string m_Channel;
};

/**
----Udp://127.0.0.1:8090;ConnectionTimeout=1;ReadTimeout=3;WriteTimeout=3;Type=Sync;
----Tcp://127.0.0.1:8090;ConnectionTimeout=1;ReadTimeout=3;WriteTimeout=3;Type=Concurrency;
----Mq://127.0.0.1:8090;ConnectionTimeout=1;ReadTimeout=3;WriteTimeout=3;Type=Queue;
**/

class MT_UTIL_API MTParser
{
public:
	//MTParser();
	//~MTParser();
public:
	bool Parse(const std::string &connect_string, ConnectionObject& connection_object);
};

NS_END_MT_UTIL_NET
#endif