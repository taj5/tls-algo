//
// Created by Shangxichao on 2018/3/14.
//

#ifndef MT_IO_Net_TCP_MTCLIENT_H
#define MT_IO_Net_TCP_MTCLIENT_H


#include <stdint.h>
#include <event.h>
#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/util.h>

#include <map>
#include <string>
#include <memory>
#include "Common/MTUtilGlobal.h"
#include "Net/ISocketEvtSubscription.h"
#include "Net/MTClientDataBuffer.h"
#include <thread>

#include "Logging/IMTLog.h"

using MT::Util::Logging::IMTLog;
using MT::Util::Logging::MTLogLevel;

NS_BGN_MT_UTIL_NET_TCP

	using MT::Util::Net::ClientDataBuffer;
	class MT_UTIL_API ClientMap
	{	
	private:
		ClientMap();
	public:
		virtual ~ClientMap();
	public:
		typedef std::map<IOHandle, std::shared_ptr<ISocketEvtSubscription>>  ClientMapData;
		static ClientMap& GetInstance();
		void Add(IOHandle fd, std::shared_ptr<ISocketEvtSubscription> subs);
		void Remove(IOHandle fd);
		std::shared_ptr<ISocketEvtSubscription> Get(IOHandle fd);
	private:
		ClientMapData m_ClientMap;
	};

	typedef enum {
		ClientStatus_UnKnow = 0,
		ClientStatus_Closed = 1,
		ClientStatus_Opened = 2
	}ClientStatus;

	class MT_UTIL_API MTTcpClient {
	public:
		//MTTcpClient();
		MTTcpClient(IMTLog* Logger);
		~MTTcpClient();
		MTTcpClient(const MTTcpClient&) = delete;
		MTTcpClient& operator=(const MTTcpClient&) = delete;
	public:
		bool Open(const char *host, int port, std::shared_ptr<ISocketEvtSubscription> tcpEvtSubscription);
		bool ReOpen();
		bool IsOpened() const;
		int Send(const char* data, int dataSize,const int retryTimes = 5);
		void Close();
		void SetFD(IOHandle fd);
		IOHandle GetFD();
		ClientStatus GetStatus();
		ClientDataBuffer* GetDataBuffer();
		bool GetStopFlag();
		void SetStopFlag(bool flag);
	void setConnect_timeout(int connect_timeout);
	private:
		void setStatus(ClientStatus status);
		static void onRead(struct bufferevent *bev, void *ctx);
		static void onEvent(struct bufferevent *bev, short what, void *ctx);
		static void onTimeoutEvt(evutil_socket_t fd, short events, void *arg);
		static void txProc(void* arg);
		char* pack(const char* data, int dataSize, int* totalSize);
		void packFree(char* sendBuf);

	protected:
		virtual void onReadProc(struct bufferevent* bev, void* ctx);
		virtual int  onSendProc(const char* data,int dataSize,const int retryTimes = 5);
	protected:
		IOHandle m_Fd;
		ClientStatus m_CltStatus;
		IMTLog* m_Logger;

	private:
		uint32_t m_ServerIp;
		int m_Port;
		std::shared_ptr<ISocketEvtSubscription> m_TcpEvtHandler;
		ClientDataBuffer m_CltBuffer;
		std::thread* m_pThread;
		bool m_StopFlag;
		event_base* m_CltEvtBase;
		int connect_timeout;
	};

NS_END_MT_UTIL_NET_TCP
#endif //MT_IO_NET_TCP_MTCLIENT_H
