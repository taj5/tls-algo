#ifndef MT_RAWTCPCLIENT_H_
#define MT_RAWTCPCLIENT_H_

#include "Common/MTUtilGlobal.h"
#include "Net/Tcp/MTTcpClient.h"

NS_BGN_MT_UTIL_NET_TCP
using MT::Util::Net::Tcp::MTTcpClient;
using MT::Util::Logging::IMTLog;
class MT_UTIL_API RawTcpClient:public MTTcpClient{
    public:
     RawTcpClient(IMTLog* logger);        
     ~RawTcpClient();
     void SetLogger(IMTLog* logger);
    protected:
        void onReadProc(struct bufferevent* bev, void* ctx);
		int  onSendProc(const char* data,int dataSize,const int retryTimes = 5);
};
NS_END_MT_UTIL_NET_TCP
#endif //MT_RAWTCPCLIENT_H_