//
// Created by Shangxichao on 2018/3/14.
//

#ifndef MT_IO_Net_TCP_MTSERVER_H
#define MT_IO_Net_TCP_MTSERVER_H
#include <stdint.h>
#include <event.h>
#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/util.h>
#include <thread>
#include <mutex>
#include <map>
#include "Common/MTUtilGlobal.h"
#include "Net/ISocketEvtSubscription.h"
#include "MTTcpClient.h"

using MT::Util::Net::ISocketEvtSubscription;
typedef evutil_socket_t IOHandle;
NS_BGN_MT_UTIL_NET_TCP
    class MT_UTIL_API MTTcpServer{
        public:
            //MTTcpServer();
			MTTcpServer(IMTLog* logger);
		private:
            MTTcpServer(const MTTcpServer&);
            MTTcpServer& operator=(const MTTcpServer&);
		public:
			~MTTcpServer();
        public:
            typedef std::map<IOHandle,ClientDataBuffer*> MTTcpClientInfo;
			typedef std::map<IOHandle, evbuffer*> MTTcpClientBuf;
            static MTTcpServer& GetInstance(IMTLog* logger = NULL);
            bool Start(const char* host,int port, std::shared_ptr<ISocketEvtSubscription> dataHandler);
            void Stop();
            int  Send(IOHandle fd,const unsigned char* data,uint32_t size);
            bool IsStarted();
			std::string GetHost() const;
			int GetPort() const;
	void setConnect_timeout(int connect_timeout);
		//private:
		//	char* unpack(char* data, int dataSize, int* contentSize);
        protected:
            void setStarted(bool isStarted);
			bool getStopFlag();
			void setStopFlag(bool isStopped);

            void doClientConnect(IOHandle cltFD);
            void doClientDisconnect(IOHandle cltFD);
			void clearClientData();
			ClientDataBuffer* getDataBuffer(IOHandle cltFD);

			void AddClientBuf(IOHandle cltfd);
			void RemoveClientBuf(IOHandle cltfd);
			void clearClientBuf();
			evbuffer* getDataBuf(IOHandle cltfd);

            static void doStartServer(MTTcpServer* tcpSrv);
            static void cbOnSrvEvt(bufferevent *bev, short events, void *arg);
            static void cbOnSrvAccept(struct evconnlistener* listener,
                      evutil_socket_t fd,
                      struct sockaddr *address,
                      int socklen,void *arg);

			static void cbOnTimeoutEvt(evutil_socket_t fd, short events, void *arg);

            static void cbOnSrvRead(struct bufferevent* bev,void* arg);          
			static void cbOnSrvReadEx(struct bufferevent* bev, void* arg);
        protected:
            MTTcpClientInfo m_ClientMap;
			MTTcpClientBuf m_ClientBufMap;/**    用evbuffer替换ClientDataBuffer    **/
            std::string     m_Host;
            int             m_Port;
            event_base*     m_SrvEvtBase;
            uint8_t         m_Started;
			uint8_t			m_StopFlag;
            std::thread     *m_TSrvProc;
            std::mutex      m_StatusLocker;
			std::shared_ptr<ISocketEvtSubscription> m_DataHandler;
			IMTLog* m_Logger;
			evbuffer* m_Evbuffer;
            int connect_timeout;
    };
NS_END_MT_UTIL_NET_TCP

#endif //MT_IO_Net_TCP_MTSERVER_H
