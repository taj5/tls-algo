//
// Created by Shangxichao on 2018/3/14.
//

#ifndef MT_IO_NET_TCP_ITCPEVTSUBSCRIPTION_H
#define MT_IO_NET_TCP_ITCPEVTSUBSCRIPTION_H
#include "Common/MTIOGlobal.h"
NS_BGN_MT_IO_NET_TCP

class MT_NET_API ITcpEvtSubscription{
public: 
    //ITcpEvtSubscription();
    //ITcpEvtSubscription(const ITcpEvtSubscription&);
    //virtual ITcpEvtSubscription&operator=(const ITcpEvtSubscription&);
	virtual ~ITcpEvtSubscription() {};
    virtual void OnDataReceived(IOHandle fd, unsigned char* data,int dataSize,void* userdata)  = 0;
    virtual void OnConnectionStatusChanged(int connFlg) = 0;
};       

NS_END_MT_IO_NET_TCP

#endif //MT_IO_NET_TCP_ITCPEVTSUBSCRIPTION_H
