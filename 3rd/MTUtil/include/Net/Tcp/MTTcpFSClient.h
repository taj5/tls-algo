//
// Created by Shangxichao on 2018/3/14.
//

#ifndef MT_IO_Net_TCP_MTTCPRAWCLIENT_H
#define MT_IO_Net_TCP_MTTCPRAWCLIENT_H


#include <stdint.h>
#include <string>

#include "Logging/IMTLog.h"
#include "Logging/MTLogMgr.h"

using MT::Util::Logging::MTLogMgr;
using MT::Util::Logging::IMTLog;
using MT::Util::Logging::MTLogLevel;

NS_BGN_MT_UTIL_NET_TCP

typedef enum {
	FSClientStatus_UnKnow = 0,
	FSClientStatus_Closed = 1,
	FSClientStatus_Opened = 2
}FSClientStatus;

class MT_UTIL_API MTTcpFSClient {
public:
	//MTTcpFSClient();
	MTTcpFSClient(IMTLog* logger);
	~MTTcpFSClient();
	MTTcpFSClient(const MTTcpFSClient&) = delete;
	MTTcpFSClient& operator=(const MTTcpFSClient&) = delete;
public:
	bool Open(const char *host, int port);
	bool ReOpen();
	bool IsOpened() const;
	int Send(const char* data, int dataSize, const int retryTimes = 5);
	void Close();
	IOHandle GetIOHandle();
	void setConnect_timeout(int connect_timeout);
private:
	char* pack(const char* data, int dataSize, int* totalSize);
	void packFree(char* sendBuf);
private:
	IOHandle m_Fd;
	FSClientStatus m_CltStatus;
	std::string m_SrvHost;
	int m_SrvPort;
	IMTLog* m_Logger;
	int connect_timeout;
};

NS_END_MT_UTIL_NET_TCP
#endif //MT_IO_NET_TCP_MTTCPRAWCLIENT_H