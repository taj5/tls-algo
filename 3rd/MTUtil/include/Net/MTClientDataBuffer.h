//
// Created by Shangxichao on 2018/3/14.
//

#ifndef MT_IO_Net_MTCLIENTDATABUFFER_H
#define MT_IO_Net_MTCLIENTDATABUFFER_H
#include <stdint.h>
#include "Common/MTUtilGlobal.h"
#include "Net/MTNetCommon.h"

NS_BGN_MT_UTIL_NET
class MT_UTIL_API ClientDataBuffer {
				public:
					ClientDataBuffer();
					~ClientDataBuffer();
				private:
					ClientDataBuffer(const ClientDataBuffer&);
					ClientDataBuffer& operator=(const ClientDataBuffer&);
				public:
					void SetFullSize(int fullSize);
					uint8_t* GetDataBuffer();
					uint8_t* GetOffsetBuffer();
					int32_t GetFullSize()const { return m_FullSize; }
					int32_t GetRcvOffset()const { return m_RcvOffset; }
					int32_t GetNeedRead() const;
					void AppendNRead(int nRead);
					bool IsFull() const;
					bool IsEmpty() const;

					uint8_t* GetBufferSize();
					uint8_t* GetHead();
					uint8_t* GetTail();
					int GetSizeOffset();
					int GetHeadOffset();
					int GetTailOffset();
					void ResetHeadOffset();
					void ResetTailOffset();
					void ResetSizeOffset();
					void AddupSizeOffset(int nRead);
					void AddupHeadOffset(int nRead);
					void AddupTailOffset(int nRead);
					//void CheckReserveAndReset();

					
				private:
					uint8_t* m_RcvBuff;	/**    接收的数据首地址    **/
					int m_RcvOffset;
					int m_FullSize;

					uint8_t m_BufferSize[4]; /**    接收数据的长度    **/
					int m_SizeOffset;     /**    数据长度偏移    **/
					uint8_t m_Head[HeadLength];
					int m_HeadOffset;
					uint8_t m_Tail[TailLength];
					int m_TailOffset;
					const int m_BufferUnit;
					//const int m_BufferExtend;
					int m_ActualSize;
				};
NS_END_MT_UTIL_NET
#endif //MT_IO_Net_MTCLIENTDATABUFFER_H