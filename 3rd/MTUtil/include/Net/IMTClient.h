//
// Created by Shangxichao on 2018/3/14.
//

#ifndef MT_IO_Net_IMTCLIENT_H
#define MT_IO_Net_IMTCLIENT_H

#include <memory>
#include "Common/MTUtilGlobal.h"
#include "ISocketEvtSubscription.h"
NS_BGN_MT_UTIL_NET

typedef enum {
	ClientStatus_UnKnow = 0,
	ClientStatus_Closed = 1,
	ClientStatus_Opened = 2
}ClientStatus;

class MT_UTIL_API IMTClient {
public:
	virtual bool Open(const char *host, int port) = 0;
	virtual void RegisterCallback(std::shared_ptr<ISocketEvtSubscription> tcpEvtSubscription) = 0;
	virtual void SendTimes(int times) = 0;
	virtual bool ReOpen() = 0;
	virtual bool IsOpened() const = 0;
	virtual int Send(const char* data, int dataSize) = 0;
	virtual void Close() = 0;
};

NS_END_MT_UTIL_NET
#endif //MT_IO_NET_IMTCLIENT_H
