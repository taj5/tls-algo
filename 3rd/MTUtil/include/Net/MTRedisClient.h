#ifndef MT_IO_REDISCLIENT_H
#define MT_IO_REDISCLIENT_H

#include "Common/MTUtilGlobal.h"
#include <vector>
#include <string>
#include <thread>
#include <mutex>
#include <map>
#include "ISocketEvtSubscription.h"

#include "Parser/MTParser.h"
#include "Common/CommonCode.h"

struct redisContext;
struct redisReply;


NS_BGN_MT_UTIL_NET

class  RedisReplyCleaner
{
public:
	RedisReplyCleaner(redisReply *reply);
	virtual ~RedisReplyCleaner();
private:
	redisReply *reply_;
};

class MT_UTIL_API MTRedisClient
{
public:
	MTRedisClient();
	virtual ~MTRedisClient();
public:
	bool Open(const std::string &constr);
	bool Connect(const char *ip, int port, const struct timeval& tv);
	bool Set(const char* key, const char* value);
	bool SetBinary(const char* key, const void* binary_addr, size_t binary_size);
	bool SetBinary(const char* key, const void* binary_addr, size_t binary_size, int64_t millisecond);
	bool SetExpire(const char* key, int64_t millisecond);
	
	/**
	 @brief 设置Reids键值对。
	 @param[in] key		键名。
	 @param[in] value	键值，可以是字符串，也可以是二进制，二进制数据需要先转化为 std::string，注意长度设置正确。
	 @param[in] millisecond 过期时间 以毫秒为单位
	 @return 返回操作是否成功
	 @note 使用之前必须调用 Open (或者Connect、Login)打开连接。
	 @example:
		 @code
			1.二进制数据
			struct BinaryData
			{
				int data;
			};

			BinaryData binary_data;
			binary_data.data = 1;
			std::string binary_str((char*)&binary_data, sizeof(binary_data));
			redis_client_.Set("key", binary_str, 1);

			2.字符串
			std::string value = "string";
			redis_client_.Set(“key”, value, 1);
		 @endcode
	**/
	bool Set(const std::string& key, const std::string& value, int64_t millisecond);

	// shihaosen 20190703
	int  MSet(std::map<std::string, std::string>&values, int64_t millisecond);
	// shihaosen 20190703  different: this functions knows which key get value failed
	int  MGetEx(std::vector<std::string>&keys, std::vector<std::string>&values) const;

	/**
	 @brief 获取Reids键值。
	 @param[in]  key	键名。
	 @param[out] value	转化为 std::string的键值。
	 @return     -1 失败；0 没有取到键值；正整数 std::string 的大小
	 @note 使用之前必须调用 Open (或者Connect、Login)打开连接。
	 @example:
		@code
			std::string value;
			redis_client_.Get(“key”, value);
		@endcode
	**/
	int  Get(const char* key, std::string& value) const;

	int  MGet(std::vector<std::string>&keys, std::map<std::string, std::string>&values) const;

	bool Delete(const char* key);
	long long Increase(const char* key);
	void Add2SortedSet(const char* key, const char*score, const char* member);
	void Add2List(const char*key, const char* value);
	void Add2List(const char*key, const std::vector<std::string>&values);
	void Add2Set(const char*key, const char* value);
	bool Publish(const char* channel_name, const char* message);
	bool Subscribe(const char* channel_name);
	void GetContentSubscribed(std::string& message);
	bool ReConnect() const;
	bool Close();
	bool SetIOTimeOut(const struct timeval& tv);
	bool StartChannelReadingThread();
	void StopChannelReadingThread();
	bool IsChannelReadingThreadStarted();
	bool Login(const char* password);
	void SetSocketEvtSubscription(ISocketEvtSubscription *socket_evt_subscription);
	bool IsDisconnected();
	bool Save();
protected:
	const bool IsContextValid() const;
	const bool IsStartupSucceed() const;
private:
	redisContext* redis_content_;
	ISocketEvtSubscription *socket_evt_subscription_;
	bool channel_reading_thread_started_;
	std::thread *reading_channel_;
	std::mutex mutex_;
	bool startup_succeed_;
	MTParser constr_parser_;
	ConnectionStatus connection_status_;
private:
	static void *ReadingChannel(void *param);
	void *ReadingChannelProc();
#ifdef _WIN32
	bool Startup();
	void Cleanup();
#endif
};

NS_END_MT_UTIL_NET


#endif //MT_IO_REDISCLIENT_H