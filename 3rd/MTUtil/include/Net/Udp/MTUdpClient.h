//
// Created by Wuhao on 2018/3/19.
//

#ifndef MT_IO_Net_UDP_MTUDPCLIENT_H
#define MT_IO_Net_UDP_MTUDPCLIENT_H


#include <stdint.h>
#include "Common/MTUtilGlobal.h"
#if !defined (_WIN32)       
#include <sys/socket.h>
       #include <netinet/in.h>
       #include <arpa/inet.h>
        #include <unistd.h>
#endif

#include "Logging/IMTLog.h"

using MT::Util::Logging::IMTLog;
using MT::Util::Logging::MTLogLevel;

NS_BGN_MT_UTIL_NET_UDP

class MT_UTIL_API MTUdpClient
{
public:
	//MTUdpClient();
	MTUdpClient(IMTLog* logger);
	~MTUdpClient();
	MTUdpClient(const MTUdpClient&) = delete;
	MTUdpClient& operator=(const MTUdpClient&) = delete;
public:
	bool Open(const char* host, int port);
	int Send(const char* data, int size);
	void Close();
	IOHandle GetIOHandle();
	void setConnect_timeout(int connect_timeout);
private:
	IOHandle m_Fd;
	bool m_UdpCltOpened;
	struct sockaddr_in m_SrvAddr;
	IMTLog* m_Logger;
	int connect_timeout;
};

NS_END_MT_UTIL_NET_UDP


#endif
