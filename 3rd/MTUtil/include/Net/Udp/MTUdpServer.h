//
// Created by Wuhao on 2018/3/19.
//

#ifndef MT_IO_Net_UDP_MTUDPSERVER_H
#define MT_IO_Net_UDP_MTUDPSERVER_H

#include "Common/MTUtilGlobal.h"
#include <stdint.h>
#include <thread>
#include <memory>
#include "Logging/IMTLog.h"
#include "Net/ISocketEvtSubscription.h"
using MT::Util::Logging::IMTLog;
#if !defined (_WIN32)       
#include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <unistd.h>
#endif


NS_BGN_MT_UTIL_NET_UDP

#define READ_BUFFER_SIZE 16384
#define HOST_SIZE 64

//class MT_UTIL_API IUdpEvtSubscription {
//public:
//	virtual ~IUdpEvtSubscription() {};
//	//virtual void  OnReadHandler(void* data, int size) = 0;
//	virtual void OnDataReceived(IOHandle fd, unsigned char* data, int dataSize, void* userdata) = 0;
//};

class MT_UTIL_API MTUdpServer
{
public:
	//MTUdpServer();
	MTUdpServer(IMTLog* logger);
	~MTUdpServer();
	MTUdpServer(const MTUdpServer&) = delete;
	MTUdpServer& operator=(const MTUdpServer&) = delete;

public:
	bool Start(const char* host, int port, std::shared_ptr<ISocketEvtSubscription> readHandler, void* userdata = 0);
	void Stop();
	bool IsStarted();
	std::string GetHost() const;
	int GetPort() const;
	
private:
	static void doStartServer(MTUdpServer* udpSrv);
	static void onRead(IOHandle sock, short int which, void *arg);
	static void onTimeout(IOHandle sock, short int which, void *arg);
	void setStarted(bool isStated);
	void clearReadBuf();
	char* getReadBuf();
	int getReadBufSize() const;
	bool GetStopFlag();
	void SetStopFlag(bool isStop);

private:
	bool 				m_Started;
	IOHandle			m_Fd;
	struct sockaddr_in	m_LocalAddr;
	std::thread		   *m_TSrvProc;
	//OnReadHandler       m_ReadCb;
	std::shared_ptr<ISocketEvtSubscription> m_ReadCb;
	char m_Host[HOST_SIZE];
	int m_Port;
	char m_ReadBuf[READ_BUFFER_SIZE];//16K
	IMTLog *m_Logger;
	bool m_StopFlag;
	int  m_connect_timeout;
public:
	int getConnect_timeout() const;

private:
	struct event_base* m_EventBase;
public:
	void setConnect_timeout(int connect_timeout);
};

NS_END_MT_UTIL_NET_UDP


#endif
