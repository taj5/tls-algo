#include "Common/MTUtilGlobal.h"

NS_BGN_MT_UTIL_NET
#define HeadLength 2
#define TailLength 2
extern char TCP_HEAD[HeadLength];
extern char TCP_TAIL[TailLength];

NS_END_MT_UTIL_NET