//
// Created by Shangxichao on 2018/3/14.
//

#ifndef MT_IO_NET_ISOCKETEVTSUBSCRIPTION_H
#define MT_IO_NET_ISOCKETEVTSUBSCRIPTION_H
#include "Common/MTUtilGlobal.h"
NS_BGN_MT_UTIL_NET

class MT_UTIL_API ISocketEvtSubscription{
public: 
	virtual ~ISocketEvtSubscription() {};
    virtual void OnDataReceived(IOHandle fd, unsigned char* data,int dataSize,void* userdata)  = 0;
    virtual void OnConnectionChanged(int connFlg) = 0;
};       

NS_END_MT_UTIL_NET

#endif //MT_IO_NET_ISOCKETEVTSUBSCRIPTION_H
