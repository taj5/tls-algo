#ifndef MT_UTIL_IO_MTMQConsumer
#define MT_UTIL_IO_MTMQConsumer

#include "IO/IMTConsumerSourceBase.h"

using MT::Util::Net::ISocketEvtSubscription;

NS_BGN_MT_UTIL_IO


class MTMQConsumer :public IMTConsumerSourceBase, public ISocketEvtSubscription
{
public:
	MTMQConsumer(const std::string& host,
		int port,
		const std::string&password,
		const std::string&channel_name,
		int read_timeout,
		int connect_timeout);
	virtual ~MTMQConsumer();
	MTMQConsumer(const MTMQConsumer&) = delete;
	MTMQConsumer& operator=(const MTMQConsumer&) = delete;
public:
	virtual bool Open();
	virtual bool ReOpen();
	virtual bool Close();
	virtual StatusInfo * GetStatistics();
	virtual bool Register(IMTIOCallback * cb);
	virtual void SetConsumeScheduling(IMTConsumerSchedule* schedule);
	virtual IMTConsumerSchedule *GetConsumerSchedule();
	virtual bool Start();
	virtual void OnDataReceived(IOHandle fd, unsigned char* data, int data_size, void* user_data);
	virtual void OnConnectionChanged(int connFlg);

private:
	MT::Util::Net::MTRedisClient redis_client_;
	std::string channel_name_;
	std::string password_;
};
NS_END_MT_UTIL_IO

#endif // !MT_UTIL_IO_IMTCOMSUMERSOURCEBASE