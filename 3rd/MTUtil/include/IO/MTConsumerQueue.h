#ifndef MT_UTIL_IO_CONSUMERQUEUE_H
#define MT_UTIL_IO_CONSUMERQUEUE_H

#include <common/MTUtilGlobal.h>
#include <IO/MTIOBuffer.h>
#include <IO/IMTConsumerSchedule.h>

#include <thread>
#include <mutex>
#include <queue>
NS_BGN_MT_UTIL_IO
class MTConsumerQueue :public IMTConsumerSchedule {
public:
	MTConsumerQueue();
	~MTConsumerQueue();

public:
	void Register(ConsumerCallback cb);
	bool Start();
	void Stop();
	void Process(void* data, int dataSize);
	bool GetStopFlag();
	void SetStopFlag(bool stop);
	void SetQueueSize(int size);
	Statistic& GetStatistic();/**    获得统计信息    **/
private:
	static void doConsume(MTConsumerQueue* consumerQueue);
	bool push(void* data, int datasize);
	bool pop(MTIOBuffer& buf);

private:
	ConsumerCallback m_ConsumerFunc;
	int m_QueueSize;
	std::queue<MTIOBuffer> m_Queue;
	std::mutex m_QueueLock;
	std::thread* m_PopThread;
	bool m_StopFlag;
	std::mutex m_StopFlagLock;
	//int m_RecvNum;
	//int m_LostNum;
	//int m_ProcNum;
	Statistic m_Statistic;
};


NS_END_MT_UTIL_IO

#endif // !MT_UTIL_IO_CONSUMERQUEUE_H