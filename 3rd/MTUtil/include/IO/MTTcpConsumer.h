/***********************************************************************
 * Module:  MTQueueConsumer.h
 * Author:  shihaosen
 * Modified: 2019年1月2日 8:58:12
 * Purpose: Declaration of the class MTQueueConsumer
 * Comment: 正常流程
 *    1.启动线程
 *    2.出栈->回调
 *    其他：1.停止；2.状态查询；
 ***********************************************************************/

#if !defined(__TDSUMLClassDiagram2_MTQueueConsumer_h)
#define __TDSUMLClassDiagram2_MTQueueConsumer_h

#include <IO/IMTConsumerSchedule.h>
#include <IO/MTIOBuffer.h>
#include <queue>
#include <thread>
#include <mutex>
#include <common/MTAtomicSemaphore.h>
#include <thread>
#include <common/MTTimeMeasure.h>
NS_BGN_MT_UTIL_IO
struct	QueueConsumerStruct
{
	int MaxQueueSize;
};
class MTQueueConsumer : 
	public IMTConsumerSchedule
{
	static void  run(void *param);
public:
	StatusInfo * GetStatus(void);
   void RegisterEvent(IMTIOCallback * cb);
   bool Init(void * param);
   bool Process(MTIOBuffer * data);
   void Start(void);
   void Stop(void);
   MTQueueConsumer();
   ~MTQueueConsumer();
   MTQueueConsumer(const MTQueueConsumer&) = delete;
   MTQueueConsumer& operator=(const MTQueueConsumer&) = delete;

protected:
private:
   /**    callback     **/
   IMTIOCallback *m_Callback;

   /**    data queue FIFO   **/
   std::queue<MTIOBuffer *> *m_QueuePointer;

   /**    status about working    **/
   StatusInfo  m_StatusInfo;

   MTAtomicSemaphore * m_AtomicSemaphorePointer;

   /**    time tool    **/
   MTTimeMeasure m_TimeMeasure;

   /**    stop lock    **/
   std::mutex    m_StopMutx;

   std::thread * m_ThreadHandle;

   bool m_Stop;

   bool m_Check;


};
NS_END_MT_UTIL_IO
#endif