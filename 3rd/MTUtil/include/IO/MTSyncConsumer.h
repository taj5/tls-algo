/***********************************************************************
 * Module:  MTSyncConsumer.h
 * Author:  shihaosen
 * Modified: 2019��1��2�� 9:01:45
 * Purpose: Declaration of the class MTSyncConsumer
 * Comment: ��������
 *    1.���߳�
 *    2.��ջ->�ص�
 *    ������1.ֹͣ��2.״̬��ѯ��
 ***********************************************************************/

#if !defined(__TDSUMLClassDiagram2_MTSyncConsumer_h)
#define __TDSUMLClassDiagram2_MTSyncConsumer_h

#include <IO/IMTConsumerSchedule.h>
#include <IO/MTIOBuffer.h>
#include <Common/MTTimeMeasure.h>
#include <mutex>

NS_BGN_MT_UTIL_IO

class MTSyncConsumer : public IMTConsumerSchedule
{
public:
	StatusInfo * GetStatus(void);
   void RegisterEvent(IMTIOCallback * cb);
   bool Init(void * param);
   bool Process(MTIOBuffer * data);
   void Start(void);
   void Stop(void);
   MTSyncConsumer();
   ~MTSyncConsumer();
   MTSyncConsumer(const MTSyncConsumer&) = delete;
   MTSyncConsumer& operator=(const MTSyncConsumer&) = delete;
protected:
private:
	/**    callback     **/
	IMTIOCallback *m_Callback;

	/**    status about working    **/
	StatusInfo  m_StatusInfo;

	/**    time tool    **/
	MTTimeMeasure m_TimeMeasure;

	/**    stop lock    **/
	std::mutex    m_StopMutx;

	bool m_Stop;
};
NS_END_MT_UTIL_IO
#endif