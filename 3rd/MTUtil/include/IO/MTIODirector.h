//
// Created by shangxichao on 2019/1/7.
//

#ifndef MTUTIL_MTIODIRECTOR_H
#define MTUTIL_MTIODIRECTOR_H

#include <stdint.h>
#include <string>
#include <vector>
#include <Common/MTUtilGlobal.h>

/*
 *
 struct _finddata32_t
{
    unsigned    attrib;
    __time32_t  time_create;    // -1 for FAT file systems
    __time32_t  time_access;    // -1 for FAT file systems
    __time32_t  time_write;
    _fsize_t    size;
    char        name[260];
};
 */

NS_BGN_MT_UTIL

        typedef enum class st_FileAttribute{
            // File attribute constants for the _findfirst() family of functions
            A_NORMAL = 0x00, // Normal file - No read/write restrictions
            A_RDONLY = 0x01, // Read only file
            A_HIDDEN = 0x02, // Hidden file
            A_SYSTEM = 0x04, // System file
            A_SUBDIR = 0x10, // Subdirectory
            A_ARCH   = 0x20, // Archive file
        }MTFileAttribute;

class MTFileInfo{
private:
    uint32_t  attrib;
    uint32_t  time_create;
    uint32_t time_access;
    uint32_t time_write;
    uint64_t file_size;
    std::string name;

public:
    MTFileInfo();

    MTFileInfo(uint32_t attrib, uint32_t time_create, uint32_t time_access, uint32_t time_write, uint64_t file_size,
               const std::string &name);

    uint32_t getAttrib() const;

    void setAttrib(uint32_t attrib);

    uint32_t getTime_create() const;

    void setTime_create(uint32_t time_create);

    uint32_t getTime_access() const;

    void setTime_access(uint32_t time_access);

    uint32_t getTime_write() const;

    void setTime_write(uint32_t time_write);

    uint64_t getFile_size() const;

    void setFile_size(uint64_t file_size);

    const std::string &getName() const;

    void setName(const std::string &name);
};
NS_END_MT_UTIL

std::vector<MT::Util::MTFileInfo> TraverseFiles(const std::string& path, const std::string& suffix="log");
#endif //MTUTIL_MTIODIRECTOR_H
