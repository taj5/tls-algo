/***********************************************************************
 * Module:  IMTIOCallback.h
 * Author:  shihaosen
 * Modified: 2018年12月29日 14:47:07
 * Purpose: Declaration of the class IMTIOCallback
 * Comment: 订阅input的业务类需要实现此接口
 *    callback是消费数据入口函数
 *    设计目标：
 *    1.其中用途可以通过设置不同的EventId进行业务逻辑分类 eg： producer  publish不同的EventId。
 *    consumer receive  根据不同的EventId进行不同的业务处理。
 *    2.此接口的设计也是为了兼容实现同步消费过程   即  publish->consume->publish
 *    3.回调采用结构体也是为了尽量减少扩展带来的隐患
 ***********************************************************************/
#include <string>

#if !defined(__TDSUMLClassDiagram2_IMTIOCallback_h)
#define __TDSUMLClassDiagram2_IMTIOCallback_h

#include <IO/MTIOBuffer.h>
NS_BGN_MT_UTIL_IO
class IMTConsumerSourceBase;
class IMTIOCallback
{
public:
   virtual void Callback(MTIOBuffer* MTInputParamer)=0;

   /**	增加这个方法是为了在发生错误时能找到回调对应的consumer	**/
   void SetConsumer(IMTConsumerSourceBase* consumer) 
   {
	   consumer_ = consumer;
   }

   IMTConsumerSourceBase *GetConsumer() const
   {
	   return consumer_;
   }
protected:
private:
	IMTConsumerSourceBase *consumer_;
};
NS_END_MT_UTIL_IO
#endif
