/***********************************************************************
* Module:  IMTConsumeScheduling.h
* Author:  shihaosen
* Modified: 2019闂佽法鍠愰弸濠氬箯閿燂拷1闂佽法鍠愰弸濠氬箯閿燂拷2闂佽法鍠愰弸濠氬箯閿燂拷 9:31:06
* Purpose: Declaration of the class IMTConsumeScheduling
* Comment: 闂佽法鍠愰弸濠氬箯閻戣姤鏅哥紒鎰墱椤ｎ噣骞忛悜鑺ユ櫢闁哄倶鍊栫€氬綊鏌ㄩ悢鍛婄伄闁归鍏橀弫鎾诲棘閵堝棗顏堕悷鏇氱閻ゅ嫰鏌ㄩ悢娲绘晭婵縿鍊栫敮鎾礌閳╁啫顏�. 闂佽法鍠愰弸濠氬箯閻戣姤鏅哥紒鎰墱椤ｎ噣骞忛悜鑺ユ櫢濡ゆ妗ㄧ槐顕€骞忛悜鑺ユ櫢婵炶弓闄嶉埀顒€鍊块弫鎾诲棘閵堝棗顏堕梺璺ㄥ枑閺嬪骞忛悜瑙ｅ亾濮樿埖鏅搁柡鍌樺€栫€氬綊鏌ㄩ悢鍛婃畬闁逛絻鍎婚鎰板箯瀹勬壆鏉介梺璺ㄥ枙椤㈡粎鎷犺鐎氬綊宕ュ畝鍕櫢闁哄倶鍊栫€氬綊鏌ㄩ悢鍛婄伄闁归鍏橀弫鎾剁玻閺嶎偒鏆滈柟椋庡厴閺佹挻顨滈崫鍕闁瑰嘲鍢茬槐锟�
*    闁烩晩鍠栨晶鐘碘偓鍦仱閺佹挾鎮板Δ鍕涧闁归鍏橀弫鎾诲棘閵堝棗顏堕梺璺ㄥ櫐閹凤拷 MTQueueConsumer闂佽法鍠愰弸濠氬箯缁嬬挆ConcurrentConsumer  闂佽法鍠愰弸濠氬箯閻戣姤鏅搁悘鐐诧攻椤掔偤鏌ㄩ悢鍛婄伄闁归鍏橀弫鎾诲棘閵堝棗顏堕梺璺ㄥ枑閺嬪骞忛敓锟� MTSyncConsumer 闂佽法鍠愰弸濠氬箯瀹勭増鍊遍梺璺ㄥ枑閺嬪骞忛悜鑺ユ櫢闁哄倶鍊栫€氬綊鏌ㄩ悢鍛婄伄闁归鍏橀弫鎾诲棘閵堝棗顏�
*    MTQueueConsumer 闂佽法鍠愰弸濠氬箯閻戣姤鏅搁悘鐐诧攻椤掔偤鏌ㄩ悢鍛婄伄闁归鍏橀弫鎾绘儗椤愮喐顫夐柟椋幣峊ConcurrentConsumer 闂佽法鍠愰弸濠氬箯閻戣姤鏅哥紒鏃戝幘閳荤厧效閻樻鏆滈柟椋庡厴閺佹捇鎯岄銏犳闁瑰嘲鍢茬槐锟�   MTSyncConsumer 闂佽法鍠愰弸濠氬箯瀹勭増鍊遍梺璺ㄥ枑閺嬪骞忛悜鑺ユ櫢闁哄倶鍊栫€氬綊鏌ㄩ悢鍛婄伄闁归鍏橀弫鎾诲棘閵堝棗顏�
*  濞达絽娼￠弫鎾绘儗椤愩儺娼愰柤鐓庡暣閺佹捇寮妶鍡楊伓 濞达絽娼￠弫鎾诲棘閵堝棗顏堕柛鎾崇Ч閺佹捇寮妶鍡楊伓闂佽法鍠愰弸濠氬箯閻戣姤鏅搁柡鍌樺€栫€氬綊鏌ㄩ悢铏广偆nit闂佽法鍠愰弸濠氬箯閻戣姤鏅搁柣銊ユ閸炲骞忛悜鑺ユ櫢闁哄倶鍊栫€氬湱鎷幇鐗堟櫢缂備胶鍋熼妴瀣箯閻戣姤鏅搁柨鐕傛嫹
*  濞达絽娼￠弫鎾诲棘閵堝棗顏舵繛澶堝姂閺佹捇寮妶鍡楊伓闂佽法鍠愰弸濠氬箯閻戣姤鏅哥紒娑橆儜缁辩殐ush 闂佽法鍠愰弸濠氬箯閻戣姤鏅搁柡鍌樺€栫€氬綊鏌ㄩ悢鍛婄伄闁归鍏橀弫鎾诲棘閵堝棗顏堕梺璺ㄥ枑閺嬪骞忛悜鑺ユ櫢闁哄倶鍊栫€氱MTConsumeSourceBase闂佽法鍠愰弸濠氬箯閻戣姤鏅搁柡鍌樺€栫€氬綊鏌ㄩ悢璇参濋悹浣筋潐鐎氬綊鏌ㄩ悢鑽ょ獩闁告垯鍊栫€氬綊鏌ㄩ悢绋跨亰闁挎稑鐭傞弫鎾诲棘閵堝棗顏堕梺璺ㄥ枑閺嬪骞忛悜鑺ユ櫢闁哄倶鍊栫€氱MTConsumeScheduling闂佽法鍠愰弸濠氬箯閻戣姤鏅搁柡鍌樺€栫€氬綊鏌ㄩ悢宄板缓闁哄倶鍊栫€氬綊鏌ㄩ悢鍛婄伄闁归鍏橀弫鎾绘煀闂堟稑娈╅柟鍑ゆ嫹
***********************************************************************/

#ifndef MT_UTIL_IO_IMTCONSUMERSCHEDULING_H
#define MT_UTIL_IO_IMTCONSUMERSCHEDULING_H

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include<Windows.h>
#include<winsock2.h>
#endif

#include <IO/IMTIOCallback.h>
#include <IO/MTIOBuffer.h>
#include <atomic>

NS_BGN_MT_UTIL_IO

typedef unsigned char MTuint8;
typedef unsigned short MTuint16;
typedef unsigned int  MTuint32;
struct StatusInfo
{
	/**  input number     **/
	std::atomic_uint in;
	/**  dealed number     **/
	std::atomic_uint out;
	/**  drop number,undealed     **/
	std::atomic_uint drop;
	/**  number per second     **/
	float nps;

	std::atomic_uint status;

	float lastMillisecond;

	std::atomic_uint reverse1;

	std::atomic_uint reverse2;
};

class IMTConsumerSchedule
{
public:
	virtual StatusInfo * GetStatus(void) = 0;
	virtual void RegisterEvent(IMTIOCallback * cb) = 0;
	virtual bool Init(void * param) = 0;
	virtual bool Process(MTIOBuffer * data) = 0;
	virtual void Start(void) = 0;
	virtual void Stop(void) = 0;
	virtual ~IMTConsumerSchedule(){}
protected:
private:

};
NS_END_MT_UTIL_IO
#endif