#ifndef IMTProducerSourceBase_H
#define IMTProducerSourceBase_H
#include <Common/MTUtilGlobal.h>
#include <Parser/MTParser.h>
using MT::Util::Net::MTParser;
using MT::Util::Net::ConnectionObject;
using MT::Util::Net::ProtoType;
using MT::Util::Net::OperationType;

#include <map>
NS_BGN_MT_UTIL_IO
class IMTProducerSourceBase;
class MT_UTIL_API MTProducerMgr {
public:
	~MTProducerMgr();
	static MTProducerMgr& GetInstance();
	typedef std::map<std::string, IMTProducerSourceBase*> MTConsumerMap;
private:
	MTProducerMgr();
	MTProducerMgr(const MTProducerMgr&) = delete;
	MTProducerMgr& operator=(const MTProducerMgr&) = delete;
public:
	IMTProducerSourceBase* Create(const std::string& modulename, const std::string &connstr);
private:
	MTParser m_Parser;
	MTConsumerMap m_ConsumerMap;
};

NS_END_MT_UTIL_IO

#endif