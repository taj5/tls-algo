/***********************************************************************
 * Module:  MTQueueConsumer.h
 * Author:  shihaosen
 * Modified: 2019锟斤拷1锟斤拷2锟斤拷 8:58:12
 * Purpose: Declaration of the class MTQueueConsumer
 * Comment: 锟斤拷锟斤拷锟斤拷锟斤拷
 *    1.锟斤拷锟竭筹拷
 *    2.锟斤拷栈->锟截碉拷
 *    锟斤拷锟斤拷锟斤拷1.停止锟斤拷2.状态锟斤拷询锟斤拷
 ***********************************************************************/

#if !defined(__TDSUMLClassDiagram2_MTQueueConsumer_h)
#define __TDSUMLClassDiagram2_MTQueueConsumer_h

#include <IO/IMTConsumerSchedule.h>
#include <IO/MTIOBuffer.h>
#include <queue>
#include <thread>
#include <mutex>
#include <Common/MTTimeMeasure.h>
#include <Common/MTAtomicSemaphore.h>
NS_BGN_MT_UTIL_IO
struct	QueueConsumerStruct
{
	int MaxQueueSize;
};
class MTQueueConsumer : 
	public IMTConsumerSchedule
{
	static void  run(void *param);
public:
	StatusInfo * GetStatus(void);
   void RegisterEvent(IMTIOCallback * cb);
   bool Init(void * param);
   bool Process(MTIOBuffer * data);
   void Start(void);
   void Stop(void);
   MTQueueConsumer();
   ~MTQueueConsumer();
   MTQueueConsumer(const MTQueueConsumer&) = delete;
   MTQueueConsumer& operator=(const MTQueueConsumer&) = delete;

protected:
private:
   /**    callback     **/
   IMTIOCallback *m_Callback;

   /**    data queue FIFO   **/
   std::queue<MTIOBuffer *> *m_QueuePointer;

   /**    status about working    **/
   StatusInfo  m_StatusInfo;

   MTAtomicSemaphore * m_AtomicSemaphorePointer;

   /**    time tool    **/
   MTTimeMeasure m_TimeMeasure;

   /**    stop lock    **/
   std::mutex    m_StopMutx;

   std::thread * m_ThreadHandle;

   bool m_Stop;

   bool m_Check;


};
NS_END_MT_UTIL_IO
#endif