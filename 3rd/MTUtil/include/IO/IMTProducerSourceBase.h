#ifndef MT_UTIL_IO_IMTPRODUCERSOURCEBASE
#define MT_UTIL_IO_IMTPRODUCERSOURCEBASE

//#define WIN32_LEAN_AND_MEAN
//
//#include<Windows.h>
//
//#include<winsock2.h>
//#pragma comment(lib,"ws2_32.lib")
#include <string>
#include <Net/Udp/MTUdpClient.h>
#include <Net/Tcp/MTTcpFSClient.h>
#include <Common/MTUtilGlobal.h>

#include "Net/MTRedisClient.h"

NS_BGN_MT_UTIL_IO

class ProduceStatistics {
public:
	ProduceStatistics() : m_ProduceNum(0),m_FailedNum(0), m_SuccessNum(0){ }
public:
	int m_Status;
	int m_ProduceNum;
	int m_FailedNum;
	int m_SuccessNum;
};

class MT_UTIL_API IMTProducerSourceBase {
public:
	virtual ~IMTProducerSourceBase() {}
	virtual bool Open() = 0;
	virtual bool ReOpen() = 0;
	virtual bool Close() = 0;
	virtual bool Produce(const void* data, int datasize) = 0;
	virtual ProduceStatistics& GetStatistics() = 0;

protected:
	ProduceStatistics m_Statistics;
	std::string m_Host;
	int m_Port;
	int m_ConnectionTimeout;
	int m_WriteTimeout;
};

class MTUdpProducer : public IMTProducerSourceBase {
public:
	MTUdpProducer(std::string& host, int port, int writetimeout);
	~MTUdpProducer();
	MTUdpProducer(const MTUdpProducer&) = delete;
	MTUdpProducer& operator=(const MTUdpProducer&) = delete;
public:
	bool Open();
	bool ReOpen();
	bool Close();
	bool Produce(const void* data, int datasize);
	ProduceStatistics& GetStatistics();
private:
	MT::Util::Net::Udp::MTUdpClient m_UdpClt;
};

class MTTcpProducer : public IMTProducerSourceBase {
public:
	MTTcpProducer(std::string& host, int port, int writetimeout);
	~MTTcpProducer();
	MTTcpProducer(const MTUdpProducer&) = delete;
	MTTcpProducer& operator=(const MTTcpProducer&) = delete;
public:
	bool Open();
	bool ReOpen();
	bool Close();
	bool Produce(const void* data, int datasize);
	ProduceStatistics& GetStatistics();
private:
	MT::Util::Net::Tcp::MTTcpFSClient m_TcpClt;
};

NS_END_MT_UTIL_IO

#endif // !MT_UTIL_IO_IMTPRODUCERSOURCEBASE