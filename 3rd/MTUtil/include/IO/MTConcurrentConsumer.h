/***********************************************************************
 * Module:  MTConcurrentConsumer.h
 * Author:  shihaosen
 * Modified: 2019锟斤拷1锟斤拷2锟斤拷 8:59:51
 * Purpose: Declaration of the class MTConcurrentConsumer
 * Comment: 锟斤拷锟斤拷锟斤拷锟竭程池ｏ拷锟斤拷锟斤拷锟斤拷
 *    锟斤拷要锟杰诧拷询锟斤拷锟斤拷锟叫碉拷锟竭筹拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷
 ***********************************************************************/

#if !defined(MT_UTIL_IO_MTConcurrentConsumer_h)
#define MT_UTIL_IO_MTConcurrentConsumer_h

#include <IO/IMTConsumerSchedule.h>
#include <queue>
#include <thread>
#include <mutex>
#include "Common/MTTimeMeasure.h"
#include <Common/MTAtomicSemaphore.h>

NS_BGN_MT_UTIL_IO

struct ConcurrentConsumerStruct
{
	int MaxThreadNumber;
	int MaxQueueSize;
};
class MTConcurrentConsumer : public IMTConsumerSchedule
{
public:
	StatusInfo * GetStatus(void);
   void RegisterEvent(IMTIOCallback* cb);
   bool Init(void * param);
   bool Process(MTIOBuffer * job);
   void Start(void);
   void Stop(void);
   MTConcurrentConsumer();
   ~MTConcurrentConsumer();

   MTConcurrentConsumer(const MTConcurrentConsumer&) = delete;
   MTConcurrentConsumer& operator=(const MTConcurrentConsumer&) = delete;
protected:
private:
	/**    callback     **/
	IMTIOCallback* m_Callback;

	/**    data queue FIFO   **/
	std::queue<MTIOBuffer *> m_Queue;

	/**    max queue size    **/
	int m_MaxQueueSize;

	/**    working thread pool    **/
	std::vector< std::thread > workers;

	/**    max thread number    **/
	short m_ThreadMaxNum;

	/**    status of working    **/
	StatusInfo  m_StatusInfo;
	
	/**    time tool    **/
	MTTimeMeasure  m_TimeMeasure;
	std::mutex m_SopMutex, m_NPSMutex;
	MTAtomicSemaphore *m_QAtomicSemaphore;
	bool m_Stop;
};
NS_END_MT_UTIL_IO
#endif