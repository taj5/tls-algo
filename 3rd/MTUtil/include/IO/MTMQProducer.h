#ifndef MT_UTIL_IO_MTMQProducer
#define MT_UTIL_IO_MTMQProducer

#include "IO/IMTProducerSourceBase.h"
#include "Net/MTRedisClient.h"

NS_BGN_MT_UTIL_IO


class MTMQProducer : public IMTProducerSourceBase {
public:
	MTMQProducer(const std::string& host,
		int port, 
		const std::string&password, 
		const std::string&channel_name,
		int write_timeout,
		int connect_timeout );
	virtual ~MTMQProducer();
	MTMQProducer(const MTMQProducer&) = delete;
	MTMQProducer& operator=(const MTMQProducer&) = delete;
public:
	virtual bool Open();
	virtual bool ReOpen();
	virtual bool Close();
	virtual bool Produce(const void* data, int datasize);
	virtual ProduceStatistics& GetStatistics();
private:
	MT::Util::Net::MTRedisClient redis_client_;
	std::string channel_name_;
	std::string password_;
};

NS_END_MT_UTIL_IO

#endif // !MT_UTIL_IO_IMTPRODUCERSOURCEBASE