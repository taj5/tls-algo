#ifndef MT_UTIL_IO_MTCONSUMERMGR_H
#define MT_UTIL_IO_MTCONSUMERMGR_H
#include <Common/MTUtilGlobal.h>
#include <Parser/MTParser.h>
using MT::Util::Net::MTParser;
using MT::Util::Net::ConnectionObject;
using MT::Util::Net::ProtoType;
using MT::Util::Net::OperationType;
#include <map>


NS_BGN_MT_UTIL_IO

class IMTConsumerSourceBase;
class MT_UTIL_API MTConsumerMgr {
public:
	~MTConsumerMgr();
	static MTConsumerMgr& GetInstance();
	typedef std::map<std::string, IMTConsumerSourceBase*> MTConsumerMap;
private:
	MTConsumerMgr();
	MTConsumerMgr(const MTConsumerMgr&) = delete;
	MTConsumerMgr& operator=(const MTConsumerMgr&) = delete;
public:
	IMTConsumerSourceBase* Create(const std::string& modulename, const std::string &connstr);
private:
	MTParser m_Parser;
	MTConsumerMap m_ConsumerMap;
};
typedef MTConsumerMgr MTConsumerFactory;
NS_END_MT_UTIL_IO

#endif