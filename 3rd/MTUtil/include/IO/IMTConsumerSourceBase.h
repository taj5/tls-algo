#ifndef MT_UTIL_IO_IMTCOMSUMERSOURCEBASE
#define MT_UTIL_IO_IMTCOMSUMERSOURCEBASE

#include <string>
#include <Common/MTUtilGlobal.h>
#include <IO/IMTConsumerSchedule.h>
#include <Net/Udp/MTUdpServer.h>
#include <Net/Tcp/MTTcpServer.h>
#include <Net/ISocketEvtSubscription.h>
#include "Net/MTRedisClient.h"

using MT::Util::Net::ISocketEvtSubscription;

NS_BGN_MT_UTIL_IO

enum CONSUMER_STATUS{
	CONSUMER_UNKOWN,
	CONSUMER_OPEN,
	CONSUMER_CLOSED
};

class IMTConsumerEvent{
public:
	virtual void Consumer() = 0;
};

class MT_UTIL_API IMTConsumerSourceBase {
public:
	virtual ~IMTConsumerSourceBase(){}
	virtual bool Open() = 0;
	virtual bool ReOpen() = 0;
	virtual bool Close() = 0;
	virtual StatusInfo * GetStatistics() = 0;
	virtual bool Register(IMTIOCallback * cb) = 0;
	virtual void SetConsumeScheduling(IMTConsumerSchedule* schedule) = 0;
protected:
	virtual bool Start() = 0;
	virtual IMTConsumerSchedule *GetConsumerSchedule() = 0;
protected:
	std::string m_Host;
	int m_Port;
	int m_ReadTimeout;
	int m_ConnectionTimeout;
	IMTConsumerSchedule* m_Schedule;
};

class MTUdpConsumer : public IMTConsumerSourceBase {
public:
	MTUdpConsumer(std::string& host, int port, int readtimeout, int connectiontimeout);
	~MTUdpConsumer();
	MTUdpConsumer(const MTUdpConsumer&) = delete;
	MTUdpConsumer& operator=(const MTUdpConsumer&) = delete;
public:
	bool Open();
	bool ReOpen();
	bool Close();
	StatusInfo * GetStatistics();
	bool Register(IMTIOCallback * cb);
	bool Start();
	void SetConsumeScheduling(IMTConsumerSchedule* schedule);
	IMTConsumerSchedule *GetConsumerSchedule();
private:
	MT::Util::Net::Udp::MTUdpServer m_UdpSrv;
};

class MTTcpConsumer : public IMTConsumerSourceBase {
public:
	MTTcpConsumer(std::string& host, int port, int readtimeout, int connectiontimeout);
	~MTTcpConsumer();
	MTTcpConsumer(const MTUdpConsumer&) = delete;
	MTTcpConsumer& operator=(const MTTcpConsumer&) = delete;
public:
	bool Open();
	bool ReOpen();
	bool Close();
	StatusInfo * GetStatistics();
	bool Register(IMTIOCallback * cb);
	bool Start();
	void SetConsumeScheduling(IMTConsumerSchedule* schedule);
	IMTConsumerSchedule *GetConsumerSchedule();
private:
	MT::Util::Net::Tcp::MTTcpServer m_TcpSrv;
};
NS_END_MT_UTIL_IO

#endif // !MT_UTIL_IO_IMTCOMSUMERSOURCEBASE