#ifndef MT_UTIL_IO_MTIOBUFFER_H
#define MT_UTIL_IO_MTIOBUFFER_H
#include <Common/MTUtilGlobal.h>
#include <string>
NS_BGN_MT_UTIL_IO

/**
下一步优化，引用计数，unique_ptr
**/
class MT_UTIL_API MTIOBuffer {
public:
	MTIOBuffer();
	MTIOBuffer(void* data, int datasize, IOHandle fd, bool error = false);
	MTIOBuffer(const MTIOBuffer& buf);
	MTIOBuffer& operator=(const MTIOBuffer& buf);
	~MTIOBuffer();
public:
	bool Init(void* data, int size, IOHandle & fd, bool error);
	void Release(); 
public:
	void* m_Data;
	int m_DataSize;
	IOHandle m_Fd;
	/**     时间      **/
	int64_t m_BirthTime;
	/**	标识此时的返回的buffer是个错误	**/
	bool m_Error;
};

NS_END_MT_UTIL_IO
#endif // !MT_UTIL_IO_MTIOBUFFER_H