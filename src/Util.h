#ifndef MTIO_UTIL_H
#define MTIO_UTIL_H
#include <stdint.h>
#include <string>
//#include "Common/MTUtilGlobal.h"
//#include "boost/pool/pool.hpp"
//#include "boost/pool/object_pool.hpp"

#define MT_UTIL_API 

/* Evaluates to the same unsigned charean value as 'p', and hints to the compiler that
* we expect this value to be false. */
#if defined(__GNUC__) && __GNUC__ >= 3         /* gcc 3.0 or later */
#define UTIL_UNLIKELY(p) __builtin_expect(!!(p),0)
#else
#define UTIL_UNLIKELY(p) (p)
#endif

struct timezone;
MT_UTIL_API int util_gettimeofday(struct timeval *tv, struct timezone *tz);
MT_UTIL_API long util_tv_to_msec(const struct timeval *tv);

MT_UTIL_API int64_t util_gettimestamp();
// "yyyy-MM-dd HH:mm:ss"
MT_UTIL_API void util_gettimestampString(int64_t timestamp, char *date_string);
// "yyyyMMddHHmmss"
MT_UTIL_API void util_gettimestampString2(int64_t timestamp, char *date_string);
// "yyyy-MM-dd HH:mm:ss.zzz"
MT_UTIL_API void util_gettimestampWithMSString(int64_t timestamp, char *date_string);

MT_UTIL_API uint16_t util_flipbytes(uint16_t value);
MT_UTIL_API uint32_t util_flipbytes(uint32_t value);
MT_UTIL_API uint64_t util_flipbytes(uint64_t value);

MT_UTIL_API void util_get_vertion(std::string &version);

//MT_UTIL_API void sprintf_std_string(std::string & destination, const char * format, ...);

#endif //ASHDETECTOR_UTIL_H
