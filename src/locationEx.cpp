#include "locationEx.h"

Location::Location(int index,int leap_th, int one_th, int zero_th, int new_th, int loadmin, int loadmax):index_max_(index),
firstval_(0),speed_(0),one_noise_th_(one_th),zero_noise_th_(zero_th),newcar_th_(new_th),
lock_(false),load_range_min_(loadmin),load_range_max_(loadmax),leap_th_(leap_th),h_offset_(0)
{
    Pos pos(0,0,-1);
    pos.index = index_max_;
    pos.head = 0;
    pos.tail = 0;
    poslist_.push_back(pos);
    runlen_.push_back(SN);
}

Location::~Location(){}

void Location::RegCallback(MessageFunc func)
{
    logger_ = func;
}

std::string DataToString(std::bitset<SN>& data)
{
    std::string strdata;
	for (int i = 0; i < SN; i++) {
		if (data[i] == 0) {
			strdata.push_back('0');
		}
		else {
			strdata.push_back('1');
		}
	}
    return strdata;
}

void Location::SetCarIndex(int index) 
{
    std::lock_guard<std::mutex> lck(mtx_);
    index_max_ = index;
}

enReturnType Location::GetTrainPos(std::bitset<SN>& data, std::vector<Pos>& poslist, float* speed)
{
    std::lock_guard<std::mutex> lck(mtx_);
    poslist.clear();
    enReturnType rt = TYPE_OK;
    std::vector<int> runlen;
    int firstval = 0;
    int distance = 0;
    int addcnt = 0;
    *speed = 0;
    std::string message;
    std::map<int, std::pair<int,int>>::iterator it = movemap_.begin();

    if (!denoise(data)) 
        return TYPE_ERR_NOISE;

    runlen = runLengthEncode(data);
    firstval = data[0];
    if (runlen.size() == 1 && firstval == 0) {
        poslist.push_back(Pos(0,0,-1));
        rt = TYPE_OK;
        goto LOC_END;
    }

    /* 车厢在光幕中，光幕全遮挡*/
    if (runlen.size() == 1 && firstval == 1 && index_max_ != 0) {
        logger_("Error=>Data is all 1.", 10);
        rt = TYPE_ERR_NOISE;
        goto LOC_END;
    }

    if (firstval == 0 && runlen[0] > zero_noise_th_) {
		lock_ = false;
	}

    /* 无锁状态下，去除左侧1 */
    if (!lock_ && firstval == 1 && runlen[0] < newcar_th_) {
		flipData(data, runlen, 0);
		runlen = runLengthEncode(data);
		firstval = data[0];
	}

    /* 光幕中没有车，却有遮挡, 去除遮挡*/
    if (runlen.size() > 1 && firstval == 0 && index_max_ == -1) {
        //rt = TYPE_ERR_NOISE;
        //logger_("Error=>index_max_ is -1 and Right side have 1.", 10);
        //goto LOC_END;
        for (int i=1; i<runlen.size(); i+=2) {
            flipData(data, runlen, i);
        }
        runlen = runLengthEncode(data);
		firstval = data[0];
    }
    

    if (runlen.size() == 1 && firstval == 0) {
        rt = TYPE_OK;
        poslist.push_back(Pos(0,0,-1));
        goto LOC_END;
    }

    /* 检查游程变化 */
    if (!checkRunLength(runlen)){
        rt = TYPE_ERR_MISINDEX;
        goto LOC_END;
    }

    if (firstval_ == 0 && firstval == 1 && !lock_ && runlen[0] >= newcar_th_ && runlen[0] < 2*newcar_th_) {
		lock_ = true;
		index_max_++;
        char text[1024];
        sprintf(text, "%dth new car appear.", index_max_);
        logger_(text, 10);
	}
	
	if (speed_ < 0 && firstval_ == 1 && firstval == 0 && runlen[0] > zero_noise_th_) {
		index_max_--;
        char text[1024];
        sprintf(text, "%dth car disappear.", index_max_);
        logger_(text, 10);
	}

    message = DataToString(data);
    logger_(message.c_str(), message.length());
    convert2Poslist(data, poslist);

    if (getMovingDistance(poslist) == false) {
        logger_("getMovingDistance error.", 10);
        rt = TYPE_ERR_LEAP;
        goto LOC_END;
    }

    for (it=movemap_.begin(); it!=movemap_.end(); ++it) {
        //printf("%dth car head move %d tail move %d\n", it->first, it->second.first, it->second.second);
        if (it->second.first < 10 && it->second.first > 0) {
            distance += it->second.first;
            addcnt++;
        } 

        if (it->second.second < 5 && it->second.second > 0) {
            distance += it->second.first;
            addcnt++;
        }
    }

    if (addcnt != 0) distance = distance / addcnt;

    if (getAverageSpeed(poslist, speed)){
        speed_ = *speed;
    }

LOC_END:
    if (rt == TYPE_OK || rt == TYPE_ERR_LEAP) {
        updateUsefulThing(runlen, poslist, firstval, *speed);
	    if (poslist[poslist.size() - 1].index == poslist_[poslist_.size() - 1].index
	    	&& poslist_[poslist_.size() - 1].head >= SN) {
	    	h_offset_ += distance;
	    } else {
	    	h_offset_ = 0;
	    }
	    poslist[poslist.size() - 1].head += h_offset_;
    }
    return rt;
}

bool Location::denoise(std::bitset<SN>& data)
{
    std::string message;

    de_one(data);
    message = DataToString(data);
    logger_(message.c_str(), message.length());

    de_one_again(data);
    message = DataToString(data);
    logger_(message.c_str(), message.length());

    de_zero(data);
    message = DataToString(data);
    logger_(message.c_str(), message.length());

    bool res = de_znum_onum(data);
    message = DataToString(data);
    logger_(message.c_str(), message.length());

    return res;
}

/*
返回值 true：去噪成功  false：去噪失败 
*/
bool Location::de_znum_onum(std::bitset<SN>& data)
{
    std::vector<int> arr;
    int znum=0, onum=0, flip_idx;
    arr = runLengthEncode(data);
    znum = (arr.size() + 1 - data[0]) / 2;
    onum = arr.size() - znum;

    if (znum > 3) return false;

    if (znum == 1 && onum == 2) {
        check_101(data, arr);
        return true;
    }

    if (znum == 2 && onum == 2) {
        check_trainhead_1010(data, arr);
    }

    if (znum == 2 && onum == 3) {
        check_10101(data, arr);
        return true;
    }
    

    if (znum == 3 && onum == 2) { // 01010,去侧边0
        flip_idx = arr[1] > arr[3] ? 4 : 0;
        flipData(data, arr, flip_idx);
        return true;
    } 
    
    /* 
	 * 010101 =>10101 0101
	 * 101010 => 1010 1010 10101  
	 * 1010101 => 10101 10101 10101
	 */
    if (znum == 3 && (onum==3 || onum==4)) { 
        int i1 = data[0];
        int i2 = i1 + 2;
        int i3 = i2 + 2;
        int imin = arr[i1] < arr[i2]? i1 : i2;
        imin = arr[imin] < arr[i3]? imin : i3; // 找出长度最小的0段索引
        flipData(data, arr, imin);

        std::vector<int> newarr;
        int newznum, newonum;
        newarr = runLengthEncode(data);
        newznum = (arr.size() + 1 - data[0]) / 2;
        newonum = arr.size() - newznum;
        if (newznum == 2 && newonum == 3) {
            check_10101(data, arr);
        }

        if (newznum == 1 && newonum == 2) {
            check_101(data, arr);
        }

        return true;
    }
    return true;
}

// 中间0比右侧1大，去除右侧1
void Location::check_101(std::bitset<SN>& data, std::vector<int>& runlen)
{
    if (runlen[1] > runlen[2]) {
        flipData(data, runlen , 2);
    }
}

// 两侧1不能大于中间1
void Location::check_10101(std::bitset<SN>& data, std::vector<int>& runlen) 
{
    if (runlen[0]>runlen[2] && runlen[4]>runlen[2]) {
        int flip_idx = runlen[1] > runlen[3] ? 3 : 1;
	    flipData(data, runlen, flip_idx);
    }
}

void Location::check_trainhead_1010(std::bitset<SN>& data, std::vector<int>& runlen)
{
    if (/*runlen[0] > runlen[2] &&*/ index_max_ < 1) {
        flipData(data, runlen, 2);
    } 
}

void Location::de_one(std::bitset<SN>& data)
{
    int curr, next;
	std::vector<int> runlen;
	runlen = runLengthEncode(data);

	curr = data[0]? 2:1;
	next = curr + 1;

	while (next < runlen.size()) {
		if (runlen[curr] <= one_noise_th_) {
			flipData(data, runlen, curr);
		}
		curr = curr + 2;
		next = curr + 1;
	}
}

void Location::de_one_again(std::bitset<SN>& data)
{
    std::vector<int> runlen;
	runlen = runLengthEncode(data);
	for (int i = (1+data[0]); i<(runlen.size()-1); i=i+2) {
		if (runlen[i]<=runlen[i-1] && runlen[i]<=runlen[i+1]) {
			flipData(data, runlen, i);		
		}
	}
}

void Location::de_zero(std::bitset<SN>& data)
{
    int iter, piter, niter;
	std::vector<int> runlen;
	iter = data[0] ? 1 : 2;
	piter = iter - 1;
	niter = iter + 1;
	runlen = runLengthEncode(data);

	while (niter < runlen.size()) {
		if (runlen[iter] <= zero_noise_th_ &&  // 必要条件
            ((runlen[piter] > zero_noise_th_ && runlen[niter] > zero_noise_th_) || // 条件1
            iter == 1 ||  // 条件2
            iter == (runlen.size()-2))) { // 条件3
			flipData(data, runlen, iter);
		}
		iter = 2 + iter;
		piter = iter - 1;
		niter = iter + 1;
	}
}

std::vector<int> Location::runLengthEncode(const std::bitset<SN>& data)
{
    std::vector<int> runlen;
	bool tmp = data[0];
	int cnt = 1;
	for (int i = 1; i < SN; i++) {
		if (data[i] == tmp) {
			cnt++;
		}
		else {
			runlen.push_back(cnt);
			tmp = !tmp;
			cnt = 1;
		}
	}
	runlen.push_back(cnt);
	return runlen;
}

void Location::flipData(std::bitset<SN>& data, std::vector<int> lens, int idx)
{
	int start = 0;
	for (int i = 0; i < idx; i++) {
		start += lens[i];
	}
	for (int i = start; i < (start+lens[idx]); i++) {
		data[i].flip();
	}
}

void Location::updateUsefulThing(const std::vector<int> runlen, 
                                const std::vector<Pos>& poslist, 
                                int firstval, float speed)
{
    firstval_ = firstval;
    runlen_.assign(runlen.begin(), runlen.end());
    poslist_.assign(poslist.begin(), poslist.end());
}
#include <string.h>
bool Location::getMovingDistance(std::vector<Pos>& poslist)
{
    movemap_.clear();
    bool normal = true;
    for (int i=0; i<poslist.size(); i++) {
        for (int j=0; j<poslist_.size(); j++) {
            if (poslist[i].index == poslist_[j].index && poslist[i].index != -1) {
                int k = poslist[i].index;
                int d_h = abs(poslist[i].head - poslist_[j].head);
                int d_t = abs(poslist[i].tail - poslist_[j].tail);
                movemap_[k] = std::make_pair(d_h, d_t);
                if (((poslist[i].head >= load_range_min_ && poslist[j].head <= load_range_max_) || 
                    (poslist_[i].head >= load_range_min_ && poslist_[j].head <= load_range_max_)) && 
                    d_h > leap_th_) {
                    char text[128];
                    sprintf(text, "%dth head move %d", poslist[i].index, d_h);
                    logger_(text, strlen(text));
                    normal = false;
                }
                if (((poslist[i].tail >= load_range_min_ && poslist[j].tail <= load_range_max_) || 
                    (poslist_[i].tail >= load_range_min_ && poslist_[j].tail <= load_range_max_)) &&
                    d_t > leap_th_) {
                    char text[128];
                    sprintf(text, "%dth tail move %d", poslist[i].index, d_t);
                    logger_(text, strlen(text));
                    normal = false;
                }
            }
        }
    }
    return normal;
}

void Location::convert2Poslist(std::bitset<SN>& data, std::vector<Pos>& poslist)
{
	int tail = 0,len = 0,head = 0;
    int index = index_max_;
    std::vector<int> runlen;
    runlen = runLengthEncode(data);
	for (int i = 0; i < runlen.size(); i++) 
	{
		tail += len;
		len = runlen[i];
		head += len;
		if (i % 2 == (1 - data[0])) {
            poslist.push_back(Pos(head, tail, index));
			index--;
		}
	}
}

bool Location::checkRunLength(const std::vector<int>& runlen)
{
    if (abs(runlen.size() - runlen_.size()) == 2) return false;
    return true;
}

#include <sys/time.h>
static long long ustime(void) {
    struct timeval tv;
    long long ust;

    gettimeofday(&tv, NULL);
    ust = ((long long)tv.tv_sec) * 1000000;
    ust += tv.tv_usec;
    return ust;
}
static long long mstime(void) { return ustime() / 1000; }

bool Location::getAverageSpeed(std::vector<Pos>& poslist, float* speed)
{
	
	long long now = mstime();
	if (posbuf_.size() == 0) {
        posbuf_[now] = poslist;
        return false;
    }

    std::map<long long, std::vector<Pos>>::iterator it = posbuf_.begin();
    if ((now - it->first) < 1000) {
        return false;
    }    

    int moves = 0, cnt = 0;
    float spd = 0.f;
	for (int i = 0; i < it->second.size(); ++i) {
		for (int j = 0; j < poslist.size(); ++j) {
			if (it->second[i].index == poslist[j].index)
			{
				if (it->second[i].head < SN && it->second[i].head > 0 && 
                    poslist[j].head < SN && poslist[j].head > 0) 
                {
					moves += poslist[j].head  - it->second[i].head;
					if (moves != 0) cnt++;
				}

				if (it->second[i].tail < SN && it->second[i].tail > 0 && 
                    poslist[j].tail < SN && poslist[j].tail > 0) 
                {
					moves += poslist[j].tail - it->second[i].tail;
					if (moves != 0) cnt++;
				}
			}
		}
	}
		
	if (cnt != 0) {
		spd = (moves*0.04) / ((now-it->first) * cnt *0.001);
	}

    posbuf_.clear();
    posbuf_[now] = poslist;

	//printf("move %d speed %.3f\n", moves, spd);
    return true;
}