#include "controller.h"
#include "IO/IMTProducerSourceBase.h"
#include "IO/IMTConsumerSourceBase.h"
#include "IO/MTConsumerMgr.h"
#include "IO/MTProducerMgr.h"
#include "Common/Util.h"
#include <stdarg.h>
#include <sys/time.h>
#include "json.hpp"
#include <Configurations/IMTConfig.h>
#include <Configurations/MTConfigMgr.h>
#include <iostream>

using MT::Util::Configurations::IMTConfig;
using MT::Util::Configurations::MTConfigMgr;
using MT::Util::Configurations::MTConfigSourceType;

using MT::Util::IO::MTConsumerMgr;
using MT::Util::IO::IMTProducerSourceBase;
using MT::Util::IO::MTProducerMgr;
using MT::Util::IO::IMTConsumerSourceBase;
using MT::Util::IO::IMTIOCallback;
using MT::Util::IO::MTIOBuffer;

#ifdef _WIN32
#include <direct.h>
#include <io.h>
#else
#include <stdarg.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#endif

#ifdef _WIN32
#define ACCESS _access
#define MKDIR(a) _mkdir((a))
#else
#define ACCESS access
#define MKDIR(a) mkdir((a),0755)
#endif

#define LOG_MAX_LEN 2048
using nlohmann::json;


// Global variable
struct controllerServer server;


bool makeDirectory(const std::string &directory_path) {
	if (directory_path.empty()) {
		printf("[Error] Create directory[%s] failed.\n", directory_path.c_str());
		return false;
	}
	std::string directory_path_temp = directory_path;

	if (directory_path_temp.back() != '\\' &&directory_path_temp.back() != '/')
	{
		directory_path_temp.push_back('/');
		directory_path_temp.push_back('\0');
	}

	for (int index = 0; index < directory_path_temp.size(); ++index)
	{
		if (directory_path_temp[index] == '\\' || directory_path_temp[index] == '/')
		{
			directory_path_temp[index] = '\0';

			int exist = ACCESS(directory_path_temp.c_str(), 0);
			if (exist != 0)
			{
				int succeed = MKDIR(directory_path_temp.c_str());
				if (succeed != 0)
				{
					return false;
				}
			}
			directory_path_temp[index] = '/';
		}
	}
	return true;
}

void serverLog(int console, enum MTLogLevel level, const char *fmt, ...) {
    va_list ap;
    char msg[LOG_MAX_LEN];

    va_start(ap, fmt);
    vsnprintf(msg, sizeof(msg), fmt, ap);
    va_end(ap);
	
    server.logger->Log(level, "%s", msg);
	if (console && server.print == 1) {
		time_t t = time(NULL);
		char* tstr = ctime(&t);
		tstr[strlen(tstr)-1] = '\0';
		DBG(printf("%s--%s\n", tstr,msg);)
	}
}

/* Return the UNIX time in microseconds */
static long long ustime(void) {
    struct timeval tv;
    long long ust;

    gettimeofday(&tv, NULL);
    ust = ((long long)tv.tv_sec) * 1000000;
    ust += tv.tv_usec;
    return ust;
}

/* Return the UNIX time in milliseconds */
static long long mstime(void) { return ustime() / 1000; }


class SimpleAction :public IAction {
public:
	SimpleAction() :m_lasttime(0) { }

	bool Init(std::string& channel, std::string& connstr)
	{
		return m_cli.Open(connstr);
	}


	// Output alarm message to alarm channel.
	void AlarmOut(std::string pointId, int pointValue)
	{
		nlohmann::json j;
		j["pointId"] = pointId;
		j["pointValue"] = pointValue;
		m_cli.Publish(ALARM_CHANNEL, j.dump().c_str());
		if (m_cli.Publish("debug", j.dump().c_str())) {
			serverLog(1, MTLogLevel::Trace, "Send alarm:\n%s\n", j.dump().c_str());
		} else {
			serverLog(1, MTLogLevel::Trace, "Send alarm:\n%s\n falied.", j.dump().c_str());
		}
	}
	
	bool Action(TLSState& state, int seq, int total) 
	{
		int offset = 1;
		float dstHeight = server.model.m_rtmodel[seq].dis;
		std::string msg("h:");
		int h = (int)dstHeight;

		// if arrive 1th point. Validate the DLC weight
		int cnt = 0;
		if (seq == 0) {
			std::string okcount;
			if (m_cli.Get("12001",okcount) > 0) {
				cnt = std::stoi(okcount);
				if (cnt != state.m_carIndex) {
					serverLog(ON, MTLogLevel::Fatal, "Supply not ok.supply num=[%d] car index=[%d]\n",cnt, state.m_carIndex);
					AlarmOut(COAL_FILL_ALARM_ID, 1);
					#ifdef HAVE_WARN_INDEX_KEEP
					server.supplyWarn = true;
					#endif
					/* 
					 * If not set cmd to pause, controller will imediately load the next car
					 * although web server set the cmd to pasue.
					 * This will casuse controller must load the next car in order to pause.
					 */
					std::string p = std::to_string(PAUSE);
					m_cli.Set(Command_Keyname, p.c_str());
					return false;
				}
			} else{
				serverLog(ON, MTLogLevel::Fatal, "Access 12001 value failed.");
				return false;
			}
			#ifdef HAVE_WARN_INDEX_KEEP
			server.supplyWarn = false;
			#endif
			serverLog(ON, MTLogLevel::Trace, "Supply ok.supply num=[%d] car index=[%d]\n",cnt, state.m_carIndex);
		}  

		if (total == 8 && seq == 0) {
			// send "open valve" message
			msg.append(VALVE_OPEN);
			goto sendmsg;
		}

		if (h != 0) {
			msg.append(std::to_string(h));
			if (total == 7 && seq == 0) {
				msg.append(":");
				msg.append(VALVE_OPEN);
			}
		}
		else {
			if (state.m_weight > server.weightExcess
				&& (total-seq) == 1 ) { 
				AlarmOut(AUTOREGULATION_ALARM_ID, 1);
				return false;
			}
			else {
				if (total == 7) {
					offset = 1;	
				} else {
					offset = 0;
				}
				serverLog(ON, MTLogLevel::Trace, "seq=%d total=%d [Realtime weight=%.2f] [Model weight-adjust=%.2f] [Weight bias=%.2f]", 
				seq, total, state.m_weight, server.model.m_rtmodel[seq].weight, server.weightBias[seq-4+offset]);

				if (state.m_weight < (server.model.m_rtmodel[seq].weight - server.weightBias[seq-4+offset])) {
					#ifdef HAVE_SHAKE
					/* Special handle for 5th point */
					if ((seq-4+offset) == 0) {
						serverLog(ON, MTLogLevel::Trace, "Send shake-shake.\n");
						msg.append(LC_SHAKE);
					} else {
					#endif
						serverLog(ON, MTLogLevel::Trace, "Send down.\n");
						msg.append(LC_DOWN);
					#ifdef HAVE_SHAKE
					}
					#endif
					
				}
				else if (fabs(state.m_weight - server.model.m_rtmodel[seq].weight) <= server.weightBias[seq-4+offset]) {
					serverLog(ON, MTLogLevel::Trace, "Send nothing.\n");
					return true;
				}
				else {
					#ifdef HAVE_SHAKE
					if ((seq-4+offset) == 0) {
						serverLog(ON, MTLogLevel::Trace, "Send shake-shake.\n");
						msg.append(LC_SHAKE);
					} else {
					#endif
						serverLog(ON, MTLogLevel::Trace, "Send up.\n");
						msg.append(LC_UP);
					#ifdef HAVE_SHAKE
					}
					#endif
				}
			}
		}
sendmsg:
		long dt = (mstime()- m_lasttime);
		/* 
		 * Train stop between (p2,p5) points that is (0,3) in the program, 
		 * so if server.stopPoint == 1 means stop in (p2,c23) points no use    
		 *    if server.stopPoint == 2 means stop in [c23-c34) points use cmd_interval23
		 * 	  if server.stopPoint == 3 means stop in [c34,p5) points use cmd_interval23&cmd_interval34
		 * */
		if (total == 7 && seq == 1 && (server.stopPoint == 2 || server.stopPoint == 3)) {
			serverLog(ON, MTLogLevel::Trace, "Time from cmd1 = [%d], configured interval = [%d]\n",dt, server.cmd_interval32);
			if (dt < server.cmd_interval32) {
				usleep((server.cmd_interval32-dt) * 1000);
			}
		} else if (total == 7 && seq == 2 && server.stopPoint == 3) {
			serverLog(ON, MTLogLevel::Trace, "Time from cmd2 = [%d], configured interval = [%d]\n",dt, server.cmd_interval43);
			if (dt < server.cmd_interval43) {
				usleep((server.cmd_interval43-dt) * 1000);
			}
		} else {
			// Default command interval 100ms
			if (dt < 100) {
				usleep((100-dt) * 1000);
			}
		}
		
		serverLog(ON, MTLogLevel::Trace, "Send raw msg: %s", msg.c_str());
		m_cli.Publish(CONTROL_CHANNEL, msg.c_str());
		m_lasttime = mstime();
		return true;
	}
private:
	MTRedisClient m_cli;
	long long m_lasttime;
};

int updateParam(json& jobject)
{
	try {
		server.startLoadIndex = stoi(jobject["start_load_index"].get<std::string>());
		server.cmd_interval32 = stoi(jobject["cmd_interval32"].get<std::string>()); 
		server.cmd_interval43 = stoi(jobject["cmd_interval43"].get<std::string>());
		server.cmd_interval54 = stoi(jobject["cmd_interval54"].get<std::string>());
		server.weightK = stof(jobject["weight_k_coef"].get<std::string>());
		#if CONFIGURE_FROM_FILE
		server.weightBias[0] = stof(jobject["weight_bias5"].get<std::string>());
		server.weightBias[1] = stof(jobject["weight_bias6"].get<std::string>());
		server.weightBias[2] = stof(jobject["weight_bias7"].get<std::string>());
		#endif
	} catch(...) {
		return -1;
	}
	return 0;
}

class MessageCallback : public IMTIOCallback
{
public:
	MTRedisClient confCli;

	MessageCallback()
	{
		confCli.Open(server.redisCache);
	}

	void Callback(MTIOBuffer* MTInputParamer)
	{
		if (nullptr == MTInputParamer || 
			nullptr == MTInputParamer->m_Data || 
			MTInputParamer->m_DataSize <= 0) {
			std::string message = "上报数据异常。";
			std::cout << message.c_str() << std::endl;
			return;
		}

		if (MTInputParamer->m_Error) {
			std::string message;
			ConnectionStatus status = *(ConnectionStatus*)MTInputParamer->m_Data;
			if (ConnectionStatus::DISCONNECTED == status) {
				message = "频道网络连接异常，尝试重连。。。";

				std::cout << message.c_str() << std::endl;

				if (!GetConsumer()->ReOpen()) {
					message = "频道网络重连失败。";
					std::cout << message.c_str() << std::endl;
				} else {
					message = "频道网络重连成功。";
					std::cout << message.c_str() << std::endl;
				}
			} else {
				message = "频道网络状态未知异常。";
				std::cout << message.c_str() << std::endl;
			}
		} else {
			if (strncasecmp((char*)MTInputParamer->m_Data, "paramUpdate", 11) == 0) {
				/* System param update */
				std::string jsonstr;
				if (confCli.Get(ConfigParam_Keyname, jsonstr) < 1) {
					serverLog(ON, MTLogLevel::Fatal, "Get ConfigParam_Keyname value failed.");
				}
				json jobject;
				
				jobject = json::parse(jsonstr.c_str());
				if (updateParam(jobject)) {
					serverLog(ON, MTLogLevel::Fatal, "Parse param json string failed.");
				} 
				//else {
					serverLog(ON, MTLogLevel::Info, 
					"Update.[%d %d %d %d]",server.startLoadIndex, server.cmd_interval32, server.cmd_interval43, server.cmd_interval54);
				//}
			} else if (strncasecmp((char*)MTInputParamer->m_Data, "modelUpdate", 11) == 0) {
				/* Model param update */
			}
		}
	}
};

bool RedisGetValue(MTRedisClient& cli, std::string keyname, std::string& strValue)
{
	int ret = 0;
	ret = cli.Get(keyname.c_str(), strValue);
	if (ret == -1) {
		if (cli.ReConnect()) {
			cli.Get(keyname.c_str(), strValue);
		}
		else {
			serverLog(ON, MTLogLevel::Error, "Get [%s] value failed.", keyname.c_str());
			return false;
		}
	}
	else if (ret == 0) {
		//serverLog(ON, MTLogLevel::Error, "Keyname [%s] is not existed.", keyname.c_str());
		return false;
	}
	return true;
}

bool RedisGetValueInt(MTRedisClient& cli, std::string keyname, int& value)
{
	std::string strValue;
	RedisGetValue(cli, keyname, strValue);
	value = atoi(strValue.c_str());
	return true;
}

bool RedisGetValueFloat(MTRedisClient& cli, std::string keyname, float& value)
{
	std::string strValue;
	RedisGetValue(cli, keyname, strValue);
	value = atof(strValue.c_str());
	return true;
}

bool initController()
{
	makeDirectory(LogPath);
	MTLogMgr::GetInstance().Start();
	server.logger = MTLogMgr::GetInstance().GetLogger("controller", LogPath);

	serverLog(ON, MTLogLevel::Trace, "Load [%s]...\n", INI_FILE_NAME);
	std::shared_ptr<MT::Util::Configurations::IMTConfig> pMTConfig
		= MTConfigMgr::GetInstance()->GetConfig(MTConfigSourceType::IniFile);
	if (pMTConfig->Open(INI_FILE_NAME) == false) {
		serverLog(ON, MTLogLevel::Fatal,"Fail to open %s\n", INI_FILE_NAME);
		return false;
	}
	pMTConfig->Load("default");
	server.redisCache = pMTConfig->GetValue("cache");
	server.configChannel = pMTConfig->GetValue("config_channel");
	serverLog(ON, MTLogLevel::Trace, "conf-channel : [%s]", server.configChannel.c_str());
	server.controlChannel = pMTConfig->GetValue("control_channel");
	server.alarmChannel = pMTConfig->GetValue("alarm_channel");
	server.logLevel = pMTConfig->GetValue("log_level");
	server.update = 0;
	server.print = 0;
	server.stopPoint = -1;
	server.optimalSpeed = atof( pMTConfig->GetValue("optimal_speed").c_str() );
	server.speedAlgorithmK = atof( pMTConfig->GetValue("speed_algorithm_k").c_str() );
	server.LCSpeed = 200 ;
	server.weightK = 1.0;
	#ifdef HAVE_WARN_INDEX_KEEP
	server.supplyWarn = false;
	#endif
	serverLog(ON, MTLogLevel::Trace, "Main redis client connect server@[%s].\n", server.redisCache.c_str());
	if (!server.mainCli.Open(server.redisCache)) {
		serverLog(ON, MTLogLevel::Fatal, "Fail.");
		return false;
	}
	serverLog(ON, MTLogLevel::Info, "Success.");

	std::string jsonstr;
	int redisConfiged = 0;
	if (server.mainCli.Get(ConfigParam_Keyname, jsonstr) < 1) {
		serverLog(ON, MTLogLevel::Fatal, "Get ConfigParam_Keyname value failed.");
	} else {
		json jobject;
		jobject = json::parse(jsonstr.c_str());
		if (updateParam(jobject)) {
			serverLog(ON, MTLogLevel::Fatal, "Parse param json string failed.");
		} else {
			redisConfiged = 1;
		}
	}

	pMTConfig->Load("controller");

	if (redisConfiged == 0) {
		server.startLoadIndex = atoi(pMTConfig->GetValue("start_load_index").c_str());
		if (server.startLoadIndex < 1) {
			server.startLoadIndex = 1;
			serverLog(ON, MTLogLevel::Warn, "Set start load index to 1.");
		}
		#ifdef CONFIGURE_FROM_FILE
		server.weightBias[0] = atof(pMTConfig->GetValue("weight_bias5").c_str());
		server.weightBias[1] = atof(pMTConfig->GetValue("weight_bias6").c_str());
		server.weightBias[2] = atof(pMTConfig->GetValue("weight_bias7").c_str());
		#endif
		server.cmd_interval32 = 100; 
		server.cmd_interval43 = 100;
		server.cmd_interval54 = 100;
	}
	
	#ifdef CONFIGURE_FROM_FILE
	server.weightExcess = atof(pMTConfig->GetValue("weight_excess").c_str());
	server.posMin = atoi(pMTConfig->GetValue("pos_min").c_str());
	server.posMax = atoi(pMTConfig->GetValue("pos_max").c_str());
	#endif
	serverLog(ON, MTLogLevel::Trace, "Load configuration(%s) success.", INI_FILE_NAME);

	MTLogMgr::GetInstance().SetMaxLogLevel(server.logLevel);
	serverLog(ON, MTLogLevel::Warn, "Set logLevel to [%s].", server.logLevel.c_str());
	
	server.loadIndex = server.startLoadIndex;
	server.prevState = STOP;
	server.totalCar = 0;
	server.speed = 0.f;
	server.isRecovery = false;

	server.mainCli.Set(Load_Done_Index_keyname, std::to_string(server.loadIndex-1).c_str());
	server.mainCli.Set(LoadingIndex_Keyname, std::to_string(server.loadIndex-1).c_str());

	serverLog(ON, MTLogLevel::Trace, "controller init success.");
	return true;
}

/**
 * topic : null-terminated string.
 */
static bool subscribeTopic(const char* topic, IMTIOCallback* cb)
{
	serverLog(ON, MTLogLevel::Info, "Subscribe channel. Address => %s", topic);
	IMTConsumerSourceBase* consumer
		= MTConsumerMgr::GetInstance().Create("Location", topic);
	if (!consumer) {
		serverLog(ON, MTLogLevel::Fatal, "Fail.");
		return false;
	}
	consumer->Register(cb);
	if (!consumer->Open()) {
		serverLog(ON, MTLogLevel::Fatal, "Fail.");
		return false;
	}
	serverLog(ON, MTLogLevel::Info, "Success.");
	return true;
}

void resetSystem()
{
	server.loadIndex = server.startLoadIndex;
	server.model.m_trainModel.clear();
	server.monitor.Reset();
	server.mainCli.Set(Load_Done_Index_keyname, std::to_string(server.loadIndex-1).c_str());
	server.mainCli.Set(LoadingIndex_Keyname, std::to_string(server.loadIndex-1).c_str());
	serverLog(ON, MTLogLevel::Fatal, "Reset system.");
}

void stopSystem()
{
	resetSystem();
	std::string stop = std::to_string(STOP);
	server.mainCli.Set(Command_Keyname, stop.c_str());
	server.prevState = STOP;
	server.mainCli.Publish("TLSControlChannel", "23006");
	serverLog(ON, MTLogLevel::Fatal, "Stop system.");
}


/**
 * Wait until system becomes RUN and Coming coal is OK and the first three train model is OK 
 * It will set recovery variable to true when state becomes RUN from PAUSE or from STOP  
 */
void waitIndicationAndCoal()
{
	time_t t0 = time(NULL); /* for RUN state timing */
	time_t t1 = time(NULL); /* for FirstThreeTrain timing */
	time_t t2 = time(NULL); /* for ComingCoal timing */

	int currState = 0; 
	bool cond = false;
	#ifdef HAVE_COMING_COAL
	int coal = 0;
	#endif

	#ifdef HAVE_SET_MODEL
	int model = 0;
	#endif

	/* when entering this loop reset the isRecovery state */
	server.isRecovery = false;

	do{
		RedisGetValueInt(server.mainCli, Command_Keyname, currState);
		#ifdef HAVE_SET_MODEL
		RedisGetValueInt(server.mainCli, Model_Set_Keyname, model);
		#endif

		#ifdef HAVE_COMING_COAL
		RedisGetValueInt(server.mainCli, Coal_Insufficient_Keyname, coal);
		#endif

		if ((RunningState)currState == STOP) {
			if (server.prevState != STOP){
				resetSystem();
				serverLog(ON, MTLogLevel::Info, "STOP!!!!!");
			}
		}
		else if ((RunningState)currState == PAUSE) {
			if (server.prevState != PAUSE) {
				serverLog(ON, MTLogLevel::Info, "PAUSE!!!!!");
			}	
		}
		else {
			if (server.prevState != RUN) {
				server.isRecovery = true;
				if (server.prevState == PAUSE) {
					serverLog(ON, MTLogLevel::Info, "RESTORE!!!!!");
				} else if (server.prevState == STOP) {
					resetSystem();	
					serverLog(ON, MTLogLevel::Info, "RUN!!!!!");
				}
				if (server.model.m_trainModel.empty()) {
					if (false == server.model.LoadTrainModelFromRedis(TrainModel_Keyname, server.mainCli, server.totalCar)) {
						serverLog(ON, MTLogLevel::Fatal, "Load train model name list fail.\n");
						exit(0);
					}
					serverLog(ON, MTLogLevel::Info, "%d cars need load.\n", server.totalCar);
					server.model.PrintModelNameList();
				}
			} 
			//else { /* Prev state is run  and Curr state is run too 
			//		  * Set recovery to false
			//		  */
			//	server.isRecovery = false;
			//}
		}
		server.prevState = (RunningState)currState;

		cond = ((RunningState)currState != RUN);
		if (cond && (time(NULL) - t0) > 20) {
			t0 = time(NULL);
			serverLog(ON, MTLogLevel::Info, "wait system start.\n");
		}

		#ifdef HAVE_COMING_COAL
		cond |= (coal == 0);
		if ((coal==0) && (time(NULL) - t2) > 20) {
			t2 = time(NULL);
			serverLog(ON, MTLogLevel::Info,"wait coming coal.\n");
		}
		#endif

		#ifdef HAVE_SET_MODEL
		/* At the beginning of load. The first three cars need set by chenchen app*/
		if (server.loadIndex == server.startLoadIndex) {
			cond |= (model == 0);
			if (model == 0 && (time(NULL) - t1) > 20) {
				t1 = time(NULL);
				serverLog(ON, MTLogLevel::Info,"wait model set\n");
			}
		}
		#endif
	} while(cond);

	serverLog(ON, MTLogLevel::Info,"Condition satisfied.\n"); //and coming coal is OK.

	/* Already find the flag is 1 and reset coming coal sufficient flag. */
	

	#ifdef HAVE_SET_MODEL
	if (server.loadIndex == (server.startLoadIndex + 1)) {
		server.mainCli.Set(Model_Set_Keyname, "0");
	}
	#endif

	#ifdef HAVE_COMING_COAL
	server.mainCli.Set(Coal_Insufficient_Keyname, "0");
	#endif
}

#define VERSION "2.3.0"
#define print_version() printf("version-%s\n", VERSION)

void constructJsonObject()
{
	auto j = R"({
				"weightBias5": 9.6,
				"weightBias6": 6.9,
				"weightBias7": 3.1,
				"posUpLimit": 1600,
				"posDownLimit": 800,
				"lastWeightTh": 40.0,
				"trainModelList": [
				{
				"position": 8400.0,
				"weight": 0.0,
				"height": 3100.0
				},
				{
				"position": 9800.0,
				"weight": 0.0,
				"height": 3660.0
				},
				{
				"position": 10800.0,
				"weight": 0.0,
				"height": 3660.0
				},
				{
				"position": 17600.0,
				"weight": 0.0,
				"height": 3550.0
				},
				{
				"position": 17600.0,
				"weight": 0.0,
				"height": 0.0
				},
				{
				"position": 17600.0,
				"weight": 0.0,
				"height": 0.0
				},
				{
				"position": 17600.0,
				"weight": 0.0,
				"height": 0.0
				},
				{
				"position": 7040.0,
				"weight": 0.0,
				"height": 3100.0
				}]
				})"_json;
	server.mainCli.Set("a", j.dump().c_str());
}

void testLoadModelFromRedis()
{
	ControlModel m;
	m.m_trainModel.push_back("a");

	m.LoadModelAndParamFromRedis(0,server.mainCli,0);
	printf("%f %f %f %d %d %f\n", server.weightBias[0],server.weightBias[1],server.weightBias[2],server.posMax, server.posMax, server.weightExcess);
}

int main(int argc, char** argv)
{
	/* Handle special -v option */
	if (argc == 2 && argv[1][1] == 'v') {
		print_version();
		return 0;
	}

	bool success = false;
	success = initController();
	if (!success) {
		serverLog(OFF, MTLogLevel::Fatal, "Init controller fail. Exit.\n");
		exit(1);
	}

	int c;
	while ((c = getopt(argc, argv, "d")) != -1) {
        switch (c) {
        case 'd':
            server.print = 1;
            break;
        default:
            fprintf(stderr, "Usage: %s [-v] [-d]\n", argv[0]);
            exit(1);
        }
    }
	
#ifdef MODEL_TEST
	nlohmann::json j;
	j["pointId"] = COAL_FILL_ALARM_ID;
	j["pointValue"] = 1;
	server.mainCli.Publish(ALARM_CHANNEL, j.dump().c_str());
	getchar();
	constructJsonObject();
	testLoadModelFromRedis();
	getchar();
	return 1;
#endif
	MessageCallback configCallback;
	if (!subscribeTopic(server.configChannel.c_str(), &configCallback)) {
		exit(1);
	}
	
	if (!server.monitor.Init(server.redisCache)) {
		serverLog(ON, MTLogLevel::Fatal, "Monitor init fail.");
		exit(1);
	}
	SimpleAction* simplePhase = new SimpleAction();
	if (!simplePhase->Init(server.controlChannel, server.redisCache)) {
		serverLog(ON, MTLogLevel::Fatal, "Create message producer fail.");
		exit(1);
	}

	while (true) {
		waitIndicationAndCoal();		
		success = server.model.LoadModelAndParamFromRedis((server.loadIndex-1), server.mainCli, server.isRecovery);
		if (success == false) {
			serverLog(ON, MTLogLevel::Fatal, "Load %dth model failed.",server.loadIndex);
			stopSystem();
			continue;
		}
		server.model.LoadInitModel((server.loadIndex-1), server.mainCli, server.isRecovery);

		server.monitor.UnRegister();
		for (std::vector<Param>::iterator iter = server.model.m_rtmodel.begin();
			iter != server.model.m_rtmodel.end(); ++iter) {
			server.monitor.Register(iter->pos, simplePhase);
		}

		/*if recovery, reset the recovery flag */
		//server.isRecovery = !server.isRecovery;
#ifdef GIVE_STOPRANGE 
		/**
		 * When program at this point, we will load the car
		 * The car must be ready for loading. 
		 */
		if (server.isRecovery) {
		
			json j;
			j["index"] = server.loadIndex;
			j["range"] = { server.model.m_rtmodel[0].pos, server.model.m_rtmodel[2].pos };
			j["status"] = 0;
			printf("Set StopRange.\n");
			server.mainCli.Set(CarInStopRange_Keyname, j.dump().c_str());
		
			RedisGetValueFloat(server.mainCli, TrainSpeed_Keyname, server.speed);
			if (fabs(server.speed) > 1e-7) {
				if (server.satisfied) {
					serverLog(ON, MTLogLevel::Warn, "Warn!! Moving!!");
				}
				server.satisfied = false;
				j["status"] = 0;
				server.mainCli.Set(CarInStopRange_Keyname, j.dump().c_str());
				continue;
			}
			
			if (!server.monitor.UpdateState(server.loadIndex)) {
				serverLog(ON, MTLogLevel::Fatal, "Update state failed(%d).",server.loadIndex);
				return EXIT_FAILURE;
			}
			if (server.monitor.m_state.m_headPos >= server.model.m_rtmodel[2].pos || 
				server.monitor.m_state.m_headPos < server.model.m_rtmodel[0].pos) {
				if (server.satisfied) {
					serverLog(ON, MTLogLevel::Warn, "Warn!! Head not fixed!![%f:(%d,%d)].", \
					server.monitor.m_state.m_headPos , server.model.m_rtmodel[0].pos, server.model.m_rtmodel[2].pos);
				}
				
				server.satisfied = false;
				server.mainCli.Set(CarInStopRange_Keyname, j.dump().c_str());
				continue;
			}
			j["status"] = 1;
			server.mainCli.Set(CarInStopRange_Keyname, j.dump().c_str());
		}
	#endif
		//server.satisfied = true;
		serverLog(ON, MTLogLevel::Info, "Start to load %dth(%zd pts)...", server.loadIndex, server.model.m_rtmodel.size());
		server.monitor.m_state.m_carIndex = server.loadIndex;
		server.mainCli.Set(LoadingIndex_Keyname, std::to_string(server.loadIndex).c_str());
		server.monitor.Observe(server.loadIndex, server.isRecovery);

		#ifdef HAVE_WARN_INDEX_KEEP
		if (server.supplyWarn == false) { /* There are no supply alarm, increase the loadindex */
			serverLog(ON, MTLogLevel::Info, "There are no supply alarm, increase the loadindex.");
			++server.loadIndex;
		} else {
			serverLog(ON, MTLogLevel::Info, "supply alarm, keep the loadindex.");
		}
		#endif
		++server.loadIndex;

		if (server.loadIndex > server.totalCar) {
			stopSystem();
			serverLog(ON, MTLogLevel::Info, "Train load finish! Total loads %d\n", server.totalCar);
		}
		server.mainCli.Set(Load_Done_Index_keyname, std::to_string(server.loadIndex).c_str());		
	}
	return EXIT_SUCCESS;
}