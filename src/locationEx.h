#ifndef LOCATIION_HEADER_FILE
#define LOCATIION_HEADER_FILE

#include <map>
#include <bitset>
#include <vector>
#include <mutex>

#define SN 400

enum enReturnType{
    TYPE_OK = 0,
    TYPE_ERR_NOISE,
    TYPE_ERR_MISINDEX,
    TYPE_ERR_LEAP,
    TYPE_ERR_UNKOWN
};

struct ReturnMsg{
    enReturnType retcode;
    const std::string rtmsg;
};


const struct ReturnMsg rtmsg[] = {
    {TYPE_OK,           "Get pos success"},
    {TYPE_ERR_NOISE,    "Too much noise"},
    {TYPE_ERR_MISINDEX, "Car fly away"},
    {TYPE_ERR_LEAP,     "Car leap"},
    {TYPE_ERR_UNKOWN,   "Unkown error"}
};
    

typedef struct Pos{
    Pos(int h, int t, int i):head(h),tail(t),index(i){}

    int head;
    int tail;
    int index;
} Pos;

typedef void (*MessageFunc)(const char* str, int len);

class Location {
public:
    Location(int index,int leap_th, int one_th, int zero_th, int new_th, int loadmin, int loadmax);
    ~Location();

public:
    enReturnType GetTrainPos(std::bitset<SN>& data, std::vector<Pos>& poslist, float* speed);
    void SetCarIndex(int index);
    void RegCallback(MessageFunc func);

private:
    bool denoise(std::bitset<SN>&);
    void de_one(std::bitset<SN>&);
    void de_one_again(std::bitset<SN>&);
    void de_zero(std::bitset<SN>&);
    bool de_znum_onum(std::bitset<SN>&);
    void check_10101(std::bitset<SN>&, std::vector<int>& runlen);
    void check_101(std::bitset<SN>&, std::vector<int>& runlen);

    void check_trainhead_1010(std::bitset<SN>&, std::vector<int>& runlen);

    // TODO 第二个1小于第一个1
    //void check_1010(std::bitset<SN>&, std::vector<int>& runlen);

    
    /*获取游程编码*/
    std::vector<int> runLengthEncode(const std::bitset<SN>&);

    /* 反转数据，长度列表， 段序号*/
    void flipData(std::bitset<SN>& data, std::vector<int> lens, int idx);

    /* true正确/false错误 */
    bool checkRunLength(const std::vector<int>& runlen);

    /* 光幕数据 转换为 车厢坐标*/
    void convert2Poslist(std::bitset<SN>& data, std::vector<Pos>& poslist);

    /* 前后两帧车厢移动距离 true正常/false跳跃 */
    bool getMovingDistance(std::vector<Pos>& poslist);

    /* 计算车速 */
    bool getAverageSpeed(std::vector<Pos>& poslist, float* speed);

    /* 更新数据 */
    void updateUsefulThing(const std::vector<int> runlen, const std::vector<Pos>&, int, float);
private:

    /* 最大车厢号 */
    int index_max_;

    /* 车厢号互斥量 */
    std::mutex mtx_; 

    /* 车厢坐标和序号列表 */
    std::vector<Pos> poslist_;

    /* 车速 */
    float speed_;

    /* 超出光幕，记录坐标*/
    int h_offset_;

    /* 光幕第一个点值*/
    int firstval_;

    /* 游程 */
    std::vector<int> runlen_;

    //std::vector<std::bitset<SN>> bit_list_;

    /* 缓存历史车厢位置坐标 */
    std::map<long long, std::vector<Pos>> posbuf_;

    /* 车厢移动距离 */
    std::map<int, std::pair<int,int>> movemap_;

    /* 去1阈值*/
    int one_noise_th_;

    /*去0阈值*/
    int zero_noise_th_;

    /* 新进入一节车阈值*/
    int newcar_th_;

    /* 装车范围 */
    int load_range_min_;
    int load_range_max_;

    /* 跳跃阈值 */
    int leap_th_;

    /* true:锁定节号*/
    bool lock_;

    /* 消息回调 */
    MessageFunc logger_;
};

#endif