#include <iostream>
#include <cstring>        // for strcat()
#include <vector>
#include <algorithm>
#include <array>
#include <vector>
#include <queue>

#include <IO/MTConsumerMgr.h>
#include <IO/IMTConsumerSourceBase.h>
#include <IO/IMTIOCallback.h>
#include <Logging/MTLogMgr.h>
#include <Logging/IMTLog.h>
#include <Configurations/IMTConfig.h>
#include <Configurations/MTConfigMgr.h>
#include <Net/MTRedisClient.h>


#include <json.hpp>
#if 0
#include "Iir.h"
#endif
#include <sys/time.h>
#include "location.h"
#include "denoise.h"
#include "Util.h"

using MT::Util::Net::MTRedisClient;
using MT::Util::Configurations::IMTConfig;
using MT::Util::Configurations::MTConfigMgr;
using MT::Util::Configurations::MTConfigSourceType;

using MT::Util::IO::MTConsumerMgr;
using MT::Util::IO::IMTConsumerSourceBase;
using MT::Util::IO::IMTIOCallback;
using MT::Util::IO::MTIOBuffer;
using MT::Util::Logging::MTLogMgr;
using MT::Util::Logging::IMTLog;

using nlohmann::json;

#ifdef _WIN32
#include <direct.h>
#include <io.h>
#else
#include <stdarg.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#endif

#ifdef _WIN32
#define ACCESS _access
#define MKDIR(a) _mkdir((a))
#else
#define ACCESS access
#define MKDIR(a) mkdir((a),0755)
#endif

#define VERSION "3.0"
#define print_version() printf("version-%s\n", VERSION)


class MessageCallback;

Location* locRef = NULL;

struct LocationConfig {
	IMTLog* appLogger;  // store the app log
	IMTLog* dataLogger; // store screen data
	char iniFile[256]; // ini file name
	FILE* fd; // log file descriptor
	char filename[256]; // log file name to read

	std::string redisCache; // redis cache connect string
	std::string alarmChannel; // alarm channel connect string
	std::string configChannel;
	std::string logLevel; // log level

	int startIndex; // train start index
	int loadmin;
	int loadmax;
	int oneth;
	int zeroth;
	int leapth;
	int newth;

	int minCarLen;

	int noise_tolerance_;
	int misindex_tolerance_;
	int leap_tolerance_;

	int sleeptime; // sleep time after one loop

	int updateParameter; // 更新参数

	int screenStartIndex; //光幕redis起始键
	std::string speedKeyname;
	std::string positionKeyname;
	int denoiseAlarmID;
	int leapAlarmID;
	int indexAlarmID;

	MTRedisClient mainCli; /* Network */
};

static LocationConfig locsrv;

class MessageCallback : public IMTIOCallback
{
public:
	void Callback(MTIOBuffer* MTInputParamer)
	{
		if (nullptr == MTInputParamer || 
			nullptr == MTInputParamer->m_Data || 
			MTInputParamer->m_DataSize <= 0) {
			std::string message = "上报数据异常。";
			std::cout << message.c_str() << std::endl;
			return;
		}

		if (MTInputParamer->m_Error) {
			std::string message;
			ConnectionStatus status = *(ConnectionStatus*)MTInputParamer->m_Data;
			if (ConnectionStatus::DISCONNECTED == status) {
				message = "频道网络连接异常，尝试重连。。。";

				std::cout << message.c_str() << std::endl;

				if (!GetConsumer()->ReOpen()) {
					message = "频道网络重连失败。";
					std::cout << message.c_str() << std::endl;
				} else {
					message = "频道网络重连成功。";
					std::cout << message.c_str() << std::endl;
				}
			} else {
				message = "频道网络状态未知异常。";
				std::cout << message.c_str() << std::endl;
			}
		} else {
			char* addr = (char*)MTInputParamer->m_Data;
			if (strcmp(addr, "update") == 0) {
				locsrv.updateParameter = 1;
			} else if (strncmp(addr, "correctIndex:", sizeof("correctIndex:"))) {
				int len = sizeof("correctIndex:");
				int i = atoi(addr+len);
				locsrv.appLogger->Log(MTLogLevel::Info, "Recieve user-verified car index %d.", i);
				locRef->SetCarIndex(i);
			}
		}
	}
};

MessageCallback configCallback; /* Network Message channel*/

static int findLastCh(char ch, char* str)
{
	int offset = -1;
	int i = 0;
	while(i < strlen(str)) {
		if (str[i] == ch){
			offset = i;
		}
		i++;
	}
	return offset;
}

static bool makeDirectory(const std::string &directory_path)
{
	if (directory_path.empty()) {
		printf("[Error] Create directory[%s] failed.\n", directory_path.c_str());
		return false;
	}
	std::string directory_path_temp = directory_path;

	if (directory_path_temp.back() != '\\' &&directory_path_temp.back() != '/')
	{
		directory_path_temp.push_back('/');
		directory_path_temp.push_back('\0');
	}

	for (int index = 0; index < directory_path_temp.size(); ++index)
	{
		if (directory_path_temp[index] == '\\' || directory_path_temp[index] == '/')
		{
			directory_path_temp[index] = '\0';

			int exist = ACCESS(directory_path_temp.c_str(), 0);
			if (exist != 0)
			{
				int succeed = MKDIR(directory_path_temp.c_str());
				if (succeed != 0)
				{
					return false;
				}
			}
			directory_path_temp[index] = '/';
		}
	}
	return true;
}

static bool getDataFromDB(MTRedisClient& cli, std::bitset<SN>& screenData)
{
	//_selectMasterOrSlave();

	char msg[2048] = { 0 };
	for (int i=locsrv.screenStartIndex; i <= (locsrv.screenStartIndex+399); i++) {
		char key[32] = { 0 };
		sprintf(key, "%d", i);
		std::string value;
		int index = i - locsrv.screenStartIndex;
		int flg = cli.Get(key, value);
		if (flg == 0) {
			printf("Key %s does not exist.\n", key);
			sprintf(msg + strlen(msg), "%d ", -1);
			continue;
		}
		else if (flg == -1) {
			printf("Redis server disconnect.Try to reconnect...\n");
			locsrv.appLogger->Log(MTLogLevel::Fatal, "Redis server disconnect.Try to reconnect...");
			
			while(cli.ReConnect() == false){
				usleep(10000);
				printf("Reconnect fail.\n");
				continue;
			}
			locsrv.appLogger->Log(MTLogLevel::Fatal, "Reconnect success.");
			printf("Reconnect success.\n");
			i--;
			continue;
			return false;
		}

		screenData.set(index, value.compare("false") ? 1 : 0);
		sprintf(msg + strlen(msg), "%d", value.compare("false") ? 1 : 0);
	}
	locsrv.dataLogger->Log(MTLogLevel::Trace, "%s", msg);
	return true;
}

int GetData(std::bitset<SN>& initialData)
{
	static uint64_t row = 0;
	int offset = 44, len = 0;
	char line[2048] = { 0 };
	char msg[1024] = { 0 };
	if (locsrv.fd) {
		row++;
		locsrv.appLogger->Log(MTLogLevel::Debug, "row %lu:", row);
		printf("line=> %lu\n", row);
		if (fgets(line, 2048, locsrv.fd) == NULL) {
			printf("Read file end, app exit.\n");
			exit(1);
		}
		len = strlen(line);
		for (int i = 1; i<3; i++) {
			if (line[len-i] == '\r' || line[len-i] == '\n') {
				line[len - i] = 0;
			}
		}
		len = strlen(line);
		offset = findLastCh(':', line);
		offset++;
		char msg[2048] = {0};
		for (int i = offset; i < len; i = i + 1) {
			initialData.set((i - offset), (line[i] - 48));
			sprintf(msg + strlen(msg), "%d", (line[i] - 48));
		}
		locsrv.dataLogger->Log(MTLogLevel::Trace, "%s", msg);
	}
	else {
		if (!getDataFromDB(locsrv.mainCli, initialData)) {
			locsrv.appLogger->Log(MTLogLevel::Fatal, "Get screen data from redis fail.");
			return EXIT_FAILURE;
		}
	}
	return EXIT_SUCCESS;
}

void addkey() 
{
	MTRedisClient cli;
	cli.Open("cache://192.168.10.200:6379;");

	for (int i = SCREEN1174; i <= SCREEN1573; i++) {
		cli.Set(std::to_string(i).c_str(), "false");
	}
	cli.Close();
}

void sendAlarm(int pointId, int value)
{
	json jarray = json::array();
	json j;
	j["pointId"] = pointId;
	j["pointValue"] = value;
	jarray.push_back(j);
	locsrv.mainCli.Publish(ALARM_CHANNEL, jarray.dump().c_str());
}

bool createRedisClient()
{
	locsrv.appLogger->Log(MTLogLevel::Info, "Connect redis cache@[%s].", locsrv.redisCache.c_str());
	printf("Connect redis cache => %s\n", locsrv.redisCache.c_str());
	if (!locsrv.mainCli.Open(locsrv.redisCache.c_str())) {
		printf("Fail.\n");
		locsrv.appLogger->Log(MTLogLevel::Fatal, "Fail.");
		return false;
	}
	locsrv.appLogger->Log(MTLogLevel::Info, "Success.");
	printf("Success.\n");
	return true;
}

bool subscribeTopic(const char* topic, IMTIOCallback* cb)
{
	printf("Subscribe channel address => %s\n", topic);
	locsrv.appLogger->Log(MTLogLevel::Info, "Subscribe channel. Address => %s", topic);
	IMTConsumerSourceBase* consumer
		= MTConsumerMgr::GetInstance().Create("Location", topic);
	if (!consumer) {
		printf("Fail to create consumer object.\n");
		locsrv.appLogger->Log(MTLogLevel::Fatal, "Fail.");
		return false;
	}
	consumer->Register(cb);
	if (!consumer->Open()) {
		printf("Fail.\n");
		locsrv.appLogger->Log(MTLogLevel::Fatal, "Fail.");
		return false;
	}
	locsrv.appLogger->Log(MTLogLevel::Info, "Success.");
	printf("Success.\n...\n");
	return true;
}

bool Init()
{
	makeDirectory(LogPath);
	MTLogMgr::GetInstance().Start();
	locsrv.appLogger = MTLogMgr::GetInstance().GetLogger("location", LogPath);
	locsrv.dataLogger = MTLogMgr::GetInstance().GetLogger("raw", LogPath);
	if (!(locsrv.appLogger && locsrv.dataLogger)) {
		printf("Init log instance failed.\n");
		return false;
	}
	sprintf(locsrv.iniFile, "%s", INI_FILE_NAME);
	locsrv.appLogger->Log(MTLogLevel::Trace, "Load [%s]...", locsrv.iniFile);
	std::shared_ptr<MT::Util::Configurations::IMTConfig> pMTConfig
		= MTConfigMgr::GetInstance()->GetConfig(MTConfigSourceType::IniFile);
	if (pMTConfig->Open(locsrv.iniFile) == false) {
		printf("Fail to open %s\n", locsrv.iniFile);
		return false;
	}
	pMTConfig->Load("setting");
	locsrv.redisCache = pMTConfig->GetValue("cache");
	locsrv.alarmChannel = pMTConfig->GetValue("alarm_channel");
	locsrv.configChannel = pMTConfig->GetValue("config_channel");
	locsrv.logLevel = pMTConfig->GetValue("log_level").c_str();
	if (!subscribeTopic(locsrv.configChannel.c_str(), &configCallback)) {
		return false;
	}
	if (!createRedisClient()) {
		return false;
	}

	locsrv.startIndex = atoi(pMTConfig->GetValue("start_index").c_str());
	locsrv.loadmin = atoi(pMTConfig->GetValue("load_range_min").c_str());
	locsrv.loadmax = atoi(pMTConfig->GetValue("load_range_max").c_str());
	locsrv.oneth = atoi(pMTConfig->GetValue("one_noise_th").c_str());
	locsrv.zeroth = atoi(pMTConfig->GetValue("zero_noise_th").c_str());
	locsrv.leapth = atoi(pMTConfig->GetValue("leap_th").c_str());
	locsrv.newth = atoi(pMTConfig->GetValue("newcar_th").c_str());
	locsrv.minCarLen = atoi(pMTConfig->GetValue("min_car_length").c_str());

	locsrv.leap_tolerance_ = atoi(pMTConfig->GetValue("leap_tolerance").c_str());
	locsrv.noise_tolerance_ = atoi(pMTConfig->GetValue("noise_tolerance").c_str());
	locsrv.misindex_tolerance_ = atoi(pMTConfig->GetValue("misindex_tolerance").c_str());
	locsrv.sleeptime = atoi(pMTConfig->GetValue("sleep_time").c_str());

	locsrv.screenStartIndex = atoi(pMTConfig->GetValue("redis_screen_start_index").c_str());
	locsrv.speedKeyname = pMTConfig->GetValue("redis_speed_key");
	locsrv.positionKeyname = pMTConfig->GetValue("redis_position_key");
	locsrv.denoiseAlarmID = atoi(pMTConfig->GetValue("redis_denoise_alarm_id").c_str());
	locsrv.leapAlarmID = atoi(pMTConfig->GetValue("redis_leap_alarm_id").c_str());
	locsrv.indexAlarmID = atoi(pMTConfig->GetValue("redis_index_alarm_id").c_str());

	MTLogMgr::GetInstance().SetMaxLogLevel(locsrv.logLevel);
	locsrv.appLogger->Log(MTLogLevel::Trace, "Load configuration(%s) success.", INI_FILE_NAME);
	locsrv.appLogger->Log(MTLogLevel::Info, "Set logLevel to [%s].", locsrv.logLevel.c_str());
	return true;
}

static void logData(IMTLog* log, std::bitset<SN>& initialData)
{
	char strScreen[1024] = { 0 };
	for (int i = 0; i < SN; i++) {
		if (initialData[i] == 0) {
			strScreen[i] = '0';
		}
		else {
			strScreen[i] = '1';
		}
	}
	log->Log(MTLogLevel::Trace, "%s", strScreen);
}

int set_param(json& jobject)
{
	//try {
	//	locsrv.alarmParam.noiseWarnCountTh = stoi(jobject["noise_warn_th"].get<std::string>());
	//	locsrv.leaveNum = stoi(jobject["leave_num"].get<std::string>());
	//	locsrv.denoiseOneTh = stoi(jobject["denoise_one_th"].get<std::string>());
	//	locsrv.denoiseZeroTh = stoi(jobject["denoise_zero_th"].get<std::string>());
	//	locsrv.startIndex = stoi(jobject["start_index"].get<std::string>());
	//	locsrv.alarmParam.leapWarnCountTh = stoi(jobject["leap_warn_th"].get<std::string>());
	//	locsrv.alarmParam.leapTh = stoi(jobject["leap_th"].get<std::string>());
	//	locsrv.sleeptime = stoi(jobject["sleep_time"].get<std::string>());
	//	locsrv.sampleInterval = stoi(jobject["sample_interval"].get<std::string>());
	//} catch(...) {
	//	return -1;
	//}
	//return 0;
}

void UpdateParam()
{
	if (locsrv.updateParameter == 1) {
		// Reset param from redis key 88888
		std::string jsonstr;
		if (locsrv.mainCli.Get("88888", jsonstr) < 1) {
			printf("Get config key(88888) value failed.\n");
			locsrv.appLogger->Log(MTLogLevel::Fatal, "Get config key(88888) value failed.");
			exit(1);
		}
		json jobject;
		jobject = json::parse(jsonstr);
		if (set_param(jobject)){
			printf("Parse param json string failed.\n");
			locsrv.appLogger->Log(MTLogLevel::Fatal, "Parse param json string failed.");
			exit(0);
		}
		locsrv.updateParameter = 0;
	}
}

void SendInfo(std::vector<Pos>& pos, float speed)
{
	json info = json::array();
	char msg[1024] = {0};
	for (int i = 0; i < pos.size(); i++) {
		printf("%dth car, tail %d, head %d\n", pos[i].index, pos[i].tail, pos[i].head);
		locsrv.appLogger->Log(MTLogLevel::Trace, "%dth car, tail %d, head %d", pos[i].index, pos[i].tail, pos[i].head);
		json j;
		j["index"] = pos[i].index;
		j["head"] = pos[i].head;
		j["tail"] = pos[i].tail;
		j["timestamp"] = util_gettimestamp();
		info.push_back(j);
	}
	locsrv.appLogger->Log(MTLogLevel::Trace, "Train speed %.2f", speed);
	printf("Train speed %.2f\n", speed);
	locsrv.mainCli.Set(locsrv.positionKeyname.c_str(), info.dump().c_str());
	locsrv.mainCli.Set(locsrv.speedKeyname.c_str(), std::to_string(speed).c_str());
}

void LogFunc(const char* msg, int len)
{
	locsrv.appLogger->Log(MTLogLevel::Trace, "%s", msg);
}

void SendNoiseAlarm()
{
	json jarray = json::array();
	json j;
	j["pointId"] = locsrv.denoiseAlarmID;
	j["pointValue"] = 1;
	jarray.push_back(j);
	locsrv.mainCli.Publish(ALARM_CHANNEL, jarray.dump().c_str());
	locsrv.appLogger->Log(MTLogLevel::Fatal, "Send noise alarm.");
}

void SendIndexAlarm()
{
	json jarray = json::array();
	json j;
	j["pointId"] = locsrv.indexAlarmID;
	j["pointValue"] = 1;
	jarray.push_back(j);
	locsrv.mainCli.Publish(ALARM_CHANNEL, jarray.dump().c_str());
	locsrv.appLogger->Log(MTLogLevel::Fatal, "Send index alarm.");
}

void SendLeapAlarm()
{
	json jarray = json::array();
	json j;
	j["pointId"] = locsrv.leapAlarmID;
	j["pointValue"] = 1;
	jarray.push_back(j);
	locsrv.mainCli.Publish(ALARM_CHANNEL, jarray.dump().c_str());
	locsrv.appLogger->Log(MTLogLevel::Fatal, "Send leap alarm.");
}

void locationLoop()
{
	Location loc(locsrv.startIndex,locsrv.leapth, locsrv.oneth, locsrv.zeroth,locsrv.newth,locsrv.loadmin,locsrv.loadmax,locsrv.minCarLen);
	locRef = &loc;
	loc.RegCallback(LogFunc);
	int noise = 0;
	int misindex = 0;
	int leap = 0;

	while (true) {
		UpdateParam();
		std::bitset<SN> data;
		if (GetData(data)) {
			std::cout << "Get data failed." << std::endl;
			locsrv.appLogger->Log(MTLogLevel::Fatal, "Get data failed.continue.\n");
			continue;
		}
		enReturnType code;
		std::vector<Pos> pos;
		float speed = 0.f;
		//code = loc.GetTrainPos(data, pos, &speed);
		code = loc.GetTrainPos2(data, pos, &speed);
		if (code != TYPE_OK) {
			std::cout << rtmsg[code].rtmsg.c_str() << std::endl;
			locsrv.appLogger->Log(MTLogLevel::Fatal, "%s", rtmsg[code].rtmsg.c_str());
		}
		
		switch (code)
		{
		case TYPE_OK:
			SendInfo(pos, speed);
			noise = 0;
			misindex = 0;
			leap = 0;
			break;
		case TYPE_NO_CAR:
			pos.push_back(Pos(0,0,-1));
			SendInfo(pos, speed);
			break;
		case TYPE_ERR_NOISE:
			if (++noise > locsrv.noise_tolerance_) {
				SendNoiseAlarm();
				noise = 0;
			}
			break;
		default:
			break;
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(locsrv.sleeptime));		
	}
}

int main(int argc, const char **argv)
{
	print_version();
	if (argc == 2) {
		strcpy(locsrv.filename, argv[1]);
		locsrv.fd = fopen(locsrv.filename, "r");
		if (locsrv.fd == NULL) exit(1);	
	} 

	if (!Init()){
		exit(1);
	}
	
	locationLoop();
}