#pragma once

#define SCREEN_NUM 400
#define SCREEN_NUM_R 389
  
#define SCREEN1174 1174
#define SCREEN1573 (SCREEN1174+SCREEN_NUM-1)

#define SCREEN3174 3174

#define SPEED_KEYNAME "11001"
#define POSITION_KEYNAME "11013"

#define ALARM_CHANNEL "TLSAlarmChannel"


#define A_DENOISE_ALARM_ID 31001
#define A_LEAP_ALARM_ID 31002
#define A_INDEX_ALARM_ID 31003

#define ALARM_NONE 0U
#define ALARM_DENOISE 1U
#define ALARM_LEAP 2U
#define ALARM_INDEX 4U

#define CONFIG_DEFAULT_CAR_CONFIDENCE 20 /* new car is comming when the 40 points are 1*/
#define CONFIG_DEFAULT_GAP_CONFIDENCE 5 /* new gap is comming when the 5 points are 0*/
#define CONFIG_DEFAULT_SMOOTH_FRAME 7

#define INI_FILE_NAME "algo-conf"
#define LogPath "./log"