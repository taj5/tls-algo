#include <string.h>

#include <glog/logging.h>

#include "location.h"


static int findLastSemicolon(char ch, char* str)
{
	int offset = -1;
	int i = 0;
	while(i < strlen(str)) {
		if (str[i] == ch){
			offset = i;
		}
		i++;
	}
	return offset;
}

int ReadScreenData(std::bitset<SN>& data, FILE* fd)
{
	static uint64_t row = 0;
	int offset = 44, len = 0;
	char line[2048] = { 0 };
	char msg[1024] = { 0 };
	row++;

	printf("File row number = %lu\n", row);
	if (fgets(line, 2048, fd) == NULL) {
		LOG(ERROR) << "Read file end, app exit.";
		exit(1);
	}
	len = strlen(line);
	for (int i = 1; i<3; i++) {
		if (line[len-i] == '\r' || line[len-i] == '\n') {
			line[len - i] = 0;
		}
	}
	len = strlen(line);
	offset = findLastSemicolon(':', line);
	offset++;
	for (int i = offset; i < len; i = i + 1) {
		data.set((i - offset), (line[i] - 48));
		sprintf(msg + strlen(msg), "%d", (line[i] - 48));
	}
	return 1;
}

// <program name>.<hostname>.<user name>.log.<severity level>.<date>.<time>.<pid>
// INFO WARNING ERROR FATAL
int main(int argc, char** argv)
{
    FLAGS_log_dir = "./log";
    FLAGS_alsologtostderr = true; 
    google::InitGoogleLogging(argv[0]);

    CHECK_EQ(argc, 2) << ": The world must be ending!";

    FILE* fd;
    fd = fopen(argv[1], "r");
    std::vector<Pos> poslist;
    Location loc(-1);
    std::bitset<SN> data;
    ReadScreenData(data, fd);
    loc.GetTrainPos(data, poslist);
    for (int i=0; i<poslist.size(); i++){
        LOG(INFO) << "carnum" << poslist[i].index << "[" << 
        poslist[i].head << "," << poslist[i].tail << "]";
    }
    
    google::ShutdownGoogleLogging();
    return 0;
}