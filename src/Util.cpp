#define _CRT_SECURE_NO_WARNINGS
#include <stdint.h>
#include <stdio.h>
#include <cstdarg>


#ifdef _WIN32
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <winsock2.h>
//#include <windows.h>
#undef WIN32_LEAN_AND_MEAN
#endif
#include <limits.h>
#include <time.h>
#if defined( __MINGW32__) || defined(__linux__) || defined(__linux)
#include <sys/time.h>
#endif

//#include "Common/Util.h"
#include "Util.h"

#ifdef _WIN32
LARGE_INTEGER system_frequence = { 0 };
#endif

int util_gettimeofday(struct timeval *tv, struct timezone *tz)
{

#if defined( __MINGW32__) || defined (__linux__) || defined(__linux)
    gettimeofday(tv,tz);
    return 0;
#endif

#ifdef _MSC_VER
#define U64_LITERAL(n) n##ui64
#else
#define U64_LITERAL(n) n##llu
#endif

#ifdef _MSC_VER

	/* Conversion logic taken from Tor, which in turn took it
	* from Perl.  GetSystemTimeAsFileTime returns its value as
	* an unaligned (!) 64-bit value containing the number of
	* 100-nanosecond intervals since 1 January 1601 UTC. */
#define EPOCH_BIAS U64_LITERAL(116444736000000000)
#define UNITS_PER_SEC U64_LITERAL(10000000)
#define USEC_PER_SEC U64_LITERAL(1000000)
#define UNITS_PER_USEC U64_LITERAL(10)
	union {
		FILETIME ft_ft;
	uint64_t ft_64;
	} ft;

	if (tv == NULL)
		return -1;

	GetSystemTimeAsFileTime(&ft.ft_ft);

	if (UTIL_UNLIKELY(ft.ft_64 < EPOCH_BIAS)) {
		/* Time before the unix epoch. */
		return -1;
	}
	ft.ft_64 -= EPOCH_BIAS;
	tv->tv_sec = (long)(ft.ft_64 / UNITS_PER_SEC);
	tv->tv_usec = (long)((ft.ft_64 / UNITS_PER_USEC) % USEC_PER_SEC);
#endif
	return 0;
}

#define MAX_SECONDS_IN_MSEC_LONG \
	(((LONG_MAX) - 999) / 1000)

long util_tv_to_msec(const struct timeval *tv)
{
	if (tv->tv_usec > 1000000 || tv->tv_sec > MAX_SECONDS_IN_MSEC_LONG)
		return -1;

	return (tv->tv_sec * 1000) + ((tv->tv_usec + 999) / 1000);
}

int64_t util_gettimestamp(){
	struct timeval tv;
	if (util_gettimeofday(&tv, NULL) == 0){
		return ((int64_t)tv.tv_sec * 1000 ) + (tv.tv_usec + 999) / 1000 ;
	}
	return 0;
}

void util_gettimestampString(int64_t timestamp,char *date_string){
    if(date_string){
        time_t nTimeT =  timestamp / 1000;
        //int tv_usec = (timestamp % 1000) * 1000;

        struct tm *pTmp = localtime(&nTimeT);
        sprintf(date_string, "%d-%02d-%02d %02d:%02d:%02d",
            pTmp->tm_year + 1900, pTmp->tm_mon + 1,
                pTmp->tm_mday, pTmp->tm_hour,
                pTmp->tm_min, pTmp->tm_sec);
    }
}

void util_gettimestampWithMSString(int64_t timestamp,char *date_string){
    if(date_string){
        time_t nTimeT =  timestamp / 1000;
        int tv_msec = (timestamp % 1000);

        struct tm *pTmp = localtime(&nTimeT);
        sprintf(date_string, "%d-%02d-%02d %02d:%02d:%02d.%03d",
            pTmp->tm_year + 1900, pTmp->tm_mon + 1,
                pTmp->tm_mday, pTmp->tm_hour,
                pTmp->tm_min, pTmp->tm_sec,tv_msec);
    }
}

void util_gettimestampString2(int64_t timestamp,char *date_string){
    if(date_string){
        time_t nTimeT =  timestamp / 1000;
        //int tv_usec = (timestamp % 1000) * 1000;

        struct tm *pTmp = localtime(&nTimeT);
        sprintf(date_string, "%d%02d%02d%02d%02d%02d",
            pTmp->tm_year + 1900, pTmp->tm_mon + 1,
                pTmp->tm_mday, pTmp->tm_hour,
                pTmp->tm_min, pTmp->tm_sec);
    }
}

uint16_t util_flipbytes(uint16_t value)
{
	return (((value >> 8) & 0x00FF) | (value << 8) | 0xFF00);
}

uint32_t util_flipbytes(uint32_t value)
{
	uint16_t hi = uint16_t(value >> 16);
	uint16_t lo = uint16_t(value & 0xFFFF);

	return uint32_t(util_flipbytes(hi)) | uint32_t(util_flipbytes(lo)) << 16;
}

uint64_t util_flipbytes(uint64_t value)
{
	uint32_t hi = uint32_t(value >> 32);
	uint32_t lo = uint32_t(value & 0xFFFFFFFF);

	return uint64_t(util_flipbytes(hi)) | uint64_t(util_flipbytes(lo)) << 32;
}

void util_get_vertion(std::string &version)
{
#ifdef _WIN32
	version = "mt_util_V1.0.1_20190115beta";
#else
#endif // _WIN32

}

void sprintf_std_string(std::string & destination, const char * format, ...)
{
	va_list args0, args1;
	va_start(args0, format);
	va_copy(args1, args0);
	size_t num_of_chars = std::vsnprintf(nullptr, 0, format, args0);
	va_end(args0);
	destination.resize(num_of_chars + 1, '\0');
	std::vsnprintf(const_cast<char*>(destination.data()), destination.size(), format, args1);
	destination.resize(num_of_chars);
	va_end(args1);
}


//boost::pool<>g_Pool(sizeof(bool));
