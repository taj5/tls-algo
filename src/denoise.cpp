﻿#include "denoise.h"

void smoothData(std::list<std::bitset<SCREEN_NUM>>& datas, std::bitset<SCREEN_NUM>& data)
{
	std::vector<int> cnt(SCREEN_NUM, 0);
	std::list<std::bitset<SCREEN_NUM>>::iterator iter = datas.begin();
	while(iter != datas.end()) {
		for (int i=0;i<SCREEN_NUM; i++){
			cnt[i] = (*iter)[i] + cnt[i];
		}
		iter++;
	}
	int th = (datas.size() + 1) / 2;
	for (int j=0; j<SCREEN_NUM; j++){
		data[j] = cnt[j] >= th ? 1 : 0;
	}
}

void flipData(std::bitset<SCREEN_NUM>& data, std::vector<int> lens, int idx)
{
	int start = 0;
	for (int i = 0; i < idx; i++) {
		start += lens[i];
	}
	for (int i = start; i < (start+lens[idx]); i++) {
		data[i].flip();
	}
}

void getLens(std::bitset<SCREEN_NUM>& data, std::vector<int>& lens)
{
	if (!lens.empty()) {
		lens.clear();
	}
	bool tmp = data[0];
	int cnt = 1;
	for (int i = 1; i < SCREEN_NUM; i++) {
		if (data[i] == tmp) {
			cnt++;
		}
		else {
			lens.push_back(cnt);
			tmp = !tmp;
			cnt = 1;
		}
	}
	lens.push_back(cnt);
	return;
}

void ConvertToScreenData(std::bitset<SCREEN_NUM>& initialData, Screen& screenData)
{
	if (!screenData.lengthArray.empty()) {
		screenData.lengthArray.clear();
	}
	screenData.startValue = initialData[0];
	bool tmp = !!screenData.startValue;
	int cnt = 1;
	for (int i = 1; i < SCREEN_NUM; i++) {
		if (!initialData[i] == !tmp) {
			cnt++;
		}
		else {
			screenData.lengthArray.push_back(cnt);
			tmp = !tmp;
			cnt = 1;
		}
	}
	screenData.lengthArray.push_back(cnt);
	return;
}

void ConvertToScreenData_R(std::bitset<SCREEN_NUM_R>& initialData, Screen& screenData)
{
	if (!screenData.lengthArray.empty()) {
		screenData.lengthArray.clear();
	}
	screenData.startValue = initialData[0];
	bool tmp = !!screenData.startValue;
	int cnt = 1;
	for (int i = 1; i < SCREEN_NUM_R; i++) {
		if (!initialData[i] == !tmp) {
			cnt++;
		}
		else {
			screenData.lengthArray.push_back(cnt);
			tmp = !tmp;
			cnt = 1;
		}
	}
	screenData.lengthArray.push_back(cnt);
	return;
}

void denoiseOne(std::bitset<SCREEN_NUM>& data, int th)
{
	int iter, piter, niter;
	std::vector<int> lens;
	getLens(data, lens);
	iter = data[0]? 2:1;
	piter = iter - 1;
	niter = iter + 1;

	while (niter < lens.size()) {
		if (lens[iter] <= th) {
			int start = 0;
			for (int i = 0; i < iter; ++i) {
				start += lens[i];
			}
			for (int i = start; i < (start + lens[iter]); i++) {
				data[i].flip();
			}
		}
		iter = 2 + iter;
		piter = iter - 1;
		niter = iter + 1;
	}
}

void fixOcclussionPoint(std::bitset<SCREEN_NUM>& data, Screen& screen, int edge)
{
	int iter, piter, niter;
	if (screen.startValue == 1) {
		iter = 2;
	}
	else {
		iter = 1;
	}
	piter = iter - 1;
	niter = iter + 1;

	while (niter < screen.lengthArray.size()) {
		if (screen.lengthArray[iter] <= edge
			&& screen.lengthArray[piter] >= 1
			&& screen.lengthArray[niter] >= 1) {
			int start = 0;
			for (int i = 0; i < iter; ++i) {
				start += screen.lengthArray[i];
			}
			for (int i = start; i < (start + screen.lengthArray[iter]); i++) {
				data[i].flip();
			}
		}
		iter = 2 + iter;
		piter = iter - 1;
		niter = iter + 1;
	}
}

void denoiseZero(std::bitset<SCREEN_NUM>& data, int th)
{
	int iter, piter, niter;
	std::vector<int> lens;
	iter = data[0] ? 1 : 2;
	piter = iter - 1;
	niter = iter + 1;
	getLens(data, lens);

	while (niter < lens.size()) {
		if (lens[iter] <= th && lens[piter] > th && lens[niter] > th) {
			int start = 0;
			for (int i = 0; i < iter; ++i) {
				start += lens[i];
			}
			for (int i = start; i < (start+lens[iter]); i++) {
				data[i].flip();
			}
		}
		iter = 2 + iter;
		piter = iter - 1;
		niter = iter + 1;
	}
}

void fixUnocclussionPoint(std::bitset<SCREEN_NUM>& data, Screen& screen, int edge)
{
	int iter, piter, niter;
	if (screen.startValue == 1) {
		iter = 1;
	}
	else { // startValue == 0
		iter = 2;
	}
	piter = iter - 1;
	niter = iter + 1;

	while (niter < screen.lengthArray.size()) {
		if (screen.lengthArray[iter] <= edge
			&& screen.lengthArray[piter] > edge
			&& screen.lengthArray[niter] > edge) {
			int start = 0;
			for (int i = 0; i < iter; ++i) {
				start += screen.lengthArray[i];
			}
			for (int i = start; i < (start+screen.lengthArray[iter]); i++) {
				data[i].flip();
			}
		}
		iter = 2 + iter;
		piter = iter - 1;
		niter = iter + 1;
	}
}

bool fixSegment(std::bitset<SCREEN_NUM>& data, Screen& screen)
{
	int	fcnt = (screen.lengthArray.size() + 1 - screen.startValue) / 2;
	if (fcnt <= 1) {
		return true;
	} else if (fcnt == 2) { //0101  1010  010
		// if num of the middle 1 seg less than left right 0 seg num, flip this 1 seg
		int tcnt = screen.lengthArray.size() - fcnt;
		int middle = 1;
		if (screen.startValue == 1) {
			middle = 2;
		} else {
			middle = 1;
		}

		if (screen.lengthArray[middle] <  screen.lengthArray[middle-1] && 
			screen.lengthArray[middle] <  screen.lengthArray[middle+1]) {
			int start = 0;
			for (int i = 0; i < middle; i++) {
				start += screen.lengthArray[i];
			}

			for (int i = start; i < (start+screen.lengthArray[middle]); i++) {
				data[i].flip();
			}		
		}
	}
	else if (fcnt == 3) {  
		int tcnt = screen.lengthArray.size() - fcnt;
		if (tcnt == 2) { //01010 保留大1左右两侧的0 其余0去除
			int first = oneStartIndex(screen);
			int second = oneStartIndex(screen) + 2;
			int maxIndex = screen.lengthArray[first] > screen.lengthArray[second] ? first : second;
			int noiseIndex = 0;
			if (maxIndex == 1) {
				noiseIndex = 4;
			}
			else if (maxIndex == 3) {
				noiseIndex = 0;
			}
			else {
				return false;
			}
			int start = 0;
			for (int i = 0; i < noiseIndex; i++) {
				start += screen.lengthArray[i];
			}

			for (int i = start; i < (start + screen.lengthArray[noiseIndex]); i++) {
				data[i].flip();
			}
			return true;
		}
		else if (tcnt == 3) { // 101010 & 010101 去除小1
			int first = 0, second = 0, third = 0;
			if (screen.startValue == 1) { //101010
				first = 0;
				second = 2;
				third = 4;
			}
			else { // 010101
				first = 1;
				second = 3;
				third = 5;
			}
			int minIndex = screen.lengthArray[first] < screen.lengthArray[second] ? first : second;
			minIndex = screen.lengthArray[minIndex] < screen.lengthArray[third] ? minIndex : third;
			int start = 0;
			for (int i = 0; i < minIndex; i++) {
				start += screen.lengthArray[i];
			}

			for (int i = start; i < (start+screen.lengthArray[minIndex]); i++) {
				data[i].flip();
			}
			return true;
		}
	}
	return false;
}

void denoiseThreeSegmentOfZero(std::bitset<SCREEN_NUM>& data)
{
	std::vector<int> lens;
	int zcnt = 0;
	getLens(data, lens);
	zcnt = (lens.size() + 1 - data[0]) / 2;
	if (zcnt != 3 || zcnt != 2) {
		return;
	}
	int ocnt = lens.size() - zcnt;
	int first = 0, second = 0, third = 0;
	int noiseIndex = 0, maxIndex = 0, minIndex = 0;
	int start = 0;

	/*
	 * flip 0  which is at side of little one
	 * 01010 => 0101/1010
	 */
	if (ocnt == 2 && zcnt == 3) { // 
		first = 1, second = 3;
		maxIndex = lens[first] > lens[second] ? first : second;
		minIndex = lens[first] < lens[second] ? first : second;
		if (lens[minIndex-1] >= lens[minIndex] && lens[minIndex+1] >= lens[minIndex]) {
			noiseIndex = minIndex;		
		} else {
			noiseIndex = (maxIndex == 1)?4:0;
		}	
	
		for (int i = 0; i < noiseIndex; i++) {
			start += lens[i];
		}

		for (int i = start; i < (start + lens[noiseIndex]); i++) {
			data[i].flip();
		}
		return;
	} else if (ocnt == 3 || ocnt == 4) {
		/* 10101 => if two side one large than midlle one flip less zero */
		if (ocnt == 3 && zcnt == 2 && lens[0] > lens[2] && lens[4] > lens[2]) {
			noiseIndex = lens[1] > lens[3] ? 3 : 1;
			for (int i = 0; i < noiseIndex; i++) {
				start += lens[i];
			}

			for (int i = start; i < (start + lens[noiseIndex]); i++) {
				data[i].flip();
			}
			return;
		}

		/* 
		 * ocnt==3 && zcnt==3
		 * ocnt==4 && zcnt==3
		 * flip little 0
		 * 1010101 => 01010/01010/01010
		 * 101010 => 01010/1010   
		 * 010101 => 0101/01010
		 */
		first = data[0];
		second = data[0] + 2;
		third = data[0] + 4;

		minIndex = lens[first] < lens[second] ? first : second;
		minIndex = lens[minIndex] < lens[third] ? minIndex : third;
		for (int i = 0; i < minIndex; i++) {
			start += lens[i];
		}

		for (int i = start; i < (start+lens[minIndex]); i++) {
			data[i].flip();
		}

		if (minIndex == 0 || minIndex == 5) {
			denoiseThreeSegmentOfZero(data);
		}
	}
	return;
}


void denoiseFinal(std::bitset<SCREEN_NUM>& data)
{
	std::vector<int> lens;
	int zcnt = 0;
	getLens(data, lens);
	zcnt = (lens.size() + 1 - data[0]) / 2;
	int ocnt = lens.size() - zcnt;
	int first = 0, second = 0, third = 0;
	int noiseIndex = 0, maxIndex = 0, minIndex = 0;
	int start = 0;

	// 10101 
	if (zcnt == 2 && ocnt == 3 && lens[0] > lens[2] && lens[4] > lens[2]) {
		noiseIndex = lens[1] > lens[3] ? 3 : 1;
		for (int i = 0; i < noiseIndex; i++) {
			start += lens[i];
		}

		for (int i = start; i < (start + lens[noiseIndex]); i++) {
			data[i].flip();
		}
		return;
	}


	// 01010 
	if (zcnt == 3 && ocnt == 2) { 
		first = 1, second = 3;
		maxIndex = lens[1] > lens[3] ? 1 : 3;
		minIndex = lens[1] < lens[3] ? 1 : 3;
		noiseIndex = (maxIndex == 1)? 4:0;
		for (int i = 0; i < noiseIndex; i++) {
			start += lens[i];
		}

		for (int i = start; i < (start + lens[noiseIndex]); i++) {
			data[i].flip();
		}
		return;
	} 

	/* 
	 * 010101 =>10101 0101
	 * 101010 => 1010 1010 10101  
	 * 1010101 => 10101 10101 10101
	 */
	if (zcnt == 3 && (ocnt == 3 || ocnt == 4)) {
		first = data[0];
		second = data[0] + 2;
		third = data[0] + 4;

		// 找出长度最小的0段索引
		minIndex = lens[first] < lens[second] ? first : second;
		minIndex = lens[minIndex] < lens[third] ? minIndex : third;

		for (int i = 0; i < minIndex; i++) {
			start += lens[i];
		}

		for (int i = start; i < (start+lens[minIndex]); i++) {
			data[i].flip();
		}

		// 
		//if (minIndex == 0 || minIndex == 5) {
		//	denoiseFinal(data);
		//}
	}
	return;
}

void denoiseTwo(std::bitset<SCREEN_NUM>& data)
{
	std::vector<int> lens;
	int zcnt = 0 , ocnt = 0, middle = 0;
	getLens(data, lens);
	zcnt = (lens.size() + 1 - data[0]) / 2;
	if (zcnt != 2) {
		return;
	}
	/* 
	 * 010   => 0
	 * 0101  => 01
	 * 1010  => 10
	 * 10101 => 101
	 */ 
	ocnt = lens.size() - zcnt;
	middle = 1;
	if (data[0]) {
		middle = 2;
	}

	if (lens[middle] <  lens[middle-1] && lens[middle] <  lens[middle+1]) {
		int start = 0;
		for (int i = 0; i < middle; i++) {
			start += lens[i];
		}

		for (int i = start; i < (start+lens[middle]); i++) {
			data[i].flip();
		}		
	}
}

/*
 * flip the middle little one
 */
void denoiseMiddleOne(std::bitset<SCREEN_NUM>& data)
{
	std::vector<int> lens;
	getLens(data, lens);
	for (int i = (1+data[0]); i< (lens.size()-1); i=i+2) {
		if (lens[i] <=  lens[i-1] && lens[i] <=  lens[i+1]) {
			flipData(data, lens, i);		
		}
	}
}


/**
 * In condition current frame rightmost is 1 and length is smaller than 6
 * if the prev frame rightmost is 0 
 * flip the rightmost value 
 * This function need be called twice. Optimize later.
 */
bool fixSegmentExtend(std::bitset<SCREEN_NUM>& data, Screen& screen)
{
	int	fcnt = (screen.lengthArray.size() + 1 - screen.startValue) / 2;
	if (fcnt <= 1) {
		return true;
	} else if (fcnt == 2) { //0101  1010  010 10101
		// if num of the middle 1 seg less than left right 0 seg num, flip this 1 seg
		int tcnt = screen.lengthArray.size() - fcnt;
		int middle = 1;
		if (screen.startValue == 1) {
			middle = 2;
		} else {
			middle = 1;
		}

		if (screen.lengthArray[middle] <  screen.lengthArray[middle-1] && 
			screen.lengthArray[middle] <  screen.lengthArray[middle+1]) {
			int start = 0;
			for (int i = 0; i < middle; i++) {
				start += screen.lengthArray[i];
			}

			for (int i = start; i < (start+screen.lengthArray[middle]); i++) {
				data[i].flip();
			}		
		}
	} else if (fcnt == 3) {  
		int tcnt = screen.lengthArray.size() - fcnt;
		if (tcnt == 2) { // 01010 
			// [Add] if little 1 number less than left and right side 0 number
			// flip the little 1
			// else 保留大1左右两侧的0 其余0去除

			int noiseIndex = 0;
			int first = oneStartIndex(screen);
			int second = oneStartIndex(screen) + 2;
			int maxIndex = screen.lengthArray[first] > screen.lengthArray[second] ? first : second;
			int minIndex = screen.lengthArray[first] < screen.lengthArray[second] ? first : second;
			if (screen.lengthArray[minIndex-1] >= screen.lengthArray[minIndex] &&
				screen.lengthArray[minIndex+1] >= screen.lengthArray[minIndex]) {
				noiseIndex = minIndex;		
			} else {
				if (maxIndex == 1) {
					noiseIndex = 4;
				}
				else if (maxIndex == 3) {
					noiseIndex = 0;
				}
				else {
					return false;
				}
			}
			
			int start = 0;
			for (int i = 0; i < noiseIndex; i++) {
				start += screen.lengthArray[i];
			}

			for (int i = start; i < (start + screen.lengthArray[noiseIndex]); i++) {
				data[i].flip();
			}
			return true;
		}
		else if (tcnt == 3) { // 101010 & 010101 去除小1
			int first = 0, second = 0, third = 0;
			if (screen.startValue == 1) { //101010
				first = 0;
				second = 2;
				third = 4;
			}
			else { // 010101
				first = 1;
				second = 3;
				third = 5;
			}
			int minIndex = screen.lengthArray[first] < screen.lengthArray[second] ? first : second;
			minIndex = screen.lengthArray[minIndex] < screen.lengthArray[third] ? minIndex : third;
			int start = 0;
			for (int i = 0; i < minIndex; i++) {
				start += screen.lengthArray[i];
			}

			for (int i = start; i < (start+screen.lengthArray[minIndex]); i++) {
				data[i].flip();
			}
			return true;
		}
	} else {
		return false;
	}
	return true;
}

int zeroCount(Screen& screen)
{
	if (screen.lengthArray.size() % 2 == 0) {
		return screen.lengthArray.size() / 2;
	}
	else {
		if (screen.startValue == 1) {
			return screen.lengthArray.size() / 2;
		}
		else {
			return (screen.lengthArray.size() + 1) / 2;
		}
	}
}

int oneCount(Screen& screen)
{
	if (screen.lengthArray.size() % 2 == 0) {
		return screen.lengthArray.size() / 2;
	}
	else {
		if (screen.startValue == 0) {
			return screen.lengthArray.size() / 2;
		}
		else {
			return (screen.lengthArray.size() + 1) / 2;
		}
	}
}

int zeroStartIndex(Screen& screen)
{
	return screen.startValue;
}

int oneStartIndex(Screen& screen)
{
	return (1 - screen.startValue);
}

int lastValueOfScreen(Screen& screen)
{
	if (screen.startValue == 1 && screen.lengthArray.size() % 2 == 0) {
		return 0;
	}
	else if (screen.startValue == 1 && screen.lengthArray.size() % 2 == 1) {
		return 1;
	} 
	else if (screen.startValue == 0 && screen.lengthArray.size() % 2 == 0) {
		return 1;
	}
	else if (screen.startValue == 0 && screen.lengthArray.size() % 2 == 1) {
		return 0;
	}
	printf("lastValueOfScreen error.\n");
	return -1;
}
//#define TEST

#ifdef TEST
int main()
{
	std::bitset<SCREEN_NUM> data;
	for (int i = 1; i < SCREEN_NUM; i = i + 2) {
		data[i].flip();
	}
	// 010101=>001011
	Screen screen1, screen2;
	ToScreenData(data, screen1);
	ToScreenData(data, screen2);
	printf("before:\n");
}

#endif