#include "monitor.h"
#include"controller.h"
#include "json.hpp"
#include <time.h>

using json = nlohmann::json;
Monitor::Monitor() : m_posBias(2), m_handleIdx(0) 
{
}

Monitor::~Monitor() {};

bool Monitor::Init(std::string& redisCache)
{
	return m_state.Init(redisCache);
}

void Monitor::Register(int pos, IAction* action)
{
	Handler handler{ pos, action };
	m_handlers.push_back(handler);
}

void Monitor::UnRegister()
{
	m_handlers.clear();
}

bool Monitor::UpdateState(const int loadIndex)
{
	if (!m_state.UpdateCurrentTrainPos(loadIndex)
		|| !m_state.UpdateCurrentLCHeight()
		|| !m_state.UpdateCurrentWeight()
		|| !m_state.UpdateCurrentCommand()) {
		return false;
	}
	return true;
}

#define run_with_period(_cnt_) if (cronloops%(_cnt_))

#include <algorithm>
void Monitor::Observe(int loadIndex, bool flag)
{	time_t t0 = time(NULL);
	if (!UpdateState(loadIndex)) {
		serverLog(ON, MTLogLevel::Fatal, "Update state fail(%d).", loadIndex);
	}
	/* 
	 * flag equals 1 means train is stopping at some point for loading
	 * Update the stop postion.
	 */
	if (flag) {
		/* When cnc runs here,the train already stop OK, green mark shows on the web page */
		JudgeTrainStopPoint();
	}
	while (true) {
		if (!UpdateState(loadIndex)) {
			serverLog(ON, MTLogLevel::Fatal, "Update state fail(%d).", loadIndex);
		}

		/* Only respond stop command when loading. */
		if (m_state.m_cmd == 0) {
			serverLog(ON, MTLogLevel::Info, "Stop at %dth car because state change to %d(0:stop,1:pause,2:run)", loadIndex, m_state.m_cmd);
			server.mainCli.Set(Load_Done_Index_keyname, std::to_string(server.startLoadIndex-1).c_str());
			server.mainCli.Set(LoadingIndex_Keyname, std::to_string(server.startLoadIndex-1).c_str());
			server.prevState = (RunningState)m_state.m_cmd;
			break;
		}

		if (InPosition(m_handleIdx, flag)) {

			/* When car head arrive at point 6, compute the standand weight
			 * Add by wuhao 2019/12/17
			 */
			int at = m_handlers.size() - m_handleIdx;
			if ((at == 2) || (at == 3)) {
				int pt4Index;
				if (m_handlers.size() == 7) {
					pt4Index = 2;
				} else {
					pt4Index = 3;
				}
				m_state.UpdateCurrentLCHeight();
				float tmp = (server.model.m_initmodel[pt4Index].dis - m_state.m_height)*server.weightK;
				server.model.m_rtmodel[m_handleIdx].weight += tmp;  
				serverLog(ON, MTLogLevel::Info, "Arrive %d point. k=%.2f rt-h=%.2f 4pt-h=%d, prev->[%.2f], now->[%.2f]",\
				m_handleIdx, server.weightK , m_state.m_height, server.model.m_initmodel[pt4Index].dis, \
				server.model.m_initmodel[m_handleIdx].weight, \
				server.model.m_rtmodel[m_handleIdx].weight);
			}
			
			/* Trigger speed calc logical if the car head at position 0 */
			if (m_handleIdx == 1 && m_handlers.size() == 8) {
				float ave = std::accumulate(server.speed12.begin(), server.speed12.end(), 0.0) / server.speed12.size();
				if (ave > server.optimalSpeed) {
					float L2 = server.model.m_rtmodel[m_handleIdx].dis;
					float L3 = server.model.m_rtmodel[m_handleIdx+1].dis;
					float L4 = server.model.m_rtmodel[m_handleIdx+2].dis;
					float adjust3 = server.speedAlgorithmK*((ave - server.optimalSpeed)*1000/3.6) * (L2-L3) / server.LCSpeed;
					float adjust4 = server.speedAlgorithmK*((ave - server.optimalSpeed)*1000/3.6) * (L3-L4) / server.LCSpeed;
					//m_handlers[2].pos = m_handlers[2].pos - adjust3;
					//if (m_handlers[2].pos < (m_handlers[1].pos+40)) {
					//	m_handlers[2].pos = m_handlers[1].pos+40;
					//}
					//m_handlers[3].pos = m_handlers[3].pos - adjust4;
					//if (m_handlers[3].pos < (m_handlers[2].pos+40)) {
					//	m_handlers[3].pos = m_handlers[2].pos+40;
					//}
					serverLog(ON, MTLogLevel::Trace, "ave-speed = [%.2f] opt-speed = [%.2f]  adjust3  = [%.2f]  adjust4 = [%.2f]  speedAlgorithmK = [%.2f]"
					, ave, server.optimalSpeed,adjust3, adjust4,server.speedAlgorithmK);
				} else {
					serverLog(ON, MTLogLevel::Trace, "No adjust.ave-speed = [%.2f] opt-speed = [%.2f]", ave, server.optimalSpeed);
				}
				server.speed12.clear();
			}
			if (m_handleIdx == (m_handlers.size() - 1)) {
				serverLog(ON, MTLogLevel::Trace, "Tail OK!(%d,%.2f,%d).", loadIndex, m_state.m_tailPos, m_handlers[m_handleIdx].pos);
			}
			else {
				serverLog(ON, MTLogLevel::Trace, "Head OK!(%d,%.2f,%d).", loadIndex, m_state.m_headPos, m_handlers[m_handleIdx].pos);
			}

			if (!m_handlers[m_handleIdx].action->Action(m_state, m_handleIdx, m_handlers.size())) {
				break;
			}
			m_handleIdx++;
			if (m_handleIdx >= (int)m_handlers.size()) {
				m_handleIdx = 0;
				serverLog(ON, MTLogLevel::Info, "%dth load finish.", loadIndex);
				break;
			}
		}
		else {
			/* Store the speed when head in range [0,1]*/
			if ((m_handleIdx == 0 || m_handleIdx == 1) && m_handlers.size() == 8) {
				std::string tmp;
				server.mainCli.Get(TrainSpeed_Keyname, tmp);
				server.speed12.push_back(stof(tmp));
			}

			time_t t = time(NULL);
			if ((t - t0) > 5) {
				t0 = t; 
				if (m_handleIdx == (m_handlers.size() - 1)) {
					serverLog(ON, MTLogLevel::Trace, "Tail Wait! [%d,%.2f,%d,(%d-%d)]", loadIndex, m_state.m_tailPos, m_handlers[m_handleIdx].pos, (m_handleIdx + 1), m_handlers.size());
				}
				else {
					serverLog(ON, MTLogLevel::Trace, "Head Wait! [%d,%.2f,%d,(%d-%d)]", loadIndex, m_state.m_headPos, m_handlers[m_handleIdx].pos, (m_handleIdx + 1), m_handlers.size());
				}	
			}
		}
	}
}

void Monitor::SetPosBias(int error)
{
	m_posBias = error;
}

void Monitor::JudgeTrainStopPoint()
{
	if (m_handlers.size() < 7) {
		server.stopPoint = -1;
	}
	
	int c23 = (m_handlers[0].pos +  m_handlers[1].pos) / 2;
	int c34 = (m_handlers[1].pos +  m_handlers[2].pos) / 2;
	if ((int)m_state.m_headPos > m_handlers[0].pos && (int)m_state.m_headPos < c23) {
		server.stopPoint = 1;
		serverLog(ON, MTLogLevel::Trace, "The train(head=%.2f) have stopped in (p2,c23)",m_state.m_headPos);
		return;
	} else if ((int)m_state.m_headPos >= c23 && (int)m_state.m_headPos < c34) {
		server.stopPoint = 2;
		serverLog(ON, MTLogLevel::Trace, "The train(head=%.2f) have stopped in [c23,c34)",m_state.m_headPos);
		return;
	} else if ((int)m_state.m_headPos >= c34 && (int)m_state.m_headPos < m_handlers[3].pos) {
		server.stopPoint = 3;
		serverLog(ON, MTLogLevel::Trace, "The train(head=%.2f) have stopped in [c34,p5)",m_state.m_headPos);
		return;
	}
	
	server.stopPoint = -1;
	return;
}

/* 
 * flag 1 use the single side threshold otherwise 0 
 * flag 1 means pause->run or stop->run
 */
bool Monitor::InPosition(int index, bool flag)
{
	if (index == (m_handlers.size() - 1)) {
		if (flag) {
			if ((int)m_state.m_tailPos >= m_handlers[index].pos) {
				return true;
			}
		} else if ((int)m_state.m_tailPos >= (m_handlers[index].pos - server.posMin) && (int)m_state.m_tailPos <= (m_handlers[index].pos + server.posMax)) {
			return true;
		}	
	} else {
		if (flag) {
			if ((int)m_state.m_headPos >= m_handlers[index].pos) {
				return true;
			}
		} else if ((int)m_state.m_headPos >= (m_handlers[index].pos - server.posMin) && (int)m_state.m_headPos <= (m_handlers[index].pos + server.posMax)) {
			return true;
		}
	}
	//printf("flag = %d, h=%.2f h0=%d.\n", flag, m_state.m_headPos, m_handlers[index].pos);
	return false;
}

void Monitor::Reset()
{
	m_handleIdx = 0;
}

TLSState::TLSState():m_headPos(-1),m_height(-1),m_weight(-1),m_carIndex(-1)
{
}

bool TLSState::Init(std::string& redisCache)
{
	if (!m_redisCli.Open(redisCache)) {
		serverLog(OFF, MTLogLevel::Fatal, "Connect redis@%s failed.", redisCache.c_str());
		return false;
	}
	return true;
}

bool TLSState::UpdateCurrentLCHeight()
{
	return redisGetValueFloat(LCHeight_Keyname, m_height);
}

bool TLSState::UpdateCurrentTrainPos(const int loadIndex)
{
	return redisGetValueInt(TrainPos_Keyname, loadIndex, m_headPos, m_tailPos);
}

bool TLSState::UpdateCurrentWeight()
{
	return redisGetValueFloat(Cabin_Weight_Keyname, m_weight);
}

bool TLSState::UpdateCurrentCommand()
{
	return redisGetValueInt(Command_Keyname, m_cmd);
}

bool TLSState::redisGetValueInt(std::string keyname, int& value)
{
	std::string strPos;
	int ret = 0;
	ret = m_redisCli.Get(keyname.c_str(), strPos);
	if (ret == -1) {
		if (m_redisCli.ReConnect()) {
			m_redisCli.Get(keyname.c_str(), strPos);
		}
		else {
			serverLog(OFF, MTLogLevel::Error, "Get %s value failed.", keyname.c_str());
			return false;
		}

	}
	else if (ret == 0) {
		serverLog(OFF, MTLogLevel::Error, "keyname %s is not existed.", keyname.c_str());
		return false;
	}
	value = atoi(strPos.c_str());
	return true;
}

bool TLSState::redisGetValueInt(std::string keyname, int loadindex, float& headpos, float& tailpos)
{
	std::string strPos;
	int ret = 0;
	ret = m_redisCli.Get(keyname.c_str(), strPos);
	if (ret == -1) {
		if (m_redisCli.ReConnect()) {
			m_redisCli.Get(keyname.c_str(), strPos);
		}
		else {
			serverLog(OFF, MTLogLevel::Error, "Get %s value failed.", keyname.c_str());
			return false;
		}
	} 
	else if (ret == 0) {
		serverLog(OFF, MTLogLevel::Error, "keyname %s is not existed.", keyname.c_str());
		return false;
	}
	json j = json::parse(strPos);
	if (j.is_array()) {
		for (json::const_iterator iter_child = j.begin(); iter_child != j.end(); ++iter_child)
		{
			if (iter_child->at("index").get<int>() == loadindex) {
				headpos = iter_child->at("head").get<float>();
				tailpos = iter_child->at("tail").get<float>();
				return true;
			}
		}
	}
	tailpos = 0;
	headpos = 0;
	return true;
}

bool TLSState::redisGetValueFloat(std::string keyname, float& value)
{
	std::string strPos;
	int ret = 0;
	ret = m_redisCli.Get(keyname.c_str(), strPos);
	if (ret == -1) {
		if (m_redisCli.ReConnect()) {
			m_redisCli.Get(keyname.c_str(), strPos);
		}
		else {
			serverLog(OFF ,MTLogLevel::Error, "Get %s value failed.", keyname.c_str());
			return false;
		}
	}
	else if (ret == 0) {
		serverLog(OFF, MTLogLevel::Error, "keyname %s is not existed.", keyname.c_str());
		return false;
	}
	value = atof(strPos.c_str());
	return true;
}