#ifndef LOCATION_DENOISE_HEADER
#define LOCATION_DENOISE_HEADER

#include <bitset>
#include <vector>
#include <list>
#include "location.h"

struct Position {
	int index;
	float tail;
	float head;
};

struct Screen {
	int startValue;
	std::vector<int> lengthArray; 
	std::vector<Position> position;
	float speed;
};


int zeroCount(Screen& screen);

int oneCount(Screen& screen);

int zeroStartIndex(Screen& screen);

int oneStartIndex(Screen& screen);

int lastValueOfScreen(Screen& screen);

void fixOcclussionPoint(std::bitset<SCREEN_NUM>& data, Screen& screen, int edge);

void fixUnocclussionPoint(std::bitset<SCREEN_NUM>& data, Screen& screen, int edge);

bool fixSegment(std::bitset<SCREEN_NUM>& data, Screen& screen);

bool fixSegmentExtend(std::bitset<SCREEN_NUM>& data, Screen& screen);

void ConvertToScreenData(std::bitset<SCREEN_NUM>& initialData, Screen& screenData);

void ConvertToScreenData_R(std::bitset<SCREEN_NUM_R>& initialData, Screen& screenData);

//=========================================================================================

void smoothData(std::list<std::bitset<SCREEN_NUM>>& datas, std::bitset<SCREEN_NUM>& data);

void flipData(std::bitset<SCREEN_NUM>& data, std::vector<int> lens, int idx);

void getLens(std::bitset<SCREEN_NUM>& data, std::vector<int>& lens);


void denoiseOne(std::bitset<SCREEN_NUM>& data, int th);

void denoiseZero(std::bitset<SCREEN_NUM>& data, int th);

void denoiseThreeSegmentOfZero(std::bitset<SCREEN_NUM>& data);

void denoiseMiddleOne(std::bitset<SCREEN_NUM>& data);

void denoiseFinal(std::bitset<SCREEN_NUM>& data);

//void denoiseTwo(std::bitset<SCREEN_NUM>& data);

#endif // !LOCATION_DENOISE_HEADER


