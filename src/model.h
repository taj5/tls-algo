#ifndef MODEL_HEADER
#define MODEL_HEADER

#include <queue>
#include <Net/MTRedisClient.h>
using MT::Util::Net::MTRedisClient;

struct Param{
	int pos;	// λ��
	float weight; // ����������
	int dis;  // Ŀ��߶�
};

class ControlModel {
public:
	ControlModel();
	int LoadModel();

	bool LoadModelMock(int indexModel);

	bool LoadTrainModelFromRedis(std::string name, MTRedisClient& cli, int& total);
	bool LoadModelFromRedis(int loadIndex, MTRedisClient& cli, bool isRecovery);
	bool LoadModelAndParamFromRedis(int loadIndex, MTRedisClient& cli, bool isRecovery);
	bool LoadInitModel(int loadIndex, MTRedisClient& cli, bool isRecovery);
	void PrintModelNameList();
	
	std::map<int, float> m_simpleMap;
	std::vector<Param> m_initmodel; /* Store the init model */
	std::vector<Param> m_rtmodel; /* Store the realtime model*/
	std::vector<std::string> m_trainModel;
	std::map<int, std::pair<float, float>> m_triple;
};
#endif // MODEL_HEADER

