#ifndef MONITOR_HEADER
#define	MONITOR_HEADER
#include "Net/MTRedisClient.h"
#include <set>
using MT::Util::Net::MTRedisClient;
using std::set;

struct TrainPos {
	int index;
	float head;
	float tail;
};

struct TLSState {
public:
	TLSState();
	float m_headPos;
	float m_tailPos;
	int m_carIndex;
	float m_height;
	float m_weight;
	int m_cmd;

public:
	bool Init(std::string& redisCache);

	bool UpdateCurrentLCHeight();
	bool UpdateCurrentTrainPos(const int loadIndex);
	bool UpdateCurrentWeight();
	bool UpdateCurrentCommand();

private:
	bool redisGetValueInt(std::string keyname, int& value);
	bool redisGetValueInt(std::string keyname, int loadIndex, float& headPos, float& tailPos);
	bool redisGetValueFloat(std::string keyname, float& value);

private:
	MTRedisClient m_redisCli;
};

class Monitor;

class IAction {
public:
	virtual bool Action(TLSState& state, int seq, int total) = 0;
};

class Monitor {
public:
	Monitor();
	~Monitor();
public:
	struct Handler {
		int pos;
		IAction* action;
	};

	// ��ʼ��
	bool Init(std::string& redisCache);

	// ע��
	void Register(int pos, IAction* action);

	void UnRegister();

	void Observe(int loadIndex, bool flag);

	bool UpdateState(const int loadIndex);

	void SetPosBias(int error);

	void JudgeTrainStopPoint();
	
	bool InPosition(int index, bool flag);

	void Reset();
	
	TLSState m_state;
	int m_posBias;// ʵʱλ�����
private:
	std::vector<Handler> m_handlers;
	int m_handleIdx; // handlers����������

};

#endif // ! MONITOR_HEADER

