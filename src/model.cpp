#include <map>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <stdexcept>
#ifdef LOAD_FROM_MYSQL
	#include <mysql/jdbc.h>
#endif
#include "model.h"
#include "controller.h"
#include "json.hpp"

ControlModel::ControlModel() {
}

#ifdef LOAD_FROM_MYSQL
int ControlModel::LoadModel()
{
	try {
		sql::mysql::MySQL_Driver *driver;
		sql::Connection *con;
		sql::Statement *stmt;
		sql::ResultSet  *res;

		driver = sql::mysql::get_mysql_driver_instance();
		con = driver->connect(TLSDATA_HOST, TLSDATA_USER, TLSDATA_PASS);
		stmt = con->createStatement();
		stmt->execute("USE " TLSDATA_DB);

		res = stmt->executeQuery("SELECT * FROM simplePhase");
		while (res->next()) {
			m_simpleMap.insert(std::make_pair(res->getInt(1), res->getDouble(2)));
		}

		res = stmt->executeQuery("SELECT * FROM stage2");
		while (res->next()) {
			m_triple.insert(std::make_pair(res->getInt(1),
				std::make_pair(res->getDouble(2), res->getDouble(3))));
		}

		delete stmt;
		delete con;
		return EXIT_SUCCESS;
	}
	catch (sql::SQLException &e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
		return EXIT_FAILURE;
	}
}
#endif

bool ControlModel::LoadModelMock(int indexModel)
{
	m_rtmodel.clear();
	m_rtmodel.push_back({ 100, 0, 2000 });
	m_rtmodel.push_back({ 130, 0, 3500 });//down
	m_rtmodel.push_back({ 150, 0, 3000 });//up
	m_rtmodel.push_back({ 200, 0, 2500 });//down
	m_rtmodel.push_back({ 240, 40, 0 });//30  //down
	m_rtmodel.push_back({ 270, 30, 0 });// //no move
	m_rtmodel.push_back({ 300, 20, 0 });// up
	m_rtmodel.push_back({ 50, 0, 2200 });// up
	return true;
}

using json = nlohmann::json;

bool ControlModel::LoadTrainModelFromRedis(std::string name, MTRedisClient& cli, int& total)
{
	std::string strValue;
	int reads = cli.Get(name.c_str(), strValue);
	if (reads <= 0) {
		return false;
	}

	json j = json::parse(strValue);
	
	for (auto& element : j) {
		m_trainModel.push_back(element);

	}
	total = j.size();
	return true;
}

void ControlModel::PrintModelNameList()
{
	std::string dumpstr;
	for (int i=0; i<m_trainModel.size(); ++i) {
		dumpstr.append(m_trainModel[i]);
		dumpstr.append(" ");
	}
	serverLog(OFF, MTLogLevel::Trace,"model:%s", dumpstr.c_str());
}

bool ControlModel::LoadModelFromRedis(int loadIndex, MTRedisClient& cli, bool isRecovery)
{
	m_rtmodel.clear();
	std::string strModel;
	if (m_trainModel.size() < loadIndex) {
		return false;
	}
	int reads = cli.Get(m_trainModel[loadIndex].c_str(), strModel);
	if (reads <= 0) {
		return false;
	}
	int cnt = 0;
	json j = json::parse(strModel);
	if (j.is_array()) {
		for (json::const_iterator iter_child = j.begin()
			; iter_child != j.end()
			; ++iter_child)
		{
			cnt++;
			Param param;
			param.pos = iter_child->at("position").get<int>();
			param.weight = iter_child->at("weight").get<float>();
			param.dis = iter_child->at("height").get<int>();
			if (cnt == 1 && isRecovery) {
				continue;
			}
			m_rtmodel.push_back(param);
		}
		return true;
	}
	return false;
}

bool ControlModel::LoadModelAndParamFromRedis(int loadIndex, MTRedisClient& cli, bool isRecovery)
{
	m_rtmodel.clear();
	std::string strModel;
	if (m_trainModel.size() < loadIndex) {
		return false;
	}
	if (cli.Get(m_trainModel[loadIndex].c_str(), strModel) <= 0) {
		return false;
	}
	serverLog(ON, MTLogLevel::Trace, "model is %s \n", strModel.c_str());
	int cnt = 0;
	json jj = json::parse(strModel);
	server.weightBias[0] = jj["weightBias5"].get<float>();
	server.weightBias[1] = jj["weightBias6"].get<float>();
	server.weightBias[2] = jj["weightBias7"].get<float>();
	server.posMin = jj["posDownLimit"].get<int>();
	server.posMax = jj["posUpLimit"].get<int>();
	server.weightExcess = jj["lastWeightTh"].get<float>();

	json j = jj["trainModelList"];
	if (j.is_array()) {
		for (json::const_iterator iter_child = j.begin()
			; iter_child != j.end()
			; ++iter_child)
		{
			cnt++;
			Param param;
			param.pos = iter_child->at("position").get<int>();
			param.weight = iter_child->at("weight").get<float>();
			param.dis = iter_child->at("height").get<int>();
			if (cnt == 1 && isRecovery) {
				continue;
			}
			m_rtmodel.push_back(param);
		}
		return true;
	}
	return false;
}

bool ControlModel::LoadInitModel(int loadIndex, MTRedisClient& cli, bool isRecovery)
{
	m_initmodel.clear();
	std::string strModel;
	if (m_trainModel.size() < loadIndex) {
		return false;
	}
	std::string initmodelstr = m_trainModel[loadIndex];
	initmodelstr.append("-init");
	if (cli.Get(initmodelstr.c_str(), strModel) <= 0) {
		return false;
	}
	int cnt = 0;
	json jj = json::parse(strModel);

	json j = jj["trainModelList"];
	if (j.is_array()) {
		for (json::const_iterator iter_child = j.begin()
			; iter_child != j.end()
			; ++iter_child)
		{
			cnt++;
			Param param;
			param.pos = iter_child->at("position").get<int>();
			param.weight = iter_child->at("weight").get<float>();
			param.dis = iter_child->at("height").get<int>();
			if (cnt == 1 && isRecovery) {
				continue;
			}
			m_initmodel.push_back(param);
		}
		return true;
	}
	return false;
}