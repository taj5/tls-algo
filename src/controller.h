#ifndef CONTROLLER_HEADER
#define CONTROLLER_HEADER

#include "Logging/MTLogMgr.h"
#include "Logging/IMTLog.h"
#include "monitor.h"
#include "model.h"


using MT::Util::Logging::MTLogMgr;
using MT::Util::Logging::IMTLog;
using MT::Util::Logging::MTLogLevel;

// Constant value
#define TLSDATA_HOST "tcp://192.168.0.233"
#define TLSDATA_USER "mtload"
#define TLSDATA_PASS "mtload"
#define TLSDATA_DB "mtload"

#define LCHeight_Error 10
#define LCHeightUp_Error 70
#define LCHeightDown_Error 130

#define LCControl_Timeout 10000

// Topic to publish 
#define CONTROL_CHANNEL "TLSControlChannel"
#define ALARM_CHANNEL "TLSAlarmChannel"

// Control message definition
#define LC_DOWN		"23003"	
#define LC_UP		"23002"
#define LC_SHAKE	"23007"	
#define LC_STOP		"23004"	
#define VALVE_OPEN	"23005"	

// Alarm code to publish
#define COAL_FILL_ALARM_ID  "33001" /* Alarm when coal is not plenty in the DLC before opening the coal valve. */
#define AUTOREGULATION_ALARM_ID    "33002" /* auto-regulation alarm when the last regulate is not sufficient */

// Redis keyname set
#define TrainSpeed_Keyname	    "11008" /* Train speed value m/s 11001 */
#define TrainPos_Keyname	    "15001" /* Train position include "head" "tail" and "index". */
#define Load_Done_Index_keyname	"11009" /* Biggest car index which have been loaded.*/
#define LoadingIndex_Keyname    "13001" /* Biggest car index which is loading */
#define CarInStopRange_Keyname  "13002" /* Car info which is the stopping range*/

// Redis keyname get
#define Cabin_Weight_Keyname    "4048"  /* DLC realtime weight */
#define LCHeight_Keyname	    "27"    /* LC realtime height (actually laser sensor value) */ 
#define Command_Keyname		    "11111" /* Command recieve from mainserver. 0-stop 1-pause 2-run */ 
#define TrainModel_Keyname	    "trains"/* Train load model list */ 
#define ConfigParam_Keyname     "88888" /* Parameter include weightbias5/6/7 and startLoadIndex */

/* Feature configure macro Uncomment if you want the feature.*/
//#define HAVE_SET_MODEL  	/* The first three train condition */
//#define HAVE_COMING_COAL 	/* Coming coal condition */
#define HAVE_SHAKE

#ifdef HAVE_COMING_COAL
	#define Coal_Insufficient_Keyname 		"70966" /* 0 stands for coal is not insufficent otherwise 1 */
#endif

#ifdef HAVE_SET_MODEL
	#define Model_Set_Keyname 		"get_trains_model_flag"
#endif

#define INI_FILE_NAME	"algo-conf"
#define LogPath			"./log"


#ifndef OFF
#define OFF 0
#endif

#ifndef ON
#define ON 1
#endif

typedef enum {
	STOP,
	PAUSE,
	RUN,
}RunningState;

struct controllerServer {
	/*network*/
	std::string redisCache;
	std::string controlChannel;
	std::string configChannel;
	std::string alarmChannel;
	/*log*/
	IMTLog* logger;
	std::string logLevel;

	float weightBias[3];
	float weightBias_adjust[3];
	float weightExcess;
	int posMin;
	int posMax;
	int startLoadIndex;
	int loadIndex;
	//bool satisfied;
	RunningState prevState;
	int totalCar;
	float speed;
	bool isRecovery; /* 1 means PAUSE/STOP to RUN */
	int update; /* if param is updated flag*/
#ifdef HAVE_WARN_INDEX_KEEP
	bool supplyWarn; /* supply warning flag */
#endif
	int cmd_interval32; /* Command sent interval when system just start */
	int cmd_interval43;
	int cmd_interval54;
	int stopPoint;  /* Store the train stop point for using cmd interval*/
	std::vector<float> speed12; /* Avarage speed between point 1 and point 2*/
	float optimalSpeed;
	float speedAlgorithmK;
	float LCSpeed; /* liu cao go up speed */
	MTRedisClient mainCli;
	ControlModel model;

	float weightK;
	Monitor monitor;
	std::string modelName; /* current car model name */
	int print; /* console print flag if 1 print else 0 */
} ;

extern struct controllerServer server;
extern void serverLog(int console, enum MTLogLevel level, const char *fmt, ...);

#define DEBUG

#ifdef DEBUG
#define DBG(x) x
#else
#define DBG(x)
#endif

#endif