#FROM gcc AS base

#ADD ./tls-algo-3.0.0-Linux.tar.gz /home

##RUN apk add g++

#WORKDIR /home/tls-algo-3.0.0-Linux/algo

#ENV  LD_LIBRARY_PATH=./libs

#RUN echo $LD_LIBRARY_PATH

#CMD ["./loc"]

FROM alpine
COPY . /usr/src/tls-algo
WORKDIR /usr/src/tls-algo
RUN echo "https://mirror.tuna.tsinghua.edu.cn/alpine/v3.8/main" > /etc/apk/repositories 
RUN echo "https://mirror.tuna.tsinghua.edu.cn/alpine/v3.8/community" >> /etc/apk/repositories
RUN apk add --no-cache libstdc++ build-base cmake hiredis libevent

RUN \
mkdir -p build && cd build && cmake ../ && make

CMD [./loc]







