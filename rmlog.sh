#! /bin/bash
# Written by wuhao 2019/08/26
# This file is used to delete the log produced by
# controller and location
# Remove log file far away 3 days.


#echo $dir1
#目标目录
dir1="$(pwd)/log/"
#删除最小粒度控制
minTime=`expr 3 \* 60 \* 60 \* 24 \* 1`
#echo "expire time : ${minTime}"
currentTime=$(date +%s)
#echo "当前时间:"$currentTime


fileArr=`ls -l ${dir1} | awk '/.log$/{print $9}'`
#echo "print file array: "
#echo "${fileArr}"
for j in $fileArr
do
  createTime=`stat -c %Y "${dir1}$j"`
  #echo ${dir1}$j"文件上次修改时间:"$createTime
  mTime=`expr $currentTime - $createTime`
  #echo "时间差:"$mTime
  if [ $mTime -gt $minTime ];then
    echo "删除文件:"${dir1}$j
    rm -f ${dir1}$j
  fi
done